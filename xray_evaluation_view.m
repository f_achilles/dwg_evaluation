%% Evaluation of G-arm settings
clear attempt
clear frmpos
clear annotation_events
clear experiment

definitions
write_images_to_png = false;

% using i because its better for reading..
i = proband_id; 

% Collecting all annotation events from original data for defining start & end
annotation_events.frame_number = cleaned_proband_data.events.framepos;
annotation_events.label = cleaned_proband_data.events.labelcode;

% Collecting start- and endpoints of experiments in frame numbers
experiment.startpoints = annotation_events.frame_number(annotation_events.label == annotation_label.start);
experiment.endpoints = annotation_events.frame_number(annotation_events.label == annotation_label.end);

% --- Extracing data for attempts only ---
for fn = fieldnames(all_recordings(i))'
    attempt(1).(fn{1}) = all_recordings(i).(fn{1})(experiment.startpoints(1):experiment.endpoints(1));   
    attempt(2).(fn{1}) = all_recordings(i).(fn{1})(experiment.startpoints(2):experiment.endpoints(2));
end

% --- Setting relative frame numbers and time stamps ---
for try_idx = 1:2
    attempt(try_idx).ts = attempt(try_idx).ts - all_recordings(i).ts(experiment.startpoints(try_idx));  
    attempt(try_idx).frame_nr = attempt(try_idx).frame_nr - experiment.startpoints(try_idx);  
end


% ***************************************************
% --- Calculating phases g-arm setup / first position @ skin cut / hammering. 
% Auto-detect when g-arm is set and no more moved before skin is cut.

for try_idx = 1:2
    % Ideal view = read from 'p23' (= x-ray ap view rec)
    %garm_ideal_orbital_this_pedicle = garm_ideal_orbital(lumbar_vert_lvl_of_proband(proband_id));
    %garm_ideal_tilt_this_pedicle = garm_ideal_tilt(lumbar_vert_lvl_of_proband(proband_id));
%     garm_ideal_trans = xray_ideal_trans(lumbar_vert_lvl_of_proband(proband_id));
    
    % Calculation deviation for orbital and tilt angles
    %attempt(try_idx).orbital_angle = attempt(try_idx).orbital_angle - garm_ideal_orbital_this_pedicle;
    %attempt(try_idx).tilt_angle = attempt(try_idx).tilt_angle - garm_ideal_tilt_this_pedicle;
%     attempt(try_idx).axial_translation = attempt(try_idx).axial_translation - garm_ideal_trans;
    
    % Calculation of instrument positions (at Entry-point)
    %poses = attempt(try_idx).instrument_pose;
    %positions = [poses.pos];
    %positions = reshape(positions,3,[]);
    %ep_position = find.. 
    %x_pos_final = positions(1,ep_position);
    %y_pos_final = positions(2,ep_position);
    %z_pos_final = positions(3,ep_position);
    %attempt(try_idx).x_pos = (positions(1,:) - x_pos_final)*100;
    %attempt(try_idx).y_pos = (positions(2,:) - y_pos_final)*100;
    %attempt(try_idx).z_pos = (positions(3,:) - z_pos_final)*100;
end


% ***************************************************
% --- Calculating difference in angles between final g-arm position and ideal view
for try_idx = 1:2
    % Ideal view = read from 'p23' (= x-ray ap view rec)
    garm_ideal_orbital_this_pedicle = garm_ideal_orbital(lumbar_vert_lvl_of_proband(proband_id));
    garm_ideal_tilt_this_pedicle = garm_ideal_tilt(lumbar_vert_lvl_of_proband(proband_id));
%     garm_ideal_trans = xray_ideal_trans(lumbar_vert_lvl_of_proband(proband_id));
    
    % Calculation deviation for orbital and tilt angles
    attempt(try_idx).orbital_angle = attempt(try_idx).orbital_angle - garm_ideal_orbital_this_pedicle;
    attempt(try_idx).tilt_angle = attempt(try_idx).tilt_angle - garm_ideal_tilt_this_pedicle;
%     attempt(try_idx).axial_translation = attempt(try_idx).axial_translation - garm_ideal_trans;
    
    % Calculation of instrument positions (relative to final (end) position!)
    poses = attempt(try_idx).instrument_pose;
    positions = [poses.pos];
    positions = reshape(positions,3,[]);
    x_pos_final = positions(1,end);
    y_pos_final = positions(2,end);
    z_pos_final = positions(3,end);
%     x_pos_ideal = [x_min,x_max];
%     y_pos_ideal = [y_min,y_max];
%     z_pos_ideal = [z_min,z_max];
    attempt(try_idx).x_pos = (positions(1,:) - x_pos_final)*100;
    attempt(try_idx).y_pos = (positions(2,:) - y_pos_final)*100;
    attempt(try_idx).z_pos = (positions(3,:) - z_pos_final)*100;
end

% ****************************************************
% PLOT results in fullscreen!
if verbose
figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
end

% Plot every attempt (2 in total)
for try_idx = 1:2
% Converting timestamps @ annotation_events from datenum-format to
% readable time

clear annotation_events;

% Collect all annotation events again from data regarding attempt only
annotation_events.frame_number = find(attempt(try_idx).label);
arraypos_try_starts = find(cleaned_proband_data.events.labelcode == 1);
arraypos_try_ends = find(cleaned_proband_data.events.labelcode == 6);
annotation_events.label = cleaned_proband_data.events.labelcode(arraypos_try_starts(try_idx):arraypos_try_ends(try_idx));
annotation_events.colorcode = {'green','blue','cyan','black','yellow','red','magenta','magenta'};
num_events = numel(annotation_events.label);

ts_start = attempt(try_idx).ts(1);
ts_end = attempt(try_idx).ts(end);

for event_idx = 1:num_events
        annotation_events.time_readable{event_idx} =...
        datestr(...
        attempt(try_idx).ts(annotation_events.frame_number(event_idx)) - ts_start,...
        'MM:SS');
end

% plot: X-Ray shots and moving average
if verbose
subplot(3,2,try_idx);
    X = attempt(try_idx).frame_nr;
    
    plot(X, attempt(try_idx).xray_event, 'Color', 'red','Linewidth',0.5);
    hold on;
end
    % alternative filtering method
    % b = ones(1,c)/c;
    % a = 1;
    % filter(b,a,attempt(a_idx).xray_event) 
    
    % Calculating mean for each attempt
    % w/ moving average filter
    k = 60; % seconds
    f = 25; % SampleRate = 25 fps
    c = k*f;  
    d = 10*f;
    mean1 = movmean(attempt(try_idx).xray_event,c);
    mean2 = movmean(mean1,d);
    
    % Area plot of 1x smoothed moving mean
    if verbose
    area(X, mean1,...
        'FaceColor', '[0.9,0.9,0.9]', 'Linewidth', 0.1);
    title(['P', num2str(i), ' (V', num2str(try_idx), ' - L', num2str(lumbar_vert_lvl_of_proband(i)), ')\newlineX-ray image frequency [1/min]']);
    ylim([0,1/k]);
    xlim([0, max(X)]);
    %datetick('x','MM:ss');
    % 2x smoothed moving mean
    plot(X, mean2, 'Color', 'blue','Linewidth',2);
    
    % Adding labels to plot
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [0.0, 1.1],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Event text.
        txt = ({[annotation_name{annotation_events.label(event_idx)}],...
               ['@ ' annotation_events.time_readable{event_idx}]});
        % saving txt-like output for later file output
        out_string{event_idx} = [annotation_name{annotation_events.label(event_idx)} ';'...
            annotation_events.time_readable{event_idx}];
           
        % Label position (always visible!)
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 1/k;
            y_pos_of_text_last = y_pos_of_text;
        elseif(y_pos_of_text < 0.15/k) % if label is set too low (y-axis)
            y_pos_of_text = 0.8/k;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/5/k;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    end
    
% Plotting g-arm deviation from ideal view
if verbose
subplot(3,2,try_idx+2);    
    plot(X, attempt(try_idx).orbital_angle, 'Color', 'blue','Linewidth',0.5);
    title(['G-arm: Orbital / tilt - deviation from final view']);
    hold on;
    
    plot(X, attempt(try_idx).tilt_angle, 'Color', 'green','Linewidth',0.5);
    xlim([0, max(X)]);
    ylim([-10, 10]);
    line(xlim, [0 0]);  %plot x-axis
    ytickformat('%.f �');
    yL = ylim;
    yL = yL(2);

    % Adding labels to plot
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], ylim,...
            'Color', [0.9 0.9 0.9],...
            'Linewidth', 1.25);
        % Event text.
        txt = ({[annotation_name{annotation_events.label(event_idx)}],...
               ['@ ' annotation_events.time_readable{event_idx}]});
           
        % Label position (always visible!)
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = yL;
            y_pos_of_text_last = y_pos_of_text;
        elseif(y_pos_of_text < 0.15*yL) % if label is set too low (y-axis)
            y_pos_of_text = 0.8*yL;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - 0.2*yL;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    legend('orbital','tilt');
end

% Plotting instrument tip deviation from final position.
if verbose
subplot(3,2,try_idx+4);
    plot(X, attempt(try_idx).x_pos, 'b', X, attempt(try_idx).y_pos, 'g', X, attempt(try_idx).z_pos, 'r');
    title(['Instrument: Tip deviation from final position']);
    xlim([0, max(X)]);
    ylim([-10, 10]);
    line(xlim, [0 0]);  %plot x-axis
    
    hold on;
    % Adding labels to plot
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], ylim,...
            'Color', [0.9 0.9 0.9],...
            'Linewidth', 1.25);
        % Event text.
        txt = ({[annotation_name{annotation_events.label(event_idx)}],...
               ['@ ' annotation_events.time_readable{event_idx}]});
           
        % Label position (always visible!)
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = yL;
            y_pos_of_text_last = y_pos_of_text;
        elseif(y_pos_of_text < 0.15*yL) % if label is set too low (y-axis)
            y_pos_of_text = 0.8*yL;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - 0.2*yL;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    
    legend('x','y','z');
    ytickformat('%g cm');
end
    
    % RESULTS:
    % save deviations into array (proband_id, try_idx)
    % evaluated frame = "bone entered"
    % y = Gertzbein, x = Dev(orbital/tilt/trans)
    eventpos = find(annotation_events.label==label_numbers.enter_bone);
    frmpos = annotation_events.frame_number(eventpos(end));
    level = lumbar_vert_lvl_of_proband(proband_id);
    dev_orbital(proband_id, try_idx) = attempt(try_idx).orbital_angle(frmpos);
    dev_tilt(proband_id, try_idx) = attempt(try_idx).tilt_angle(frmpos);
    % translation / not needed
    %dev_trans(proband_id, try_idx) = attempt(try_idx).axial_translation(frmpos);
    %detailed printout of numbers:
    %fprintf('p%i, V%i: orbital %2.2f tilt %2.2f\n', proband_id, try_idx, dev_orbital(proband_id, try_idx), dev_tilt(proband_id,try_idx));
    
    % IMAGE FREQUENCY ANALYSIS
    % Mean per attempt:
    eventpos = find(annotation_events.label==label_numbers.enter_bone);
    frmpos = annotation_events.frame_number(eventpos(1)); %1st bone entered event
    images_avg_bone(proband_id, try_idx) = mean1(frmpos);
    %fprintf('Mean images/min @ bone_entered p%i, V%i: %2.2f\n', proband_id, try_idx, mean1(frmpos));
        
end % try_idx loop ends here

% SAVING images in PNG with export_fig
if(write_images_to_png == true)
    if verbose
    export_fig(sprintf('xray_eval_garm_instrument_p%i.png', i));
    end
end


%%

% File output for label-values including time (i = proband_id)
if(write_data_to_file == true)
    filename = 'DATA_xray.txt';
    fileID = fopen(filename,'a+');
    
    fprintf(fileID, ['Proband' num2str(i) ';shots;time try\r\n']);
    for line_idx=1:length(out_string)
        fprintf(fileID, '%s\r\n', string(out_string(line_idx)));
    end
    clear out_string;
    
    % adding xray-shots info
    fprintf(fileID,'Total;%s\r\n',num2str(xray_shots.total));
    fprintf(fileID,'1st;%s\r\n',num2str(xray_shots.try(1)));
    fprintf(fileID,'Last;%s\r\n',num2str(xray_shots.try(end)));
    fprintf(fileID,'C-Pad switches;%s\r\n\r\n',num2str(cpad_switches));
    fprintf(fileID,'Mean x-ray images per minute;%s\r\n\r\n',num2str(mean(mean1)));
    fclose(fileID);
end
