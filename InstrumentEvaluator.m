classdef InstrumentEvaluator < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        io_evaluation_data_table_;  % reference to evaluation dataset to draw graphs
        results_ = table();
        results_aoms_ = cell(22,2);
        results_angles_ = table();
        results_ep_data_table_ = table();
        angulation_absolute_ = cell(22,2);
        angulation_relative_ = cell(22,2);
        angulation_screw_ = cell(22,2);
    end
    
    properties (Constant)
        kClassName = 'InstrumentEvaluator';
        kClassVersion = 'V1.4';
        kClassVersionDate = '19-05-2020';
        % Changelog InstrumentEvaluator.m
        % 03-04-2020, V1.1: Feat. GetAOMIndices()
        % 12-05-2020, V1.2: Feat. EvaluateNeedleAngulation() includes angulation of screw (final pos)
        % 15-05-2020, V1.3: Feat. ComputeInstrumentPathLength() with AOMs evaluation
        % 19-05-2020, V1.4: Feat. Usage of indices from PhasesEvaluator
        % class (idx_bone_entered_last = idx_bone_entered_manual from via EP from definitions)
        kFileNameResultsXls = [InstrumentEvaluator.kClassName,'_results.xlsx'];
        kFileNameResultsTxt = [InstrumentEvaluator.kClassName,'_results_',date(),' - ',InstrumentEvaluator.kClassVersion,'.txt'];
        kFileNameResultsSheetName = [date(), ' - ', InstrumentEvaluator.kClassVersion];
        kVerbose = true;
    end
    
    methods
        function obj = InstrumentEvaluator(io_ref, probands)
            obj.SyntrackIo_ref = io_ref;
            
            % check for modified set of probands ids to evaluate (args)
            if exist('probands','var') && ~isempty(probands)
                obj.proband_ids_ = probands;
                
                % Formatted message output for evaluated probands
                if nnz(diff(diff(probands))) == 0
                    p_id_range_txt = ['p', num2str(probands(1)), ' - p', num2str(probands(end))];
                else
                    p_id_range_txt = strrep(num2str(probands),'  ',', ');
                end
                obj.Message("Initialized to evaluate 3D data for a modified range or probands (" + p_id_range_txt + ").");
            else
                % Standard case without args: Plot all available data that
                % was loaded by SyntrackIo
                obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
                obj.Message("Initialized to evaluate 3D data for p" + io_ref.proband_ids_(1) + " - p" + io_ref.proband_ids_(end) + ".");
            end
            
            if(isempty(obj.SyntrackIo_ref.clean_data_table_))
                success = 'error. no clean data available. please execute DataCleaner first';
            end
            obj.Message("Clean data loaded. Ready to evaluate instrument positions and angles.");
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;
            
            if success
                disp('Tests for InstrumentEvaluator class are all working');
            else
                warning('Not all Tests for InstrumentEvaluator class are working. Please debug and fix.');
            end
        end
        
        function success = PlotInstrumentPath(obj)
            % 3D plot of instrument (trocar) path from certain 'start' to certain 'end' label
            obj.Message("> Plotting instrument paths...");
            proband_ids = obj.proband_ids_;
            
            % Plot instrument paths
            for p_id = proband_ids
                % Setting dataset for each proband (contains try 1 and 2)
                dataset_table = obj.SyntrackIo_ref.clean_data_table_(p_id,:);
                
                obj.PlotTrocarPath(dataset_table, p_id);
            end
            
            obj.Message("Instrument paths plotted.");
        end
        
        function success = ComputePathLengthCountAOMs(obj, length_setting, arg, option)
            % Computes path length and counts number of AOMs per interval
            % Specify arg = 'UseAoms', 'true' to use aom_indices for
            % additional path length calculation within AOMs intervals
            
            % computes path lenghts of instrument tip (standard setting = total path)
            if nargin < 1
                length_setting = 'start_end';   % standard setting
                UseAoms = false;                % standard = evaluate raw path length only
            end
            if nargin > 2
                if (arg == "UseAoms" && option == "true")
                    use_aoms = true;    % evaluate raw path length + path within AOMs
                else
                    use_aoms = false;
                end
            end
            
            definitions;
            proband_ids = obj.proband_ids_;
            aom_indices_cellarray = obj.results_aoms_;
            
            obj.Message("Calculating path lengths with setting '" + length_setting + "'...");
            
            if(use_aoms)
                if(sum(any(cellfun('isempty', aom_indices_cellarray))) == 0)
                    obj.Message("> AoM indices found. Using indices to calculate path lengths within AoMs only.");
                else
                    obj.Message("> No AoM indices found. Executing 'GetAOMIndices()'...");
                    obj.GetAOMIndices('debug', false, 'savefigures', false);
                    if(~isempty(obj.results_aoms_))
                        obj.Message("> AoM indices found. Using indices to calculate path lengths within AoMs only.");
                        aom_indices_cellarray = obj.results_aoms_;
                    end
                end
            end
            
            % pre-allocation
            var_names_std = {'p_id', 'try_id'};
            var_names_evaluated = {['instr_path_raw_', length_setting], ['instr_path_aoms_large_', length_setting], ['instr_path_aoms_small_', length_setting], ['aoms_large_count_', length_setting], ['aoms_small_count_', length_setting]};
            var_names = [var_names_std var_names_evaluated];
            var_types(1:length(var_names)) = {'double'};
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);
            row_no = 1;
            
            path_type_names = {'raw';'aoms_large';'aoms_small'};
            path_lengths_struct = struct(path_type_names{1},[], path_type_names{2},[], path_type_names{3},[]);
            interval_count_struct = path_lengths_struct;
            
            if(use_aoms) % if false, evaluate raw path length only!
                path_types_to_evaluate = 1:3;
            else
                path_types_to_evaluate = 1;
            end
            
            for p_id = proband_ids
                for try_id = 1:2
                    database = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
                    
                    % getting instrument positions and rotations
                    poses = database.instrument_pose;
                    positions = [poses.pos];
                    positions = reshape(positions,3,[]);
                    
                    % Resample to 24 Hz (mean SampleRate of all proband recordings)
                    time_rel = database.datetime_relative;
                    TT = timetable(time_rel, positions(1,:)', positions(2,:)', positions(3,:)',database.label);
                    TT = sortrows(TT);
                    TT_resampled= retime(TT, 'regular', 'pchip', 'SampleRate', 24);
                    positions_resampled = [TT_resampled.Var1';TT_resampled.Var2';TT_resampled.Var3'];
                    
                    % Settings determine which path length to calculate
                    
                    % Getting indices: phase_indices{p_id, try_id}.skin_cut/bone_entered_first/bone_perf_ep
                    phase_indices = obj.SyntrackIo_ref.results_.PhasesEvaluator.phases_indices_{p_id, try_id};
                    % only take first 'skin cut' event after start_indices(n)
                    cut_idx = phase_indices.skin_cut;
                    % if skin_cut not found, first enter_bone is taken instead (one of these
                    % must exist!)
                    bone_first_idx = phase_indices.bone_entered_first;
                    bone_perf_ep_idx = phase_indices.bone_perf_ep;
                    end_idx = length(positions);
                    
                    switch length_setting
                        % --- [start -> end] ------------------------
                        case 'start_end'    % [start -> end] = path_total
                            start_idx = 1;
                        % --- [start -> skin cut] ------------------------
                        case 'start_skin'
                            start_idx = 1;
                            if isempty(cut_idx)
                                obj.Message(sprintf("p%i, try%i: No 'skin_cut' event found. Using first 'enter_bone' instead.\n", p_id, try_id));
                                if isempty(cut_idx)
                                    cut_idx = bone_first_idx;
                                end
                            end
                            end_idx = cut_idx;
                        % --- [skin cut -> bone entered] ------------------------
                        case 'skin_bone_first'
                            start_idx = cut_idx;
                            if isempty(cut_idx)
                                obj.Message(fprintf("p%i, try%i: No 'skin_cut' event found. Using first 'enter_bone' instead.\n", p_id, try_id));
                                if isempty(cut_idx)
                                    cut_idx = bone_first_idx;
                                end
                            end
                            end_idx = bone_first_idx;
                        % --- [bone entered (first) -> end] ------------------------
                        case 'bone_first_end'
                            start_idx = bone_first_idx;
                        % --- [bone_perf_ep -> end] ------------------------
                        case 'bone_perf_end'
                            start_idx = bone_perf_ep_idx;
                    end

                    % ! Re-Calculating label indices for resampled data
                    start_idx = interp1(TT_resampled.time_rel, 1:size(TT_resampled.time_rel), time_rel(start_idx), 'nearest');
                    end_idx = interp1(TT_resampled.time_rel, 1:size(TT_resampled.time_rel), time_rel(end_idx), 'nearest');

                    % get aom indices and loop stepwise through path intervals (n:n+1) of aom_indices
                    aoms_indices = aom_indices_cellarray{p_id, try_id};
                    
                    % doing calc for both, large and small AOMs
                    aoms_indices_to_use_large = aoms_indices.aoms_large_indices(start_idx <= aoms_indices.aoms_large_indices & aoms_indices.aoms_large_indices <= end_idx);
                    aoms_indices_to_use_small = aoms_indices.aoms_small_indices(start_idx <= aoms_indices.aoms_small_indices & aoms_indices.aoms_small_indices <= end_idx);
                                       
                    % 1 = raw evaluation, 2 = aoms_large, 3 = aoms_small
                    indices_to_use_array = {[start_idx, end_idx]; aoms_indices_to_use_large; aoms_indices_to_use_small};
                    for path_type = path_types_to_evaluate
                        path_length_aom_total = 0;
                        path_length_aom_interval = 0;
                        aoms_count_per_inteval = 0;
                        for aom_idx = 1:length(indices_to_use_array{path_type})-1
                            if(mod(aom_idx,2) == 1)
                                % Mark areas between odd -> even indices = AoMs
                                start_idx_interval = indices_to_use_array{path_type}(aom_idx);
                                end_idx_interval = indices_to_use_array{path_type}(aom_idx+1);
                                path = positions_resampled(:,start_idx_interval:end_idx_interval);
                                path_diff = diff(path,1,2);
                                steplength = zeros(1,size(path_diff,2));
                                for vec_idx = 1:size(path_diff,2)
                                    steplength(vec_idx) = norm(path_diff(:,vec_idx));
                                end
                                path_length_aom_interval = sum(steplength);
                                path_length_aom_total = path_length_aom_total + path_length_aom_interval;
                                aoms_count_per_inteval = aoms_count_per_inteval + 1;
                            end
                        end
                        interval_count_struct.(path_type_names{path_type}) = aoms_count_per_inteval;
                        path_lengths_struct.(path_type_names{path_type}) = round(path_length_aom_total * 100,1); % in cm (0.1 cm precision)
                    end
                    
                    if(use_aoms)
                        obj.Message(sprintf("> p%i, try%i: path lengths -> raw: %.1f cm, AOMs: = %.1f cm. | %.1f cm (large/small)", p_id, try_id, path_lengths_struct.raw, path_lengths_struct.aoms_large, path_lengths_struct.aoms_small));                        
                    else
                        obj.Message(sprintf("> p%i, try%i: path length = %.1f cm.", p_id, try_id, path_lengths_struct.raw));
                        path_lengths_struct.aoms_large = NaN;
                        path_lengths_struct.aoms_small = NaN;
                        interval_count_struct.aoms_large = NaN;
                        interval_count_struct.aoms_small = NaN;
                    end
                    
                    T_newrow = {p_id, try_id, path_lengths_struct.raw, path_lengths_struct.aoms_large, path_lengths_struct.aoms_small, interval_count_struct.aoms_large, interval_count_struct.aoms_small};
                    T_results(row_no,:) = T_newrow;
                    row_no = row_no + 1;
                end
            end
            obj.Message("Computation done.");
            
            T_results = obj.AssignGroups(T_results);
            disp(T_results);
            
            if(isempty(obj.results_))
                obj.results_ = T_results;
            else
                % join tables using new data ('T_results') to overwrite old
                % columns with same name via 'KeepOneCopy' option
                obj.results_ = join(T_results, obj.results_, 'Keys', {'p_id', 'try_id', 'group'}, 'KeepOneCopy', var_names_evaluated);
            end
            
            obj.Message(sprintf("Path lengths and counted AOMs ('%s') saved into 'results_'.", length_setting));
            success = true;
        end
        
        
        function success = EvaluateNeedleEPDistanceFromIdeal(obj)
            % 1. Get ideal EP positions: 'entry_points_ideal_array' (definitions)
            % 2. Get positions from first bone contact (label), 'path_bone_entered_coordinates' (definitions)
            %   via Search closest neighbor: 
            %   database = data_io.clean_data_table_{p_id, try_id}
            %   poses = database.instrument_pose;
            %   positions = [poses.pos];
            %   positions = reshape(positions,3,[]);
            %	haystack = positions';
            %   needle = path_bone_entered_coordinates(p_id, try_id);
            %	idx_pos = knnsearch(haystack, needle);
            %	x_pos_ep = positions(1,idx_pos);
            %	y_pos_ep = positions(2,idx_pos);
            %   z_pos_ep = positions(3,idx_pos);
            %   distance_3d = sqrt((x-x1)^2+(y-y1)^2+(z-z1)^2) ?
            %   distance_2d = sqrt((x-x1)^2+(y-y1)^2) ?
            
            obj.Message("> Distance and relative position to pedicle of Needle Entry Points evaluated.");
            warning('not yet implemented');
            
            definitions;
            
            % 'Evaluate' means
            % 1. calculation:   CalculateEntryPointDistance()
            % 2. plotting:      PlotEntryPointDistance()
            
            for p_id = obj.proband_ids_
                for try_id = 1:2
                    
                    % Get distance at first bone entered label
                    % (definitions)_
                  
                    % 
                    % Get distance at manually identified EP
                    % (definitions)_
                  
                    % 
                end
            end
            
            obj.Message("Distance and relative position to pedicle of Needle Entry Points evaluated.");
        end
        
        % Helper function.
        function h = PlotCircle(obj, x, y, r)
            hold on
            th = 0:pi/50:2*pi;
            xunit = r * cos(th) + x;
            yunit = r * sin(th) + y;
            h = plot(xunit, yunit);
            hold off
        end
        
        function [pedicle_centers, pedicle_principal_directions, pedicle_principal_variances] ...
                = PlotBoneMesh3D(obj,...
                probands_ids, grouping_criterion, bead_offset_in_rai_coordinates, pedicle_meshes)               
            entry_points_figure_handle = figure('Color','black');
            entry_points_figure_handle.WindowState = 'maximized';
            axiscolor = [0.75 0.75 0.75];
            set(gca,'Color','k','XColor',axiscolor,'YColor',axiscolor,'ZColor',axiscolor);
            if(grouping_criterion == "plot_all")
                % set title here for 'plot_all'
                title_header = sprintf('Entry point map of probands %i to %i, grouped by "%s"',...
                probands_ids(1),...
                probands_ids(end),...
                grouping_criterion);
                title_color_explanation = ['(Novice = blue, Experienced = green,', 'Ideal Entry Point (AO) = black, Gertzbein Class > A = magenta)'];
                
                title({title_header, title_color_explanation}, 'color', 'white', 'Interpreter', 'none');
            end

            % Load pedicle meshes (here: 2 sides x 3 vertebrae)
            for p = 1:numel(pedicle_meshes)
                pedicle_mesh = loadawobj(pedicle_meshes{p});
                pm_handle_patch = patch(...
                    'vertices', bsxfun(@minus,...
                    pedicle_mesh.v', bead_offset_in_rai_coordinates),...
                    'faces', pedicle_mesh.f3',...
                    'edgecolor', 'none',...
                    'facecolor', 'red',...
                    'facealpha', 1);
                    %'facecolor', [0.3010 0.7450 0.9330],...
                rotate(pm_handle_patch,[1 0 0], 90, [0 0 0])
                rotate(pm_handle_patch,[0 0 1], -90, [0 0 0])

                % Compute centers.
                % pm_handle_patch.Vertices is [Nx3]
                vs = pm_handle_patch.Vertices';
%                 pedicle_centers(:, p) =(...
%                     max(vs, [], 2) +...
%                     min(vs, [], 2))...
%                     / 2;
%                 pedicle_centers(:, p) = median(vs, 2);
				pedicle_centers(:, p) = mean(vs, 2);

                % Compute principal directions, to do an ellipse
                % mapping on the entry points later on.
                %
                % Columns are pca-dirs in descending order.
                [principal_components, ~, pc_vars] = ...
                    pca(pm_handle_patch.Vertices);
                % The pedicles on the left have the second pca
                % vector pointing towards medial, hence their pca
                % vectors have to be rotated.
                % Rotation: 180 degrees around the PCA-X.
                if (pedicle_centers(2, p) < 0.0)
                    principal_components = ...
                        principal_components * ...
                        angle2dcm(pi, 0, 0, 'XYZ');
                end
                pedicle_principal_directions{p} = ...
                    principal_components;
                pedicle_principal_variances{p} = ...
                    pc_vars;
            end

            % Load bone mesh (levels L2, L3 and L4) into figure.
            % All levels are loaded, in case the proband chose the
            % wrong level.
            for lumbar_level = 2:4
                bone_mesh_path = sprintf('./meshes/mecanix_nice_L%i.obj', lumbar_level);
                bone_mesh = loadawobj(bone_mesh_path);
                bonemesh_handle_patch = patch(...
                    'vertices', bsxfun(@minus, bone_mesh.v', bead_offset_in_rai_coordinates),...
                    'faces', bone_mesh.f3',...
                    'edgecolor', 'none',...
                    'facecolor', [1 1 1],...
                    'facealpha', 0.5);
                rotate(bonemesh_handle_patch,[1 0 0], 90, [0 0 0])
                rotate(bonemesh_handle_patch,[0 0 1], -90, [0 0 0])
            end

            % figure config
            camlight
            xlabel('X: inf->sup')
            ylabel('Y: links->rechts')
            zlabel('Z: ant->post')
            % Set view such that cranial direction is up on screen.
            view(90, 90);
            axis equal;

            obj.Message("Plotting done.");
        end
        
        function success = PlotNeedleEntryPointsIdealViewAxial(obj)
            definitions; 
            statistics_use_bootstrapping = true; % set ellipses statistics to 'bootstrapping' instead of '95% percentile'

            T = obj.results_ep_data_table_;
            
            % Plot 1) All EPs plotted separate for each pedicle, coloured by groups
            obj.Message("> Plotting Entry Point Positions per Pedicle in Axial View separated by Groups...");
            group_colors = {'blue', 'green'};
            groups = ["novice", "experienced"];
            
            figure('Units','normalized','Position',[0.375 0.2 0.25 0.6]);
            for pedicle_id = 1:6
                subplot(3, 2, pedicle_id);
                for group_idx = 1:2
                    group = groups(group_idx);
                    
                    eps_relative_pos_to_ideal_ep = T.ep_xyz_ideal_view_mm((T.pedicle_id == pedicle_id & T.group == group),:);
                    eps_rel_x = eps_relative_pos_to_ideal_ep(:,1);
                    eps_rel_z = eps_relative_pos_to_ideal_ep(:,3);
                    
                    hold on
                    % Plot coordinates in optimal view in millimeters
                    plot(...
                        eps_rel_x,...
                        eps_rel_z,...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    hold off
                end
                axis equal;
                ax = gca;
                ax.YLim = ([-12 12]);
                ax.XLim = ([-12 12]);
                ax.YTick = (-10:5:10);
                ax.XTick = (-10:5:10);
                
                is_left_side = logical(mod(pedicle_id, 2));
                if is_left_side
                    xlabel('lateral -> medial (mm)');
                else
                    xlabel('medial -> lateral (mm)');
                end
                ylabel('ventral -> dorsal (mm)');
                title(pedicle_meshes_titles_en{pedicle_id});
            end
            sgtitle({
                'Entry Point Positions per Pedicle',...
                'Centers of optimal EP = black',...
                'Experienced = green, Novices = blue',...
                '',...
                'axial view'});
            obj.Message("Done.");
            
            % Plot 2) All EPs mapped combined into two big plots (separated by side)
            obj.Message("> Plotting combined plot (left and right side separated)...");
            sides = ["left", "right"];
            
            figure('Units','normalized','Position',[0.375 0.35 0.25 0.3]);
            for side_idx = 1:2
                vertebra_side = sides(side_idx);
                
                subplot(1,2,side_idx)
                hold on;
                for group_idx = 1:2
                    group = groups(group_idx);
                    
                    eps_xz_side = T.ep_xyz_ideal_view_mm((T.side == vertebra_side & T.group == group),[1,3]);
                    plot(...
                        eps_xz_side(:,1),...
                        eps_xz_side(:,2),...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    
                    is_left_side = logical(mod(side_idx, 2));
                    if is_left_side
                        xlabel('lateral -> medial (mm)');
                    else
                        xlabel('medial -> lateral (mm)');
                    end
                    ylabel('caudal -> cranial (mm)');
                    axis equal;
                    ax = gca;
                    ax.YLim = ([-12 12]);
                    ax.XLim = ([-12 12]);
                    ax.YTick = (-10:5:10);
                    ax.XTick = (-10:5:10);
                    title(vertebra_side + ' side');
                end
                hold off;
            end
            sgtitle({
                'Combined Entry Point Positions (two sides)',...
                '(all of same side projected onto one pedicle)',...
                'Experienced = green, Novices = blue, Ideal Entry Point = black',...
                '',...
                'axial view'});
            obj.Message("Done.");
            
            % Plot 3) All EPs plotted into _one_ plot
            obj.Message("> Plotting combined plot (one plot)...");
            sides = ["left", "right"];
            
            figure('Units','normalized','Position',[0.375 0.35 0.25 0.3]);
            % Creating new table with flipped mediolateral (x) coordinates
            T_comb = T;
            T_comb.ep_xyz_ideal_view_mm(T.side == "right",1) = -1 * T.ep_xyz_ideal_view_mm(T.side == "right",1);
            
            for group_idx = 1:2
                group = groups(group_idx);
                
                for side_idx = 1:2
                    vertebra_side = sides(side_idx);
                    
                    eps_xz_side = T_comb.ep_xyz_ideal_view_mm((T_comb.side == vertebra_side & T_comb.group == group),[1,3]);
                    
                    hold on;
                    plot(...
                        eps_xz_side(:,1),...
                        eps_xz_side(:,2),...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    hold off;
                    
                    is_left_side = logical(mod(side_idx, 2));
                    if is_left_side
                        xlabel('lateral -> medial (mm)');
                    else
                        xlabel('medial -> lateral (mm)');
                    end
                    ylabel('ventral -> dorsal (mm)');
                    axis equal;
                    ax = gca;
                    ax.YLim = ([-12 12]);
                    ax.XLim = ([-12 12]);
                    ax.YTick = (-10:5:10);
                    ax.XTick = (-10:5:10);
                    title({...
                        'Combined Entry Point Positions',...
                        '(all projected onto one pedicle)',...
                        'Ideal Entry Point = black, Median of Group = magenta',...
                        'Experienced = green, Novices = blue',...
                        '',...
                        'axial view'});
                end
                
                hold on;
                    plot(...
                        median(eps_xz_side(:,1)),...
                        median(eps_xz_side(:,2)),...
                        '.','MarkerSize',25,'Color', 'magenta');
                hold off;
            end
            obj.Message("Done.");
            
            obj.Message("> Plotting statistical evaluation z/y-axis with ellipses, 95% percentile and means...");
            % Plot 4) Scatter ellipse plot zy
            clear g;
            colormap_groups_gb = [[0 1 0];[0 0 1]];
            % using flipped x-coordinates from before (T_comb)
            
            x_novice = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "novice",1);
            x_exp = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "experienced",1);
            z_novice = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "novice",3);
            z_exp = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "experienced",3);
            x = [x_novice;x_exp];
            y = [z_novice;z_exp];
            color_groups = [T.group(T.group == "novice");T.group(T.group == "experienced")];
            g=gramm('x',x,'y',y,'color',color_groups);
            g.set_names('x', 'lateral -> medial (mm)', 'y', 'ventral -> dorsal (mm)', 'color','Group');
            g.geom_point();
            %'patch_opts' can be used to provide more options to the patch() internal call
            g.set_point_options('base_size',2.5);
            g.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            if (statistics_use_bootstrapping == true)
                g.stat_ellipse('type','ci','geom','area','patch_opts',{'FaceAlpha',0.05,'LineWidth',1.5});
                g.set_stat_options('nboot', 10000);
            else
                g.stat_ellipse('type','95percentile','geom','area','patch_opts',{'FaceAlpha',0.05,'LineWidth',1});
            end
            g.set_title({...
                'Combined Entry Point Positions',...
                '(all projected onto one pedicle)',...
                'Ellipses with means (crosses), 95% percentile (outlines)',...
                'Experienced = green, Novices = blue', 'Ideal Entry Point = black',...
                '',...
                'axial view'});
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            g.axe_property('YLim',[-15 15]);
            g.axe_property('XLim',[-15 15]);
            figure;
            g.draw();
            set([g.results.stat_ellipse.center_handle],'Marker', '+', 'LineWidth', 2.5, 'MarkerSize', 15);
            % Plots a cross for ideal entry point center
            plot(g.facet_axes_handles(1), 0.0, 0.0, ...
                '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
            % Plots a circle
            plot(g.facet_axes_handles(1),0.0, 0.0, ...
                'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
            hold off;
            obj.Message("Done.");
        end
        
        function success = PlotNeedleEntryPointsIdealViewAP(obj)
            definitions; 
            statistics_use_bootstrapping = true; % set ellipses statistics to 'bootstrapping' instead of '95% percentile'

            T = obj.results_ep_data_table_;
            
            % Plot 1) All EPs plotted separate for each pedicle, coloured by groups
            obj.Message("> Plotting Entry Point Positions per Pedicle in a.p. Ideal View separated by Groups...");
            group_colors = {'blue', 'green'};
            groups = ["novice", "experienced"];
            
            figure('Units','normalized','Position',[0.375 0.2 0.25 0.6]);
            for pedicle_id = 1:6
                subplot(3, 2, pedicle_id);
                for group_idx = 1:2
                    group = groups(group_idx);
                    
                    eps_relative_pos_to_ideal_ep = T.ep_xyz_ideal_view_mm((T.pedicle_id == pedicle_id & T.group == group),:);
                    eps_rel_x = eps_relative_pos_to_ideal_ep(:,1);
                    eps_rel_y = eps_relative_pos_to_ideal_ep(:,2);
                    
                    hold on
                    % Plot coordinates in optimal view in millimeters
                    plot(...
                        eps_rel_x,...
                        eps_rel_y,...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    hold off
                end
                axis equal;
                ax = gca;
                ax.YLim = ([-12 12]);
                ax.XLim = ([-12 12]);
                ax.YTick = (-10:5:10);
                ax.XTick = (-10:5:10);
                
                is_left_side = logical(mod(pedicle_id, 2));
                if is_left_side
                    xlabel('lateral -> medial (mm)');
                else
                    xlabel('medial -> lateral (mm)');
                end
                ylabel('caudal -> cranial (mm)');
                title(pedicle_meshes_titles_en{pedicle_id});
            end
            sgtitle({
                'Entry Point Positions per Pedicle and Group',...
                'Centers of optimal EP = black',...
                'Experienced = green, Novices = blue',...
                '',...
                'a.p. view'});
            obj.Message("Done.");
            
            % Plot 2) All EPs mapped combined into two big plots (separated by side)
            obj.Message("> Plotting combined plot (left and right side separated)...");
            sides = ["left", "right"];
            
            figure('Units','normalized','Position',[0.375 0.35 0.25 0.3]);
            for side_idx = 1:2
                vertebra_side = sides(side_idx);
                
                subplot(1,2,side_idx)
                hold on;
                for group_idx = 1:2
                    group = groups(group_idx);
                    
                    eps_xy_side = T.ep_xyz_ideal_view_mm((T.side == vertebra_side & T.group == group),1:2);
                    plot(...
                        eps_xy_side(:,1),...
                        eps_xy_side(:,2),...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    
                    is_left_side = logical(mod(side_idx, 2));
                    if is_left_side
                        xlabel('lateral -> medial (mm)');
                    else
                        xlabel('medial -> lateral (mm)');
                    end
                    ylabel('caudal -> cranial (mm)');
                    axis equal;
                    ax = gca;
                    ax.YLim = ([-12 12]);
                    ax.XLim = ([-12 12]);
                    ax.YTick = (-10:5:10);
                    ax.XTick = (-10:5:10);
                    title(vertebra_side + ' side');
                end
                hold off;
            end
            sgtitle({
                'Combined Entry Point Positions (two sides)',...
                '(all of same side projected onto one pedicle)',...
                'Experienced = green, Novices = blue, Ideal Entry Point = black',...
                '',...
                'a.p. view'});
            obj.Message("Done.");
            
            % Plot 3) All EPs plotted into _one_ plot
            obj.Message("> Plotting combined plot (one plot)...");
            sides = ["left", "right"];
            
            figure('Units','normalized','Position',[0.375 0.35 0.25 0.3]);
            % Creating new table with flipped mediolateral (x) coordinates
            T_comb = T;
            T_comb.ep_xyz_ideal_view_mm(T.side == "right",1) = -1 * T.ep_xyz_ideal_view_mm(T.side == "right",1);
            
            for group_idx = 1:2
                group = groups(group_idx);
                
                for side_idx = 1:2
                    vertebra_side = sides(side_idx);
                    
                    eps_xy_side = T_comb.ep_xyz_ideal_view_mm((T_comb.side == vertebra_side & T_comb.group == group),1:2);
                    
                    hold on;
                    plot(...
                        eps_xy_side(:,1),...
                        eps_xy_side(:,2),...
                        '.','MarkerSize',15,'Color',group_colors{group_idx});
                    % Plots a cross for ideal entry point center
                    plot(0.0, 0.0, ...
                        '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                    % Plots a circle
                    plot(0.0, 0.0, ...
                        'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    hold off;
                    
                    is_left_side = logical(mod(side_idx, 2));
                    if is_left_side
                        xlabel('lateral -> medial (mm)');
                    else
                        xlabel('medial -> lateral (mm)');
                    end
                    ylabel('caudal -> cranial (mm)');
                    axis equal;
                    ax = gca;
                    ax.YLim = ([-12 12]);
                    ax.XLim = ([-12 12]);
                    ax.YTick = (-10:5:10);
                    ax.XTick = (-10:5:10);
                    title({...
                        'Combined Entry Point Positions plot',...
                        '(all projected onto one pedicle)',...
                        'Ideal Entry Point = black, Median of Group = magenta',...
                        'Experienced = green, Novices = blue',...
                        '',...
                        'a.p. view'});
                end
                
                hold on;
                plot(...
                    median(eps_xy_side(:,1)),...
                    median(eps_xy_side(:,2)),...
                    '.','MarkerSize',25,'Color', 'magenta');
                hold off;
            end
            obj.Message("Done.");
            
            obj.Message("> Plotting statistical evaluation x/y-axis with ellipses, 95% percentile and means...");
            % Plot 4) Gramm scatter ellipse plot xy
            colormap_groups_gb = [[0 1 0];[0 0 1]];
            
            % using flipped x-coordinates from before (T_comb)
            x_novice = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "novice",1);
            x_exp = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "experienced",1);
            y_novice = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "novice",2);
            y_exp = T_comb.ep_xyz_ideal_view_mm(T_comb.group == "experienced",2);
            x = [x_novice;x_exp];
            y = [y_novice;y_exp];
            color_groups = [T.group(T.group == "novice");T.group(T.group == "experienced")];
            g=gramm('x',x,'y',y,'color',color_groups);
            g.set_names('x', 'lateral -> medial (mm)', 'y', 'caudal -> cranial (mm)', 'color','Group');
            g.geom_point();
            %'patch_opts' can be used to provide more options to the patch() internal call
            g.set_point_options('base_size',2.5);
            g.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            if (statistics_use_bootstrapping == true)
                g.stat_ellipse('type','ci','geom','area','patch_opts',{'FaceAlpha',0.05,'LineWidth',1.5});
                g.set_stat_options('nboot', 10000);
            else
                g.stat_ellipse('type','95percentile','geom','area','patch_opts',{'FaceAlpha',0.05,'LineWidth',1});
            end
            g.set_title({...
                'Combined Entry Point Positions plot',...
                '(all projected onto one pedicle)',...
                'Ellipses with means (crosses), 95% percentile (outlines)',...
                'Experienced = green, Novices = blue', 'Ideal Entry Point = black',...
                '',...
                'a.p. view'});
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            g.axe_property('YLim',[-15 15]);
            g.axe_property('XLim',[-15 15]);
            figure;
            g.draw();
            set([g.results.stat_ellipse.center_handle],'Marker', '+', 'LineWidth', 2.5, 'MarkerSize', 15);
            % Plots a cross for ideal entry point center
            plot(g.facet_axes_handles(1), 0.0, 0.0, ...
                '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
            % Plots a circle
            plot(g.facet_axes_handles(1),0.0, 0.0, ...
                'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
            hold off;
            obj.Message("Done.");
        end
        
        function success = PlotNeedleEntryPoints(obj, grouping_criterion, plot_trajectory)
            %% Visualization of EPs at manual identified points below the cortical bone, from which the needle tip is advanced to the final position.
            % EP mapping of all EPs (grouped by try/level/event) into _one_
            % model (z-axis view)
            %
            % FA: Added plotting option "plot_single" and "plot_all". Also "plot_trajectory = false/true"
           
            if(~exist('grouping_criterion', 'var'))
                grouping_criterion = 'plot_single';
            end
            if(~exist('plot_trajectory', 'var'))
                plot_trajectory = true;
            end
            
            save_figure = true; % switch to save 3D figure
                       
            probands_ids = obj.proband_ids_;
            
            obj.Message("> Plotting Entry point map for p" + probands_ids(1)...
                + " - p" + probands_ids(end) +...
                ", grouped by " + grouping_criterion + "...");
            
            % Debug options and settings
            debug_show_pca_vectors = false;         % debug pca vectors
            debug_show_ideal_eps = true;            % show mapped ideal EPs accoring to AO Surg Ref
            debug_mark_gertzbein_c_to_e = false;    % mark gertzbein class c and d red
            debug_show_ideal_trajectories = true;   % show yellow lines indicating the "perfect" screw trajectory (Weinstein ep trough the pedicle center)
            debug_plot_pca_mapped_eps = false;      % plot one plot of all eps in pca coordinates with pca-ellipse
            
            definitions;
            % Defining meshes & Bead offset.
            bead_offset_in_rai_coordinates = [0.144, 0.1277, 0.234];
            
            % Initialize containers.
            pedicle_principal_directions = cell(1, 6);
            pedicle_principal_variances = cell(1, 6);
            pedicle_centers = zeros(3,6);
            
            % ---- Option: 'plot_all' -> Plot bone mesh, 
            if strcmp(grouping_criterion, 'plot_all')
                [pedicle_centers, pedicle_principal_directions, pedicle_principal_variances] = ...
                    PlotBoneMesh3D(obj, probands_ids, grouping_criterion, bead_offset_in_rai_coordinates, pedicle_meshes);
            end
            
            % Compute entry points and the mapping to PCA-space.
            dataset_table = obj.SyntrackIo_ref.clean_data_table_;
            
            % pre-allocate table 'entry_point_data_table'
            varNames = {'ep_id', 'pedicle_id', 'vertebra', 'side', 'p_id', 'try_id', 'group',...
                'ep_xyz_orig', 'ep_xyz_ideal_view_mm', 'ep_2d_distance_ap_mm', 'ep_2d_distance_axial_mm'};
            T_entry_point_data = table(zeros(44,1),zeros(44,1),categorical(strings(44,1)),categorical(strings(44,1)),zeros(44,1),zeros(44,1),categorical(strings(44,1)),...
                zeros(44,3),zeros(44,3),zeros(44,1),zeros(44,1),...
                'VariableNames',varNames);
            pedicle_side = {'right','left'};
            vertebra = {'L2','L2','L3','L3','L4','L4'};
            
            ep_idx = 1;
            for p_id = obj.proband_ids_
                lvl = lumbar_vert_lvl_of_proband(p_id);
                group = groups{p_id};
                %pediculation_positions = zeros(3,2);
                
                for try_id = 1:2                   
                    % Get positions of probands' entry points (from definitions)
                    entry_point = path_bone_entered_coordinates{p_id, try_id}';
                    
                    % Get closest pedicle center for each entry point
                    pedicle_center_distances = ...
                        sqrt(sum((pedicle_centers - entry_point).^2, 1));
                    [~, closest_pedicle_center_idx] = min(pedicle_center_distances);

                    % Saving data for each entry point into table row
                    T_entry_point_data(ep_idx,1:8) = {...
                        ep_idx,...
                        closest_pedicle_center_idx,...
                        vertebra(closest_pedicle_center_idx),...
                        pedicle_side(mod(closest_pedicle_center_idx,2)+1),...
                        p_id,...
                        try_id,...
                        group,...
                        entry_point'};
                    
                    ep_idx = ep_idx + 1;
                end
                
                % Make a new bone mesh plot for each proband (e.g. for manual entry point
                % corrections, moving along the trajectory).
                if strcmp(grouping_criterion, 'plot_single')
                    [pedicle_centers, pedicle_principal_directions, pedicle_principal_variances] = ...
                        PlotBoneMesh3D(obj, probands_ids, grouping_criterion, bead_offset_in_rai_coordinates, pedicle_meshes);
                    
                    axial_slice_width = 0.06;
                    % ToDo(MM): Case separation for two different levels
                    % -> use entry_point_data_table !
                    switch lvl
                        case 2
                             axis([-0.075+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                        case 3
                             axis([-0.0315+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                        case 4
                             axis([0.005+[0 axial_slice_width] -0.06 0.06 0.01 0.11]);
                    end
                end
                
                hold on               
                % Plot Jamshidi tip trajectory.
                if plot_trajectory
                    try_1_and_2_for_this_proband_table = dataset_table(p_id,:);
                    obj.PlotTrocarPath(try_1_and_2_for_this_proband_table, p_id);
                end
                hold off
                
                % Switch on the nicely interpolated light at the end in order to affect all
                % meshes in the figure.
                lighting gouraud     
                
                % Debug plot: show ideal Entry Points (from definitions)
                if (debug_show_ideal_eps)
                    hold on;
                    for pedicle_id = 1:6
                        % Plots a cross
                        plot3(...
                            entry_points_ideal_array(pedicle_id, 1),...
                            entry_points_ideal_array(pedicle_id, 2),...
                            entry_points_ideal_array(pedicle_id, 3),...
                            '+','MarkerSize',25,'LineWidth', 1, 'Color','black');
                        % Plots a circle
                        plot3(...
                            entry_points_ideal_array(pedicle_id, 1),...
                            entry_points_ideal_array(pedicle_id, 2),...
                            entry_points_ideal_array(pedicle_id, 3),...
                            'o','MarkerSize',15,'LineWidth', 2, 'Color','black');
                    end
                    hold off;
                end               
                
                % Debug plot: show PCA vectors for 'plot_single'
                if ((strcmp(grouping_criterion, 'plot_single')) && (debug_show_pca_vectors))
                    % Debug plot of the pca vectors.
                    for pedicle_id = 1:6
                        pca_dirs = pedicle_principal_directions{pedicle_id};
                        pca_sdevs = sqrt(pedicle_principal_variances{pedicle_id});
                        %pca_dirs = pca_dirs * 0.02;
                        pca_dirs = pca_dirs .* repmat(...
                            reshape(pca_sdevs, 1, 3), 3, 1);
                        hold on
                        center = pedicle_centers(:, pedicle_id);
                        plot3(...
                        center(1) + [0 pca_dirs(1,1)],...
                        center(2) + [0 pca_dirs(2,1)],...
                        center(3) + [0 pca_dirs(3,1)],...
                        '-','LineWidth',3,'Color','red');
                        plot3(...
                        center(1) + [0 pca_dirs(1,2)],...
                        center(2) + [0 pca_dirs(2,2)],...
                        center(3) + [0 pca_dirs(3,2)],...
                        '-','LineWidth',3,'Color','green');
                        plot3(...
                        center(1) + [0 pca_dirs(1,3)],...
                        center(2) + [0 pca_dirs(2,3)],...
                        center(3) + [0 pca_dirs(3,3)],...
                        '-','LineWidth',3,'Color','blue');
                        hold off
                    end
                end
                
                % Option so save figure (set @ beginning of function)
                if((strcmp(grouping_criterion, 'plot_single')) && save_figure)
                    filename = ['Instrument_trajectory_p', num2str(p_id),'.fig'];
                    savefig(gcf, filename);
                    obj.Message(sprintf("Figure saved as '%s'.", filename));
                end
            end
            if(strcmp(grouping_criterion, 'plot_single'))
                obj.Message("All entry points (plot_single) plotted.");
            end
            
            % Plot 3d entry points for all probands together.
            % Also plot the 2d ellipsis mapping.
            if strcmp(grouping_criterion, 'plot_all')
                obj.Message("> Plotting entry point map...");
                
                if(debug_mark_gertzbein_c_to_e)
                    % Debug/Setting: Mark bad Gertzbein/Robbins with circle
                    % -> Load Evaluation of screw classifications
                    T_eval_data = obj.SyntrackIo_ref.evaluation_data_table_;
                end
                
                % Manually identified entry points.
                hold on
                for p_id = obj.proband_ids_
                    group = groups{p_id};
                    ep_color = 'green';
                    if group == "novice"
                        ep_color = 'blue';
                    end
                    for try_id = 1:2
                        corrected_entry_point = path_bone_entered_coordinates{p_id, try_id};
                        plot3(...
                            corrected_entry_point(1),...
                            corrected_entry_point(2),...
                            corrected_entry_point(3),...
                            '.','MarkerSize', 15, 'Color', ep_color);
                        
                        % Debug/Setting: Mark bad Gertzbein/Robbins with circle
                        % (LineWidth correlated with Class)
                        if(debug_mark_gertzbein_c_to_e)                           
                            gertzbein_class = string(T_eval_data.gertzbein_class(T_eval_data.p_id == p_id & T_eval_data.try_id == try_id));
                            color = 'magenta';
                            linewidth = 0.1;
                            
                            switch(gertzbein_class)
                                case "A"
                                    color = 'none';
                                case "B"
                                    linewidth = 1.0;
                                    fprintf("p%i, try%i: class %s\n", p_id, try_id, gertzbein_class);
                                case "C"
                                    linewidth = 1.5;
                                    fprintf("p%i, try%i: class %s\n", p_id, try_id, gertzbein_class);
                                case "D"
                                    linewidth = 2.0;
                                    fprintf("p%i, try%i: class %s\n", p_id, try_id, gertzbein_class);
                                case "E"
                                    linewidth = 2.5;
                                    fprintf("p%i, try%i: class %s\n", p_id, try_id, gertzbein_class);
                            end
                            plot3(...
                            corrected_entry_point(1),...
                            corrected_entry_point(2),...
                            corrected_entry_point(3),...
                            'o','MarkerSize', 12, 'LineWidth', linewidth, 'Color', color);
                        end
                    end
                end
                hold off;
                
                % Option to save figure
                if(strcmp(grouping_criterion, 'plot_all') && save_figure)
                    filename = ['Instrument_EP_map_p', num2str(obj.proband_ids_(1)),'-p',num2str(obj.proband_ids_(end)),'.fig'];
                    savefig(gcf, filename);
                    obj.Message(sprintf("Figure saved as '%s'.", filename));
                end
                
                % Plot ideal trajectories (EP ideal -> Pedicle Center)
                if (debug_show_ideal_trajectories)
                    hold on;
                    for vec_idx = 1:6
                        %ideal_trajectory_vectors{vec_idx} = ComputeTrajectoryVector(entry_points_ideal_array(vec_idx,:)', pedicle_centers(vec_idx,:));
                        point1 = entry_points_ideal_array(vec_idx,:)';
                        point2 = pedicle_centers(:,vec_idx);
                        
                        x1 = point1(1);
                        x2 = point2(1);
                        y1 = point1(2);
                        y2 = point2(2);
                        z1 = point1(3);
                        z2 = point2(3);
                        
                        nx = x2 - x1;
                        ny = y2 - y1;
                        nz = z2 - z1;
                        len = 2.5;
                        xx = [x1 - len*nx, x2 + len*nx];
                        yy = [y1 - len*ny, y2 + len*ny];
                        zz = [z1 - len*nz, z2 + len*nz];
                        
                        plot3(xx, yy, zz, '-', 'LineWidth', 3, 'Color', 'yellow');
                    end
                    hold off;
                end
                
                T_entry_point_data = obj.ComputeEntryPointIdealViewPositionsAndDistances(T_entry_point_data);                             
                
                % Saving results into table with pedicle ids and so on
                obj.results_ep_data_table_ = T_entry_point_data;
                disp(T_entry_point_data);
                
                % Saving parts of the results into result table
                var_names_evaluated = {'vertebra', 'side', 'ep_xyz_ideal_view_mm', 'ep_2d_distance_ap_mm', 'ep_2d_distance_axial_mm'};
                if(isempty(obj.results_))
                    obj.results_ = obj.results_ep_data_table_(:,[{'p_id', 'try_id', 'group'},var_names_evaluated]);
                else
                    T_entry_point_data = sortrows(T_entry_point_data, 'try_id');
                    obj.results_ = join(T_entry_point_data(:,[{'p_id', 'try_id', 'group'},var_names_evaluated]), obj.results_, 'Keys', {'p_id', 'try_id', 'group'}, 'KeepOneCopy', var_names_evaluated);
                end
                
                obj.Message("Results of Entry Point Mappings of original and ideal view saved into 'results_ep_data_table_' and joined into 'results_'.");
                obj.Message("Entry point map plotted (grouped by plot_all).");
            end
        end
        
        function success = EvaluateRetries(obj)
            database = obj.SyntrackIo_ref.clean_data_table_;
            definitions;
            obj.Message("> Evaluating events 'retry' and 'bone_entered' after event 'skin_cut'..");
            fprintf("p_id\tt1\tt2\n");
            for p_id = 1:22
                fprintf("p%2i:", p_id);
                for try_id = 1:2
                    start_search_idx = 0;
                    idx_count = 0;
                    skin_cut_event_idx = find(database{p_id,try_id}.label == label_numbers.cut);
                    if(skin_cut_event_idx == 0) 
                        bone_entered_event_idx = find(database{p_id,try_id}.label == label_numbers.enter_bone);
                        start_search_idx = bone_entered_event_idx(1);
                    else
                        start_search_idx = skin_cut_event_idx(1);
                    end
                    retries_bone_entered_idx = find(database{p_id,try_id}.label == label_numbers.enter_bone | database{p_id,try_id}.label == label_numbers.retry);
                    count_retries_bone = length(retries_bone_entered_idx(retries_bone_entered_idx > start_search_idx));
                    fprintf("\t%i", count_retries_bone);
                end
                fprintf("\n");
            end
            obj.Message("Evaluation done.");
        end
        
        function success = EvaluateNeedleAngulation(obj)
            % Two objections here: 
            % 1) Compute amount of angulation via AoMs
            % 2a) Compute angle at event 'bone entered' and calculate,
            %    whether (initial) trajectory would be within pedicle
            % 2b) Compute angle at manually identified EP (definitions) and
            %    calculate deviation of trajectory from final position
            % 2c) Compute screw angulation (ml) for later comparison
            % 3) Compare with final angle and trajectory of placed screw
            %    (amount of angle correction within xz and yz planes)
            % 4) Compare with 'ideal' trajectory (yellow line in plots)
            % 
            % How to be done:
            % 1) Get EPs from 'EvaluateNeedleEntryPoints'.
            % 2) Get Angulation via FA's computations
            % 3) Calculate differences of each projection to final screw
            % position
            % 4) Get coordinates of ideal trajectory and project onto
            % planes
            
            save_results_to_extra_table = false;
            
            obj.Message("> Computing angles @ labels ('cortical perforation') and @ manual identified EPs..");
            
            definitions;
            proband_ids = obj.proband_ids_;
            
            if(isempty(obj.angulation_relative_{1,1}))
                obj.Message("No angulation data found. Executing 'ComputeInstrumentAngulation'.");
                obj.ComputeInstrumentAngulation();
            else
                obj.Message("Instrument angulation data found. Using data..");
            end
            
            % pre-allocation
            var_names_std = {'p_id', 'try_id'};
            var_names_evaluated = {'angle_rel_label_ml', 'angle_rel_label_cc', 'angle_rel_ep_ml', 'angle_rel_ep_cc', 'angle_abs_label_ml', 'angle_abs_label_cc', 'angle_abs_ep_ml', 'angle_abs_ep_cc', 'angle_diff_label_ep_ml', 'angle_diff_label_ep_cc', 'angle_abs_screw_ml'};
            var_names = [var_names_std var_names_evaluated];
            var_types(1:length(var_names)) = {'double'};
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);
            row_no = 1;
            
            for p_id = proband_ids
                for try_id = 1:2
                    database = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
                    
                    % gettings angle arrays
                    angulation_relative = obj.angulation_relative_{p_id, try_id};                   
                    angulation_absolute = obj.angulation_absolute_{p_id, try_id};
                    
                    angulation_relative_medio_lateral = angulation_relative(:,1);
                    angulation_relative_cranio_caudal = angulation_relative(:,2);
                    angulation_absolute_medio_lateral = angulation_absolute(:,1);
                    angulation_absolute_cranio_caudal = angulation_absolute(:,2);
                    
                    % 2a) Angulation @ 'bone entered' label
                    bone_entered_labels = find(database.label == annotation_label.enter_bone);
                    angle_rel_label_ml = angulation_relative_medio_lateral(bone_entered_labels(end));
                    angle_rel_label_cc = angulation_relative_cranio_caudal(bone_entered_labels(end));
                    angle_abs_label_ml = angulation_absolute_medio_lateral(bone_entered_labels(end));
                    angle_abs_label_cc = angulation_absolute_cranio_caudal(bone_entered_labels(end));
                    
                    % 2b) Angulation @ manually identified 'bone entered'
                    % getting idx for cortical perforation (ep_perf) from
                    % phasesEvaluator
                    idx_ep_perf_pos = obj.SyntrackIo_ref.results_.PhasesEvaluator.phases_indices_{p_id,try_id}.bone_perf_ep;
                    if(isempty(idx_ep_perf_pos) && obj.SyntrackIo_ref.flag_phases_evaluation_done == true)
                        error('No index of cortical perforation found! Please check PhasesEvaluator results!');
                    elseif(obj.SyntrackIo_ref.flag_phases_evaluation_done == false)
                        obj.Message("No index of cortical peforation found. Please execute PhasesEvaluator first.");
                    end
                    
                    % 2c) Angulation @ end position = screw angulation
                    angle_abs_screw_ml = angulation_absolute_medio_lateral(end);
                    
                    angle_rel_ep_ml = angulation_relative_medio_lateral(idx_ep_perf_pos);
                    angle_rel_ep_cc = angulation_relative_cranio_caudal(idx_ep_perf_pos);
                    angle_abs_ep_ml = angulation_absolute_medio_lateral(idx_ep_perf_pos);
                    angle_abs_ep_cc = angulation_absolute_cranio_caudal(idx_ep_perf_pos);
                    
                    angle_diff_label_ep_ml = angle_rel_ep_ml-angle_rel_label_ml;
                    angle_diff_label_ep_cc = angle_rel_ep_cc-angle_rel_label_cc;
                    
                    T_newrow = {p_id, try_id, angle_rel_label_ml, angle_rel_label_cc, angle_rel_ep_ml, angle_rel_ep_cc, angle_abs_label_ml, angle_abs_label_cc, angle_abs_ep_ml, angle_abs_ep_cc, angle_diff_label_ep_ml, angle_diff_label_ep_cc, angle_abs_screw_ml};
                    T_results(row_no,:) = T_newrow;
                    row_no = row_no + 1;
                end
            end
            T_results = AssignGroups(obj, T_results);
            obj.Message("Computation done.");
            
            % Correction for probands who startet at wrong side
            obj.CorrectNeedleAngulation(T_results);
            
            disp(T_results);
            
            if(isempty(obj.results_))
                obj.results_ = T_results;
            else
                obj.results_ = join(T_results, obj.results_, 'Keys', {'p_id', 'try_id', 'group'}, 'KeepOneCopy', var_names_evaluated);
            end
            
            obj.Message("Resulting angles saved into 'results_'.");
            if(save_results_to_extra_table)
                writetable(T_results, 'InstrumentEvaluator_results_angles.xlsx');
                obj.Message("Results saved into 'InstrumentEvaluator_results_angles.xlsx'.");
            else
                obj.Message("Table not saved to .xlsx. Use option at the beginning of the function.");
            end
        end
        
        function success = PlotNeedleAngulationEP(obj)
            % 3D-Plot a cylinder at the EP position with initial rotation
            % and thickness of the used screw
            % 3D-Plot a cylinder at the final position with final rotation
            % to visualize angulations in the same plot
            % 2D-Plotting of angulations (in the XY, YZ, XZ?) -> see papers
            % Plotting comulative (integral) angulation
        end
        
        function success = GetAOMIndices(obj, arg_debug, val_debug, arg_save, val_save)
            if((arg_debug == "debug") && (val_debug == true))
                % Setting switches for debug
                flag_show_fft_spectrum = false;
                flag_show_median_mean_plot = false;
                flag_show_filter_comparison = false;
                flag_show_filtered_velocity = true;
                flag_compare_with_original_data = false;
                flag_show_changepoints = false;
            else
                flag_show_fft_spectrum = false;
                flag_show_median_mean_plot = false;
                flag_show_filter_comparison = false;
                flag_show_filtered_velocity = false;
                flag_compare_with_original_data = false;
                flag_show_changepoints = false;
            end
            
            if((arg_save == "savefigures") && (val_save == true))
                save_figures = true;
            else
                save_figures = false;
            end
                        
            database = obj.SyntrackIo_ref.clean_data_table_;
            proband_ids = obj.proband_ids_;
            
            % pre-allocation
            T_results_idxs = cell(length(proband_ids),2);    
            var_names = {'p_id', 'try_id', 'aoms_large', 'aoms_small'};
            var_types(1:length(var_names)) = {'double'};
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);
            row_no = 1;
            
            obj.Message("> Starting evaluation of Area of Movements (AoMs)..");
            
            for p_id = proband_ids               
                for try_id = 1:2
                    data_in_x = database{p_id,try_id}.datetime_relative;
                    SampleRate = 24;
                    
                    poses = database{p_id,try_id}.instrument_pose;
                    positions = [poses.pos];
                    positions = reshape(positions,3,[]);
                    x_pos_final = positions(1,end);
                    y_pos_final = positions(2,end);
                    z_pos_final = positions(3,end);
                    x_pos_relative = (positions(1,:) - x_pos_final)*100;
                    y_pos_relative = (positions(2,:) - y_pos_final)*100;
                    z_pos_relative = (positions(3,:) - z_pos_final)*100;
                    
                    velocity_x_pos_relative = [0, diff(x_pos_relative)];
                    velocity_y_pos_relative = [0, diff(y_pos_relative)];
                    velocity_z_pos_relative = [0, diff(z_pos_relative)];
                    
                    % Velocity in 3D space:
                    velocity_pos_relative_3d = sqrt(velocity_x_pos_relative.^2+velocity_y_pos_relative.^2+velocity_z_pos_relative.^2);
                    velocity_pos_relative_xy = sqrt(velocity_x_pos_relative.^2+velocity_y_pos_relative.^2);
                    velocity_pos_relative_z = sqrt(velocity_z_pos_relative.^2);
                    
                    data_in_y = velocity_pos_relative_3d;
                    
                    % Retime and interpolate to fixed Sample Rate
                    TT = timetable(data_in_x, data_in_y');
                    TT = sortrows(TT);
                    TT_retimed = retime(TT, 'regular', 'pchip', 'SampleRate', 24);
                    data_in_x = TT_retimed.data_in_x;
                    data_in_y = TT_retimed.Var1;
                    
                    % Detect AoMs
                    high_peaks_cut_off = data_in_y;
                    threshold_max_peaks = mean(data_in_y) * 50;
                    high_peaks_cut_off(high_peaks_cut_off > threshold_max_peaks) = threshold_max_peaks;
                    
                    dig_filt = designfilt('lowpassfir', 'PassbandFrequency', 0.5, 'StopbandFrequency', 8, 'PassbandRipple', 1, 'StopbandAttenuation', 60, 'SampleRate', SampleRate);
                    data_in_y_lowpass_3d = filtfilt(dig_filt, high_peaks_cut_off);
                    % cut-off for waves < 0
                    data_in_y_lowpass_3d(data_in_y_lowpass_3d < 0) = 0;
                    % calc movmean for large AoMs
                    mean_window_size = 24*2; % 24 Samples = 1 sec
                    data_in_y_movmean = movmean(data_in_y, mean_window_size);
                    % Threshold values for detection of AoMs
                    threshold_lp = 0.024; %blue
                    threshold_mean = 0.018; %red
                    data_changepts_lp = zeros(1, length(data_in_y_lowpass_3d));
                    data_changepts_mean = zeros(1, length(data_in_y_lowpass_3d));
                    for eval_idx = 1:length(data_in_y_lowpass_3d)
                        % Small AoMs
                        if (data_in_y_lowpass_3d(eval_idx) > threshold_lp)
                            data_changepts_lp(eval_idx) = 1;
                        end
                        % Large AoMs
                        if (data_in_y_movmean(eval_idx) > threshold_mean)
                            data_changepts_mean(eval_idx) = 1;
                        end
                    end
                    small_AoMs_idxs = find(diff(data_changepts_lp));
                    large_AoMs_idxs = find(diff(data_changepts_mean));
                    %small_AoMs_count = length(small_AoMs_idxs)/2;
                    %large_AoMs_count = length(large_AoMs_idxs)/2;
                    
                    if(data_changepts_lp(1) == 1)
                        small_AoMs_idxs = [1, small_AoMs_idxs];
                        %small_AoMs_count = small_AoMs_count + 1;
                    end
                    if(data_changepts_mean(1) == 1)
                        large_AoMs_idxs = [1, large_AoMs_idxs];
                        %large_AoMs_count = large_AoMs_count + 1;
                    end
                                    
                    % show moving mean and lowpass filtered velocity(3d)
                    if(flag_show_filtered_velocity)                      
                        figure;
                        plot(data_in_x, data_in_y, 'Color', [0.75 0.75 0.75], 'Linewidth', 0.5);

                        % median filtering
                        medfilt1_grade = 25;
                        data_in_y_medfilt = medfilt1(data_in_y, medfilt1_grade);

                        hold on;
                        plot(data_in_x, data_in_y_lowpass_3d, 'b', data_in_x, data_in_y_movmean, 'r', data_in_x, data_in_y_medfilt, 'g', 'Linewidth', 1.25);
                        hold off;
                        title(['p', num2str(p_id), ' try', num2str(try_id)]);
                        legend('original','lowpass filtered', 'mov mean', 'medfilt1');
                        if(save_figures)
                            saveas(gcf, ['AoM_velocity3d_p', num2str(p_id), ' try', num2str(try_id)]);
                        end
                    end
                    
                    % show filtered velocity and detected AoMs with
                    % original data
                    if(flag_compare_with_original_data)
                        figure;
                        % plotting lowpass filtered data and moving mean
                        plot(data_in_x, data_in_y, 'black', data_in_x, data_in_y_lowpass_3d, 'blue', data_in_x, data_in_y_movmean, 'red', 'LineWidth', 1.25);
                        ylim([0 15]);                  

                        % Drawing thresholds
                        hold on;
                        x_val = data_in_x;
                        plot([x_val(1), x_val(end)], [threshold_lp, threshold_lp], 'Color', 'blue', 'LineStyle', '--');
                        plot([x_val(1), x_val(end)], [threshold_mean, threshold_mean], 'Color', 'red', 'LineStyle', '--');
                        plot([x_val(1), x_val(end)], [threshold_max_peaks, threshold_max_peaks], 'Color', 'red', 'LineStyle', '--');
                        hold off;
                        
                        if(flag_show_changepoints)
                        hold on;
                        for changept_idx = find(diff(data_changepts_lp))+1
                            x_val = data_in_x(changept_idx);
                            plot([x_val, x_val], [-0.1, 0.5], 'Color', 'blue','Linewidth', 1);
                        end
                        if(data_changepts_lp(1) == 1)
                            plot([data_in_x(1), data_in_x(1)], [-0.1, 1.0], 'Color', 'red','Linewidth', 1);
                        end
                        hold off;
                        
                        hold on;
                        for changept_idx = find(diff(data_changepts_mean))+1
                            x_val = data_in_x(changept_idx);
                            plot([x_val, x_val], [-0.1, 1.0], 'Color', 'red','Linewidth', 1);
                        end
                        if(data_changepts_mean(1) == 1)
                            % case: startpoint is not zero velocity..
                            plot([data_in_x(1), data_in_x(1)], [-0.1, 1.0], 'Color', 'red','Linewidth', 1);
                        end
                        hold off;
                        end
                    end
                    
                    % saving indices in cell of tables
                    T_results_idxs{p_id, try_id} = table(small_AoMs_idxs, large_AoMs_idxs, 'VariableNames', {'aoms_small_indices', 'aoms_large_indices'});
                end
            end
            
            obj.results_aoms_ = T_results_idxs;
            obj.Message("Result table (n x 2 cell) with aom indices saved into 'results_aoms_'.");
            obj.SyntrackIo_ref.flag_aoms_evaluation_done_ = true;
        end
        
        function [instr_angle_absolute, instr_angle_relative] = ComputeInstrumentAngulation(obj)
            database = obj.SyntrackIo_ref.clean_data_table_;
            angulation_screw_cell = cell(22,2);
            angulation_absolute_cell = cell(22,2);
            angulation_relative_cell = cell(22,2);
            
            obj.Message("> Computing instrument angulation to x / y / z planes...");
            
            for p_id = obj.proband_ids_
                for try_id = 1:2
                    
                    obj.Message("> evaluating p " + num2str(p_id) + ", try " + num2str(try_id) + "..");
                    poses = database{p_id, try_id}.instrument_pose;
                    rotations = {poses.rot};
                    
                    % pre-allocation
                    instr_angle_x = zeros(1,length(rotations));
                    instr_angle_y = zeros(1,length(rotations));
                    instr_angle_z = zeros(1,length(rotations));
                    
                    % Computing angulations:
                    % Alternative representation using the central
                    % instrument axis ("up_vector"), pointing from the
                    % instrument tip towards the handle.
                    a = 1;
                    for R = rotations
                        up_vector = R{1}(3,:);
                        % Longitudinal axis pointing from inferior to
                        % superior (=-x in the chai3d frame).
                        rotation_around_longitudinal_patient_axis = ...
                            atan2(up_vector(3), -up_vector(2));
                        
                        % Sagittal axis pointing from right to left (=-y
                        % in the chai3d frame).
                        rotation_around_sagittal_patient_axis = ...
                            atan2(up_vector(3), up_vector(1));
                        
                        rot_mesh_to_chai3d = R{1};
                        [~, ~, rot_angle_around_z_mesh] = dcm2angle(rot_mesh_to_chai3d, 'XYZ');
                        
                        instr_angle_x(a) = rotation_around_longitudinal_patient_axis;
                        instr_angle_y(a) = rotation_around_sagittal_patient_axis;
                        instr_angle_z(a) = rot_angle_around_z_mesh;
                        a = a + 1;
                    end
                    
                    % defining axes
                    instr_angle_absolute{1} = instr_angle_x' * (180.0/pi); %x
                    instr_angle_absolute{2} = instr_angle_y' * (180.0/pi); %y
                    instr_angle_absolute{3} = instr_angle_z' * (180.0/pi); %z
                    
                    % Calculating relative angles
                    for i = 1:3
                        angle_final{i} = instr_angle_absolute{i}(end);
                        instr_angle_relative{i} = (instr_angle_absolute{i} - angle_final{i});
                    end
                    
                    angulation_screw_cell{p_id, try_id} = [angle_final{1} angle_final{2} angle_final{3}];
                    angulation_relative_cell{p_id, try_id} = [instr_angle_relative{1} instr_angle_relative{2} instr_angle_relative{3}];
                    angulation_absolute_cell{p_id, try_id} = [instr_angle_absolute{1} instr_angle_absolute{2} instr_angle_absolute{3}];
                end
            end
            obj.Message("Computation done.");
            
            % Saving to class
            obj.angulation_absolute_ = angulation_absolute_cell;
            obj.angulation_relative_ = angulation_relative_cell;
            obj.angulation_screw_ = angulation_screw_cell;
            obj.Message("Absolute Instrument Angulation (degrees) saved into 'angulation_screw_(x y z)', 'angulation_absolute_(x y z)', 'angulation_relative_(x y z)'.");
        end
        
        function success = SaveResults2Tables(obj)
            T = obj.results_;
            try
                warning('off', 'MATLAB:xlswrite:AddSheet');
                writetable(T,obj.kFileNameResultsXls, 'sheet', obj.kFileNameResultsSheetName);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsXls + "' with worksheet named after actual date.");
                
                writetable(T,obj.kFileNameResultsTxt);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsTxt + "'.");
                success = true;
            catch
                success = false;
                obj.Message("Error saving results!");
            end
        end
                      
        function success = SaveResults2Io(obj)
            svd = obj.results_aoms_;
            svd2 = obj.angulation_absolute_;
            svd3 = obj.angulation_relative_;
            svd4 = obj.angulation_screw_;
            svd5 = obj.results_angles_;
            svd6 = obj.results_ep_data_table_;        
            
            T = obj.results_;
            results_struct = struct();
            results_struct.aoms_indices_ = svd;
            results_struct.angulation_absolute = svd2;
            results_struct.angulation_relative = svd3;
            results_struct.angulation_screw = svd4;
            results_struct.results_angles = svd5;
            results_struct.results_ep_data_table = svd6;
            
            results_struct.data_table_ = T;
            obj.SyntrackIo_ref.results_.InstrumentEvaluator = results_struct;
            obj.Message("Data results from '" + obj.kClassName + "' saved into Io.results_.");
        end
    end
    
    methods (Access = private)
        
        function T_ep_data = ComputeEntryPointIdealViewPositionsAndDistances(obj, T_ep_data)
            % Calculate relative position of EPs to ideal EPs in ideal view
            % -- Calc EP position in ideal a.p. view for for each vertebra
            % and save into table
            definitions;
            entry_point_data_table = T_ep_data;
            
            obj.Message("> Starting Mapping EPs from PCA frames into ideal view frames..");
            for pedicle_id = 1:6
                ep_orig_coords = entry_point_data_table.ep_xyz_orig(entry_point_data_table.pedicle_id == pedicle_id,:)';
                num_mapped_eps = size(ep_orig_coords,2);
                
%                 % Map to coordinate frame of each pedicle ring.
%                 pca_dirs = pedicle_principal_directions{pedicle_id};
%                 center = pedicle_centers(:, pedicle_id);
%                 entry_points_in_pca_world = ...
%                     inv([[pca_dirs center];[0 0 0 1]]) * ...
%                     [ep_orig_coords; ones(1, num_mapped_eps)];
%                 
%                 pca_sdevs = sqrt(pedicle_principal_variances{pedicle_id});
%                 pca_entry_points_xMedLat_yCaudCran =[...
%                     entry_points_in_pca_world(2,:) / pca_sdevs(2);
%                     -entry_points_in_pca_world(1,:) / pca_sdevs(1)];
                
                % Rotating/map the original coordinate frame to an ideal
                % view coordinate frame (dorsal a.p. view with
                % parallel vertebra plates and rotated proc. spin.)
                % => Optimal view per level at ideal EP
                level = ceil(pedicle_id / 2) + 1;      % counts 2, 3, 4 for Lumbar2, 3, 4
                % getting ideal views (flouro) from definitions
                orbital_rad = pi * garm_ideal_orbital(level) / 180.0;
                tilt_rad = pi * garm_ideal_tilt(level) / 180.0;
                % This is correct for the right side (coordinate frame)
                new_x = [0.0, 1.0, 0.0]';
                new_y = [-1.0, 0.0, 0.0]';
                new_z = [0.0, 0.0, 1.0]';
                rotation_matrix = [new_x, new_y, new_z];
                
                % Creating a Direction Cosine Matrix (DCM) for rotation
                from_ideal_mapping_frame_to_lvl_specific = ...
                    angle2dcm(orbital_rad, tilt_rad, 0.0, 'YXZ');
                
                optimal_view_rotation_matrix =...
                    rotation_matrix * from_ideal_mapping_frame_to_lvl_specific;
                % Computing centers of ideal entry points ('entry_points_ideal_array' from definitions)
                center = entry_points_ideal_array(pedicle_id, :)';
                % Considering view with tranlation for each pedicle (x-translation)
                ep_ideal_view_and_ep_2d_xMedLat_yCaudCran_zDorsVentr = ...
                    inv([[optimal_view_rotation_matrix center];[0 0 0 1]]) * ...
                    [ep_orig_coords; ones(1, num_mapped_eps)];
                
                % saving ep coordinates with ideal view and 2d
                % distance into table
                ep_ids = entry_point_data_table.ep_id(entry_point_data_table.pedicle_id == pedicle_id);
                for i_ep = 1:num_mapped_eps
                    entry_point_data_table.ep_xyz_ideal_view_mm(entry_point_data_table.ep_id == ep_ids(i_ep),:) = 1000.0 * ep_ideal_view_and_ep_2d_xMedLat_yCaudCran_zDorsVentr(1:3,i_ep)';
                    dist_ap_mm = 1000.0 * norm(ep_ideal_view_and_ep_2d_xMedLat_yCaudCran_zDorsVentr(1:2,i_ep));
                    dist_axial_mm = 1000.0 * norm(ep_ideal_view_and_ep_2d_xMedLat_yCaudCran_zDorsVentr(2:3,i_ep));
                    entry_point_data_table.ep_2d_distance_ap_mm(entry_point_data_table.ep_id == ep_ids(i_ep)) = dist_ap_mm;
                    entry_point_data_table.ep_2d_distance_axial_mm(entry_point_data_table.ep_id == ep_ids(i_ep)) = dist_axial_mm;
                end
            end
            % -- Calculation done
            T_ep_data = entry_point_data_table;
            obj.Message("Mapping done.");
        end
        
        function T_corrected_results = CorrectNeedleAngulation(obj, T_results_angles)
            T = T_results_angles;
            obj.Message("Trying to correct angles for probands who chose the wrong side first..");
            
            definitions;
            wrong_proband_ids = proband_ids_start_left_instead_of_right;
            
            for p_id = wrong_proband_ids
                % Correct only medio-lateral relative angles and differences
                %T.angle_abs_ep_ml(T.p_id == p_id) = -T.angle_abs_ep_ml(T.p_id == p_id);
                %T.angle_abs_label_ml(T.p_id == p_id) = -T.angle_abs_label_ml(T.p_id == p_id);
                T.angle_diff_label_ep_ml(T.p_id == p_id) = -T.angle_diff_label_ep_ml(T.p_id == p_id);
                T.angle_rel_ep_ml(T.p_id == p_id) = -T.angle_rel_ep_ml(T.p_id == p_id);
                T.angle_rel_label_ml(T.p_id == p_id) = -T.angle_rel_label_ml(T.p_id == p_id);
            end
            
            obj.Message("Corrections applied for all medio-lateral angles.");
            T_corrected_results = T;
        end
       
        function PlotTrocarPath(obj, try12ofproband_cells, proband_id)
            % Plots trocar path with 2 trys of _one_ proband.
            
            obj.Message("> Plotting trocar path of p" + proband_id + "..");
            
            trajectory_colors = {'blue','green'};
            for try_idx = 1:2
                poses = try12ofproband_cells{try_idx}.instrument_pose;
                % getting positions and rotations
                positions = [poses.pos];
                positions = reshape(positions,3,[]);

                pediculation_positions = positions(:,:);
                hold on
                plot3(...
                    pediculation_positions(1,:),...
                    pediculation_positions(2,:),...
                    pediculation_positions(3,:),...
                'color', trajectory_colors{try_idx});
                hold off
            end
            
            definitions;
            lvl = lumbar_vert_lvl_of_proband(proband_id);
            axial_slice_width = 0.06;
            axis equal
            switch lvl
                case 2
                     axis([-0.075+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 3
                     axis([-0.0315+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 4
                     axis([0.005+[0 axial_slice_width] -0.06 0.06 0.01 0.11]);
            end
            gcf;

            % set title for 'plot_single' here
            title_header = sprintf('Entry point map of proband %i, "plot_single"', proband_id);
            title_color_explanation = '(1st try = blue, 2nd try = green)';

            title({title_header, title_color_explanation}, 'color', 'white', 'Interpreter', 'none');
            
%             % Use annotations to crop the paths. Plot one path per attempt.
%             pediculation_positions = [];
%             cut_framenumbers = annotation_events.frame_number(...
%                 annotation_label.cut == annotation_events.label);
%             while (numel(cut_framenumbers) ~= numel(experiment.endpoints))
%                 latest_endpoints = [];
%                 for i = 1:(numel(annotation_events.label) - 1)
%                     if (annotation_events.label(i) == annotation_label.end &&...
%                             annotation_events.label(i+1) == annotation_label.end)
%                         latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i+1));
%                     elseif (annotation_events.label(i) == annotation_label.end)
%                         latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i));
%                     elseif (i == (numel(annotation_events.label) - 1) && ...
%                             annotation_events.label(i+1) == annotation_label.end)
%                         latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i+1));
%                     end
%                 end
%                 experiment.endpoints = unique(latest_endpoints);
%                 
%                 earliest_cutpoints = [];
%                 candidates = [];
%                 for endpoint_idx = 1:numel(experiment.endpoints)
%                     for i = 1:numel(cut_framenumbers)
%                         if (cut_framenumbers(i) < experiment.endpoints(endpoint_idx))
%                             candidates = cat(1,candidates,cut_framenumbers(i));
%                         end
%                     end
%                     if (isempty(earliest_cutpoints))
%                         earliest_cutpoints = cat(1,earliest_cutpoints,min(candidates));
%                     else
%                         for j = 1:numel(candidates)
%                             if(candidates(j) > experiment.endpoints(endpoint_idx-1))
%                                 earliest_cutpoints = cat(1,earliest_cutpoints,candidates(j));
%                                 break;
%                             end
%                         end
%                     end
%                 end
%                 cut_framenumbers = unique(earliest_cutpoints);
%             end
%             for try_idx = 1:numel(experiment.endpoints)
%                 cut_idx = cut_framenumbers(try_idx);
%                 end_idx = experiment.endpoints(try_idx);
%                 pediculation_positions = positions(:,cut_idx:end_idx);
%                 plot3(pediculation_positions(1,:), pediculation_positions(2,:), pediculation_positions(3,:))
%             end
%             % axis image vis3d
%             % xlabel('X-Achse')
%             % ylabel('Y-Achse')
%             % zlabel('Z-Achse')
%             title(['Trajektorie von Proband ' num2str(proband_id)...
%                 ', file: ' filename_proband{proband_id}],...
%                 'interpreter', 'none')
%             
%             % TODO(FA): Plot the Bonemesh movement as well
%             % patient_poses = all_recordings.patient_pose;
%             % patient_positions = [patient_poses.pos];
%             % patient_positions = reshape(patient_positions,3,[]);
%             % % figure;
%             % plot3(patient_positions(1,:), patient_positions(2,:), patient_positions(3,:))
%             hold off

            obj.Message("p" + proband_id + ": Plotting done.");
        end
        
        function [entry_point_distance] = CalculateEntryPointDistance(obj, pm_handle_patch, entry_point_handle, proband_id)
            % Code ported from 'ScrewEvaluator'
            % Use this code to compute the entry point distances for each
            % attempt of a proband

            warning('not yet implemented!');
            
            %% Load and plot bone mesh of case 'mecanix'
            
            % 1. Alle objekte laden (mit richtiger skalierung und orientierung s. MeshLab)
            % 2. Position durch Lage des Beads korrigieren
            % 3. Rotation der Meshes
            % 4. Eintrittspunkte ideal laden (definitions)
            % 5. Eintrittspunkte manuell korrigiert laden (definitions)
            
            obj.Message("> Computing distance of Needle Entry Point relative to ideal EP for p" + proband_id + "..");
            
            % Defining meshes & Bead offset
            definitions;
            lvl = lumbar_vert_lvl_of_proband(proband_id);
            screw_mesh_path = './meshes/screw.obj';
            bead_offset_in_rai_coordinates = [0.144, 0.1277, 0.234];
            
            % Load bonemesh into figure.
            gertzbein_figure_handle = figure('Color','black');
            axiscolor = [0.75 0.75 0.75];
            set(gca,'Color','k','XColor',axiscolor,'YColor',axiscolor,'ZColor',axiscolor);
            
            % Load pedicle meshes.
            for p = 1:numel(pedicle_meshes)
                pedicle_mesh = loadawobj(pedicle_meshes{p});
                pm_handle_patch(p) = patch(...
                    'vertices', bsxfun(@minus,...
                    pedicle_mesh.v', bead_offset_in_rai_coordinates),...
                    'faces', pedicle_mesh.f3',...
                    'edgecolor', 'none',...
                    'facecolor', [1 0 0],...
                    'facealpha', 1);
                
                rotate(pm_handle_patch(p),[1 0 0], 90, [0 0 0])
                rotate(pm_handle_patch(p),[0 0 1], -90, [0 0 0])
            end
            
            % figure config
            camlight
            xlabel('X: inf->sup')
            ylabel('Y: links->rechts')
            zlabel('Z: ant->post')
            axial_slice_width = 0.06;
            axis equal;
            switch lvl
                case 2
                    % knapp
                    %         axis([-0.05 -0.025 -0.04 0.04 0.02 0.12]);
                    % mit discs
                    %         axis([-0.065 -0.025 -0.04 0.04 0.02 0.12]);
                    % full
                             axis([-0.075+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 3
                    % knapp
                    %         axis([-0.0135 0.015 -0.04 0.04 0.02 0.12]);
                    % mit discs
                    %         axis([-0.0165 0.015 -0.04 0.04 0.02 0.12]);
                    % full
                             axis([-0.0315+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 4
                    % knapp:
                    %         axis([0.029 0.055 -0.04 0.04 0.01 0.11]);
                    % mit discs:
                    %         axis([0.015 0.060 -0.04 0.04 0.01 0.11]);
                    % full
                             axis([0.005+[0 axial_slice_width] -0.06 0.06 0.01 0.11]);
            end
            bone_mesh_path = sprintf('./meshes/mecanix_nice_L%i.obj', lvl);
            bone_mesh = loadawobj(bone_mesh_path);
            bonemesh_handle_patch = patch(...
                'vertices', bsxfun(@minus, bone_mesh.v', bead_offset_in_rai_coordinates),...
                'faces', bone_mesh.f3',...
                'edgecolor', 'none',...
                'facecolor', [1 1 1],...
                'facealpha', 0.5);
            rotate(bonemesh_handle_patch,[1 0 0], 90, [0 0 0])
            rotate(bonemesh_handle_patch,[0 0 1], -90, [0 0 0])
            %axis image vis3d    % cropping view to visible meshes

            view(-90.4000, 5.6);
            % view all 3 pedicles:
            % axis([-0.07 0.06 -0.04 0.04 -0.01 0.12])
            
            % Plotting screw
            hold on
            
            % Creating temporary concat table (like a raw dataset..)
            dataset_table_concat = table();
            dataset_table_concat = [dataset_table{1};dataset_table{2}];
            
            poses = dataset_table_concat.instrument_pose;
            % getting positions and rotations
            positions = [poses.pos];
            positions = reshape(positions,3,[]);
            rotations = {poses.rot};
            % getting end indices
            end_indices = find(dataset_table_concat.label == label_numbers.end);
            % getting position and rotation of try 1 & 2
            pediculation_positions = positions(:,end_indices);
            pediculation_rotations = rotations(end_indices);
            % plot two markers for end positions
            plot3(pediculation_positions(1,:),...
                pediculation_positions(2,:),...
                pediculation_positions(3,:),...
                '.','MarkerIndices',[1 2],'MarkerSize',15,'Color','yellow');
            % plotting of screws at end positions
            screw_mesh_orig = loadawobj(screw_mesh_path);
            color_codes = {[0.1 0.4 1],[0 1 0]}; % 1st try = blue, 2nd = green
            screw_meshes = cell(1,2);
            % screwmesh_handle_patch = zeros(1,2);
            for try_idx = 1:2
                screw_meshes{try_idx} = screw_mesh_orig;
                % rotation of screw
                screw_meshes{try_idx}.v = pediculation_rotations{try_idx}' * screw_meshes{try_idx}.v;
                % positioning of screw
                screw_meshes{try_idx}.v = bsxfun(@plus,...
                    screw_meshes{try_idx}.v, pediculation_positions(:,try_idx));
                
                screwmesh_handle_patch(try_idx) = patch(...
                    'vertices', screw_meshes{try_idx}.v',...
                    'faces', screw_meshes{try_idx}.f3',...
                    'edgecolor', 'none',...
                    'facecolor', color_codes{try_idx},...
                    'facealpha', 0.5);
            end
            
            % Switch on the nicely interpolated light at the end in order to affect all
            % meshes in the figure.
            lighting gouraud
            
            title({sprintf('Proband ID: %i, vertebra: L%i',...
                proband_id, lumbar_vert_lvl_of_proband(proband_id)),...
                '(1st try = blue, 2nd try = green)'}, 'color', 'white');
            
            hold off
            
            obj.Message("Computation done.");
        end
                         
        function [table_w_groups] = AssignGroups(obj, table)
            % Assigns a column 'groups' to a result table.
            % Cave: Only valid for a connected series of probands
            % e.g. 2:5; not: 2,5,7 !
            
            definitions;
            probands = obj.proband_ids_;
            T = sortrows(table,'try_id');
            % Assigning exp/nov from definitions
            T.group = [groups_sorted_by_try_id(probands(1):probands(end)); groups_sorted_by_try_id(probands(1):probands(end))];
            T.group = categorical(T.group);
            T = T(:,[1,2,end,3:end-1]);
            table_w_groups = T;
        end
        
        function ideal_trajectory_vector = ComputeTrajectoryVector(obj, point1, point2, length_of_trajectory)
            if(nargin < 3)
                length_of_trajectory = 1000;
            end
            
            x1 = point1(1);
            x2 = point2(1);
            y1 = point1(2);
            y2 = point2(2);
            z1 = point1(3);
            z2 = point2(3);
            
            nx = x2 - x1;
            ny = y2 - y1;
            nz = z2 - z1;
            len = length_of_trajectory;
            xx = [x1 - len*nx, x2 + len*nx];
            yy = [y1 - len*ny, y2 + len*ny];
            zz = [z1 - len*nz, z2 + len*nz];
            
            ideal_trajectory_vector = [xx, yy, zz];
            %
            %H = plot3(xx, yy, zz);
        end
    end
end



