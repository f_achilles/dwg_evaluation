classdef SyntrackPlotter < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        raw_vs_clean_absolute_time_ = true;
    end
    
    properties (Constant)
        kClassName = 'SyntrackPlotter';
        kClassVersion = 'V1.1';
        % Changelog SyntrackPlotter.m
        % V1.1: Feat. 'PlotCharacteristics' -> debug option to use relative or absolute values
        kVerbose = true;
        kFlagWriteImagesToPNG = false;
        kFontSizeEventAnnotation = 8;
    end

    methods
        function obj = SyntrackPlotter(io_ref, probands)
            obj.SyntrackIo_ref = io_ref;
            
            % check for modified set of probands ids to evaluate (args)
            if exist('probands','var') && ~isempty(probands)
                obj.proband_ids_ = probands;
                
                % Formatted message output for evaluated probands
                if nnz(diff(diff(probands))) == 0
                    p_id_range_txt = ['p', num2str(probands(1)), ' - p', num2str(probands(end))];
                else
                    p_id_range_txt = strrep(num2str(probands),'  ',', ');
                end
                obj.Message("Initialized to plot data for a modified range or probands (" + p_id_range_txt + ").");
            else
                % Standard case without args: Plot all available data that
                % was loaded by SyntrackIo
                obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
                obj.Message("Initialized to plot data for p" + io_ref.proband_ids_(1) + " - p" + io_ref.proband_ids_(end) + ".");
            end
            
            obj.Message("Setting: WriteImagesToPNG_ == " + string(obj.kFlagWriteImagesToPNG) + ".");
            
            % Check for clean data
            if(obj.SyntrackIo_ref.flag_clean_data_successfully_loaded_ == false)
                obj.Message("No Clean data given. Cannot plot. Please execute CleanData() from class DataCleaner first.");
                warning('Error!');
                success = 'error';
            end
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for UseCases class are all working');
            else
                warning('Not all Tests for UseCases class are working. Please debug and fix.');
            end
        end
        
        % TODO (FA): Inherit from a Definitions class so that all our objects have access to them OR load definitions in ctor
        % already.
        % TODO (FA): Refactor the plotting function, so that constants like the annotation_events.colorcode are actually
        % stored as constant member variables in the class.
        % TODO (MM): Agree w/ FA -> Plotter should not need ref/handles to other classes
        
        function success = PlotRawVsCleanData(obj, cleaner_ref)
            % Plots a preview of Raw data | Raw w/ corrected labels | Clean data
            % This is how it works:
            % 1. Setting proband id (i)
            % 2. Plot raw data (top)
            % 3. Plot annotations from corrected labels to raw data (top)
            %   -> see which labels will be manipulated
            % 4. Plot raw data w corrected labels (from cleaner) to show effect (center)
            % 5. Plot clean data (which has been trimmed (at 'start','end','tech probs')
            %   -> show split data in 3rd and 4th plot (bottom)
            
            definitions;
            success = true;
            probands = obj.proband_ids_;
            
            % Loading and saving data references for easy access
            cleaning_specs = cleaner_ref.cleaning_specs_;
            raw_data_w_corr_labels = cleaner_ref.raw_data_w_corr_labels_;
            
            obj.Message("> Plotting Raw vs Clean Data including visualized annotations for p" + probands(1) + " - p" + probands(end) + "...");
            
            % using i because its better for reading..
            for i = obj.proband_ids_
                        
            % ****************************************************
            % PLOT Raw vs Clean data in Fullscreen
            figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
            %export_fig(['xray_p' num2str(i)]);
            
            % -------------------------------------------------------
            % Doing calculations and label extraction for RAW data:
            % 
            database = obj.SyntrackIo_ref.raw_data_;

            % Call function with args = database(proband)
            % Set switch for time counted from each start (per try)
            obj.raw_vs_clean_absolute_time_ = true;
            [annotation_events,num_events,experiment] = obj.CollectEventsStartEndpoints(database(i));

            % -----------------------------------------------------
            % Plot of RAW data
            subplot(3,2,[1,2]);
                plot(database(i).frame_nr, database(i).xray_event,...
                    'color', '[0.75,0.75,0.75]', 'Linewidth', 4);
                title(['P', num2str(i), ' Raw data w/ annotations for cleaning (durations calculated from first start)']);
                xlim([experiment.start,experiment.end]);
                ylim([0.1,1.1]);
                set(gca,'xtick',[]);
                hold on;
                % Adding annotation labels to plot
                last_x_pos = -51;    % for optimized readability
                last_event_idx = -2;
                for event_idx = 1:num_events
                    plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [0.1, 1.1],...
                        'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                        'Linewidth', 1.25);
                    % Event text.
                    txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(event_idx) ')'],...
                           ['@ ' annotation_events.time_readable{event_idx}]});

                    % Label position (always visible!)
                    if(annotation_events.label(event_idx) == 1)
                        % setting new position for label 'Start'
                        y_pos_of_text = 1;
                        y_pos_of_text_last = y_pos_of_text;
                    elseif(y_pos_of_text < 0.3) % if label is set too low (y-axis)
                        y_pos_of_text = 0.8;
                        y_pos_of_text_last = y_pos_of_text;
                    else
                        y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/6;
                        y_pos_of_text_last = y_pos_of_text;
                    end

                    % Visualizing CLEANING info loaded from SPECIFICATIONS
                    x_pos = annotation_events.frame_number(event_idx);
                    text(x_pos, y_pos_of_text, txt);
                    
                    % determine if the action for the label is delete/insert/correct (del/corr/ins)
                    % getting values from specs
                    cleaning_specs_value = cleaning_specs.value(cleaning_specs.p_id == i & cleaning_specs.label_no == event_idx);
                    cleaning_specs_action = string(cleaning_specs.action(cleaning_specs.p_id == i & cleaning_specs.label_no == event_idx));
                    cleaning_specs_label = char((cleaning_specs.label_name(cleaning_specs.p_id == i & cleaning_specs.label_no == event_idx)));
                    
                    if ~isempty(cleaning_specs_value)
                        if cleaning_specs_action == "del"
                            txt_spec = ['[X ', num2str(event_idx), ']'];
                        elseif cleaning_specs_action == "corr"
                            if duration(cleaning_specs_value) < 0
                                pointer = '<-';
                            else
                                pointer = '->';
                            end
                            txt_spec = ['[Corr (', num2str(event_idx), ') ', pointer ,']'];
                        elseif cleaning_specs_action == "ins"
                            txt_spec = ['[ins (', cleaning_specs_label ,') ->]'];
                        end
                        
                        % Verify readable annotations                   
                        no_of_xaxis_frames = experiment.end - experiment.start;
                        % 6 Percent distance appreciated !
                        if ((event_idx == last_event_idx + 1) && ((x_pos - last_x_pos) < 6*(no_of_xaxis_frames/100)))
                            x_pos = last_x_pos + 6*(no_of_xaxis_frames/100);
                        end
                        text(x_pos, 0.05, txt_spec);
                        last_x_pos = x_pos;
                        last_event_idx = event_idx;
                    end
                end
                hold off;
               
                % -------------------------------------------------------
                % Doing same calculations and label extraction for RAW DATA
                % w/ CORRECTED LABELS (if available)
                %
                if(cleaner_ref.flag_corr_raw_labels_executed_)
                    database = cleaner_ref.raw_data_w_corr_labels_;
                    
                    % Call function with args = database(proband)
                    % Set switch for time counted from each start (per try)
                    obj.raw_vs_clean_absolute_time_ = false;
                    
                    [annotation_events,num_events,experiment] = obj.CollectEventsStartEndpoints(database(i));
                    
                    % -----------------------------------------------------
                    % Plot of RAW DATA W/ CORR LABELS data below
                    subplot(3,2,[3,4]);
                    plot(database(i).frame_nr, database(i).xray_event,...
                        'color', '[0.75,0.75,0.75]', 'Linewidth', 4);
                    title(['P', num2str(i), ' Raw data w/ corrected labels (durations calculated relative for each attempt)']);
                    xlim([experiment.start,experiment.end]);
                    ylim([0.1,1.1]);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [0.1, 1.1],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Event text.
                        txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(event_idx) ')'],...
                            ['@ ' annotation_events.time_readable{event_idx}]});
                        
                        % Label position (always visible!)
                        if(annotation_events.label(event_idx) == 1)
                            % setting new position for label 'Start'
                            y_pos_of_text = 1;
                            y_pos_of_text_last = y_pos_of_text;
                        elseif(y_pos_of_text < 0.3) % if label is set too low (y-axis)
                            y_pos_of_text = 0.8;
                            y_pos_of_text_last = y_pos_of_text;
                        else
                            y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/6;
                            y_pos_of_text_last = y_pos_of_text;
                        end
                        
                        % setting text for label
                        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
                    end
                    set(gca,'xtick',[]);
                    hold off;
                else
                    obj.Message("p" + num2str(i) + ": No corrected labels given. Cannot plot.");
                end
                
                % -----------------------------------------------------
                % Plot of CLEAN_data{n,2} below to show final formatting
                if(cleaner_ref.flag_data_cleaned_)
                    % clean_data_ = struct of (n,2)
                    database = obj.SyntrackIo_ref.clean_data_;            
                    
                    for try_idx = 1:2
                        % Getting events and labels
                        [annotation_events,num_events,~] = obj.CollectEventsStartEndpoints(database{i, try_idx});
                        plot_pos = 4 + try_idx;
                        subplot(3,2,plot_pos);
                        
                        plot(database{i, try_idx}.datetime_relative, database{i, try_idx}.xray_event,...
                            'color', '[0.75,0.75,0.75]', 'Linewidth', 4);
                        title(['P', num2str(i), ' clean data - try no. ', num2str(try_idx)]);
                        
                        datetick('x','MM:SS', 'keepticks');
                        
                        xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                        ylim([0.1,1.1]);
                        hold on;
                        % Adding labels to plot
                        for event_idx = 1:num_events
                            plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [0.1, 1.1],...
                                'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                                'Linewidth', 1.25);
                            % Event text.
                            txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(event_idx) ')'],...
                                ['@ ' annotation_events.time_readable{event_idx}]});
                            
                            % Label position (always visible!)
                            if(annotation_events.label(event_idx) == 1)
                                % setting new position for label 'Start'
                                y_pos_of_text = 1;
                                y_pos_of_text_last = y_pos_of_text;
                            elseif(y_pos_of_text < 0.3) % if label is set too low (y-axis)
                                y_pos_of_text = 0.8;
                                y_pos_of_text_last = y_pos_of_text;
                            else
                                y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/6;
                                y_pos_of_text_last = y_pos_of_text;
                            end
                            
                            % setting text for label
                            text(annotation_events.datetime_relative(event_idx), y_pos_of_text, txt)
                        end
                        %set(gca,'xtick',[]);
                        hold off;
                    end
                    obj.Message("p" + num2str(i) + ": Raw vs Clean view of p" + i + " plotted.");
                else
                    obj.Message("p" + num2str(i) + ": No Clean data given. Cannot plot.");
                end
            end
        end
        
        function success = PlotXRay(obj)
            % This function should do:
            % 1) Plot x-ray images lateral vs ap (switches) with different
            % colors within _one_ plot (not only black bars!)
            % 2) Plot of G-arm movement relative to final position
            % 3) Plot (via flag) x-ray image frequency (-> use a private
            % function for frequency calculation)
            % 4) Plot of G-arm deviation from 'ideal' view
            %
            % TODO (MM): Use 'GArmEvaluator' or 'XRayEvalator' to filter the point of time 
            % when the g-arm is set and no more moved 
            % (and detect if that happpens even after skin incision is done!)
            % -> Include and mark that point of time within the plot!
            
            definitions;
            success = true;
            probands = obj.proband_ids_;
            
            obj.Message("> Plotting X-ray evaluation view of Clean Data for p" + probands(1) + " - p" + probands(end) + "...");
            
            % Using loaded clean_data_table_ from SyntrackIo class
            database = obj.SyntrackIo_ref.clean_data_table_;
            for i = probands
                
                % ****************************************************
                % PLOT results in fullscreen!
                figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
                %export_fig(['xray_p' num2str(i)]);
                
                % Plotting a 5 x 2 plot
                % 1st row: Event-plot
                % 2nd row: Ap/lat switches
                % 3rd-5th row: Axial / orbital / tilt angles
                for try_idx = 1:2
                    % Getting events and labels
                    [annotation_events,num_events,~] = obj.CollectEventsStartEndpoints(database{i,try_idx});
                    
                    % *************************
                    % Plotting X-ray events
                    plot_pos = 0 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.xray_event;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis,...
                        'color', '[0.75,0.75,0.75]', 'Linewidth', 4);
                    title(['P', num2str(i), ' X-ray shots - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([0.1,1.1]);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [0.1, 1.1],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Event text.
                        dataset_range_table = database{i,try_idx}(annotation_events.idx_of_label(1):annotation_events.idx_of_label(event_idx),:);
                        [cumulative_xrayshots,~,~] = obj.CountXrayShots(dataset_range_table);
                        
                        txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(cumulative_xrayshots) ')'],...
                            ['@ ' annotation_events.time_readable{event_idx}]});
                        
                        % Label position (always visible!)
                        if(annotation_events.label(event_idx) == 1)
                            % setting new position for label 'Start'
                            y_pos_of_text = 1;
                            y_pos_of_text_last = y_pos_of_text;
                        elseif(y_pos_of_text < 0.3) % if label is set too low (y-axis)
                            y_pos_of_text = 0.8;
                            y_pos_of_text_last = y_pos_of_text;
                        else
                            y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/6;
                            y_pos_of_text_last = y_pos_of_text;
                        end
                        
                        % setting text for label
                        text(annotation_events.datetime_relative(event_idx), y_pos_of_text, txt, 'FontSize', obj.kFontSizeEventAnnotation)
                    end
                    hold off;
                    
                    % *************************
                    % Plotting cpad switches
                    plot_pos = 2 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.cpad_switch_code;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis, 'color', '[0,0,0]', 'Linewidth', 1.1);
                    title(['P', num2str(i), ' C-Pad switches (ap/lat) - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([15;35]);
                    ylabel('AP <-> LAT')
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [15, 35],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Do not add any event text here
                    end
                    hold off;
                    
                    % *************************
                    % Plotting axial translation
                    plot_pos = 4 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.axial_translation;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis);
                    title(['P', num2str(i), ' C-arm Axial translation - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([-0.2,0.1]);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [-0.2, 0.1],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Do not add any event text here
                    end
                    hold off;
                    
                    % *************************
                    % Plotting orbital rotation
                    plot_pos = 6 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.orbital_angle;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis);
                    title(['P', num2str(i), ' C-arm Orbital rotation - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([-100,100]);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [-100, 100],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Do not add any event text here
                    end
                    hold off;
                    
                    % Plotting tilt
                    plot_pos = 8 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.tilt_angle;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis);
                    title(['P', num2str(i), ' C-arm tilting angle - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([-30,30]);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [-30, +30],...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Do not add any event text here
                    end
                    hold off;
                    
                end     % end of try-loop
                obj.Message("p" + num2str(i) + ": X-ray plot of p" + i + " plotted.");
            end     % end of proband loop
        end
        
        function success = PlotInstrumentMoves(obj)
            % Evaluation of Instrument movements including x-ray image frequency
            % 
            % CODE includes:
            % X-ray image frequency calculation (see old code) -> 
            % TODO(MM): Move frequency calculation to separate function!
            % TODO(MM): Use calculation of deviation of g-arm setup from
            % 'ideal' angles (virtual 'p23')?
            %
            % This function should do:
            % 1) Plot x-ray image frequency -> Correlation with movements?
            % 2) Plot the instrument position (in X/Y/Z) relative to the final
            % position
            % 3) Plot the angle deviations from the final angle (in 3
            % planes)
            % 4) Maybe also plot the amount of angulation per time
            % (floating average as with image frequency?)
            %

            definitions;
            success = true;
            probands = obj.proband_ids_;
            
            obj.Message("> Plotting Instrument evaluation view of Clean Data for p" + probands(1) + " - p" + probands(end) + "...");
            
            % Using loaded clean_data_table_ from SyntrackIo class
            database = obj.SyntrackIo_ref.clean_data_table_;
            for i = probands
                
                % ****************************************************
                % PLOT results in fullscreen!
                figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
                %export_fig(['xray_p' num2str(i)]);
                
                % Plotting a 3 x 2 plot
                % 1st row: Label events and X-ray plot (Ap/lat images in different colors)
                % 2nd row: Instrument movements in x/y/z-axis relative to end position
                % 3rd row: Instrument angulation in XZ and YZ plane
                for try_idx = 1:2
                    % Getting events and labels
                    clear annotation_events;
                    clear num_events;
                    clear experiment;
                    
                    [annotation_events,num_events,experiment] = obj.CollectEventsStartEndpoints(database{i,try_idx});
                    
                    % *************************
                    % Plotting X-ray events and moving average image
                    % frequency of x-ray shots
                    plot_pos = 0 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    y_axis = database{i, try_idx}.xray_event;
                    
                    % setting plot position
                    subplot(3,2,plot_pos);
                    
                    plot(x_axis, y_axis,...
                        'color', 'red', 'Linewidth', 0.5);
                    title(['P', num2str(i), ' X-ray shots and moving average - try no. ', num2str(try_idx)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    ylim([0.1,1.1]);
                    hold on;
                    
                    % Calculating mean for each attempt
                    % w/ moving average filter
                    k = 60; % seconds
                    f = 25; % SampleRate = 25 fps
                    c = k*f;
                    d = 10*f;   % every 10 seconds an average
                    mean1 = movmean(y_axis,c);
                    mean2 = movmean(mean1,d);
                    
                    % alternative filtering method
                    % b = ones(1,c)/c;
                    % a = 1;
                    % filter(b,a,y_axis)
                    
                    % Area plot of 1x smoothed moving mean
                    area(x_axis, mean1,...
                        'FaceColor', '[0.9,0.9,0.9]', 'Linewidth', 0.1);
                    title(['P', num2str(i), ' (V', num2str(try_idx), ' - L', num2str(lumbar_vert_lvl_of_proband(i)), ')\newlineX-ray image frequency [1/min]']);
                    ylim([0,1/k]);
                    %xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    %datetick('x','MM:ss');
                    % 2x smoothed moving mean
                    plot(x_axis, mean2, 'Color', 'blue','Linewidth',2);
                    %xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], ylim,...
                            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
                            'Linewidth', 1.25);
                        % Event text.
                        dataset_range_table = database{i,try_idx}(annotation_events.idx_of_label(1):annotation_events.idx_of_label(event_idx),:);
                        [cumulative_xrayshots,~,~] = obj.CountXrayShots(dataset_range_table);
                        
                        txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(cumulative_xrayshots) ')'],...
                            ['@ ' annotation_events.time_readable{event_idx}]});
                        
                        % Label position (always visible!)
                        if(annotation_events.label(event_idx) == 1)
                            % setting new position for label 'Start'
                            y_pos_of_text = 1/k;
                            y_pos_of_text_last = y_pos_of_text;
                        elseif(y_pos_of_text < 0.15/k) % if label is set too low (y-axis)
                            y_pos_of_text = 0.8/k;
                            y_pos_of_text_last = y_pos_of_text;
                        else
                            y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/6/k;
                            y_pos_of_text_last = y_pos_of_text;
                        end
                        
                        % setting text for label
                        text(annotation_events.datetime_relative(event_idx), y_pos_of_text, txt, 'FontSize', obj.kFontSizeEventAnnotation)
                    end
                    hold off;
                    
                    % *************************
                    % Plotting instrument movements in x/y/z-axis
                    
                    % Calculation of instrument positions (relative to final (end) position!)
                    poses = database{i, try_idx}.instrument_pose;
                    positions = [poses.pos];
                    positions = reshape(positions,3,[]);
                    x_pos_final = positions(1,end);
                    y_pos_final = positions(2,end);
                    z_pos_final = positions(3,end);
                    x_pos_relative = (positions(1,:) - x_pos_final)*100;
                    y_pos_relative = (positions(2,:) - y_pos_final)*100;
                    z_pos_relative = (positions(3,:) - z_pos_final)*100;
                    
                    % setting plot position
                    plot_pos = 2 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    subplot(3,2,plot_pos);
                    
                    plot(x_axis, x_pos_relative, 'b', x_axis, y_pos_relative, 'g', x_axis, z_pos_relative, 'r');
                    title(['Instrument: Tip deviation from final position']);
                    datetick('x','MM:SS', 'keepticks');
                    ylim([-10, 10]);
                    line(xlim, [0 0]);  %plot x-axis
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], ylim,...
                            'Color', [0.9 0.9 0.9],...
                            'Linewidth', 1.25);
                        % Do not add any event text here
                    end
                    hold off;
                    
                    legend('x','y','z');
                    ytickformat('%g cm');
                    % end of instrument movement plotting
                    
                    % *************************
                    % Plotting instrument angulation in XZ and YZ planes
                    
                    % --- WARNING TODO (MM): Implement!
                    
                    % Calculation of instrument positions (relative to final (end) position!)
                    poses = database{i, try_idx}.instrument_pose;
                    positions = [poses.pos];
                    positions = reshape(positions,3,[]);
                    rotations = {poses.rot};
                    %rot_matrix_final = rotations{end};
                    % makes sense?
                    %rot_matrix_relative = rotations - rot_matrix_final;

                    instr_angle_x = zeros(1,length(rotations));
                    instr_angle_y = zeros(1,length(rotations));
                    instr_angle_z = zeros(1,length(rotations));
                    
                    % Convert 3x3 rotation matrices into Euler rotations.
%                     a = 1;
%                     for R = rotations
%                         [instr_angle_x(a), instr_angle_y(a), instr_angle_z(a)] = dcm2angle(R{1}, 'XYZ');
%                         a = a + 1;
%                     end
                    
                    % Alternative representation using the central
                    % instrument axis ("up_vector"), pointing from the
                    % instrument tip towards the handle.
                    a = 1;
                    for R = rotations
                        up_vector = R{1}(3,:);
                        % Longitudinal axis pointing from inferior to
                        % superior (=-x in the chai3d frame).
                        rotation_around_longitudinal_patient_axis = ...
                            atan2(up_vector(3), -up_vector(2));
                        
                        % Sagittal axis pointing from right to left (=-y
                        % in the chai3d frame).
                        rotation_around_sagittal_patient_axis = ...
                            atan2(up_vector(3), up_vector(1));
                        
                        rot_mesh_to_chai3d = R{1};
                        [~, ~, rot_angle_around_z_mesh] = dcm2angle(rot_mesh_to_chai3d, 'XYZ');
                        
                        instr_angle_x(a) = rotation_around_longitudinal_patient_axis;
                        instr_angle_y(a) = rotation_around_sagittal_patient_axis;
                        instr_angle_z(a) = rot_angle_around_z_mesh;
                        a = a + 1;
                    end
                    
                    instr_angle_x_deg = instr_angle_x * (180.0/pi);
                    instr_angle_y_deg = instr_angle_y * (180.0/pi);
                    instr_angle_z_deg = instr_angle_z * (180.0/pi);
                    
                    % setting plot position
                    plot_pos = 4 + try_idx;
                    x_axis = database{i, try_idx}.datetime_relative;
                    subplot(3,2,plot_pos);
                    
                    plot(x_axis, instr_angle_x_deg, 'r', x_axis, instr_angle_y_deg, 'g', x_axis, instr_angle_z_deg, 'b');
                    title(['Instrument rotation around']);
                    datetick('x','MM:SS', 'keepticks');
                    ylim([-180, 180]);
                    line(xlim, [0 0]);  %plot x-axis
                    xlim([annotation_events.datetime_relative(1),annotation_events.datetime_relative(end)]);
                    
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], ylim,...
                            'Color', [0.9 0.9 0.9],...
                            'Linewidth', 1.25);
                        % Do not add any event text but a grey vertical bar here
                    end
                    hold off;
                    
                    legend('axial','sagittal','needle');
                    ytickformat('%g deg');
                    % --- End of angulation plotting ---
                    
                end     % End of try_idx loop
                obj.Message("p" + num2str(i) + ": Instrument evaluation view plotted.");
                % SAVING images in PNG with export_fig
                if(obj.kFlagWriteImagesToPNG == true)
                    export_fig(sprintf('instrument_eval_p%i.png', i));
                    obj.Message("Image of plot saved into 'instrument_eval_p" + i + ".png'.");
                end
                
            end     % End of proband loop
            
%             % ---- old code from 'xray_evaluation_view.m' ------
%             % RESULTS:
%             % save deviations into array (proband_id, try_idx)
%             % evaluated frame = "bone entered"
%             % y = Gertzbein, x = Dev(orbital/tilt/trans)
%             eventpos = find(annotation_events.label==label_numbers.enter_bone);
%             frmpos = annotation_events.frame_number(eventpos(end));
%             level = lumbar_vert_lvl_of_proband(proband_id);
%             dev_orbital(proband_id, try_idx) = attempt(try_idx).orbital_angle(frmpos);
%             dev_tilt(proband_id, try_idx) = attempt(try_idx).tilt_angle(frmpos);
%             % translation / not needed
%             %dev_trans(proband_id, try_idx) = attempt(try_idx).axial_translation(frmpos);
%             %detailed printout of numbers:
%             %fprintf('p%i, V%i: orbital %2.2f tilt %2.2f\n', proband_id, try_idx, dev_orbital(proband_id, try_idx), dev_tilt(proband_id,try_idx));
%             
%             % IMAGE FREQUENCY ANALYSIS
%             % Mean per attempt:
%             eventpos = find(annotation_events.label==label_numbers.enter_bone);
%             frmpos = annotation_events.frame_number(eventpos(1)); %1st bone entered event
%             images_avg_bone(proband_id, try_idx) = mean1(frmpos);
%             %fprintf('Mean images/min @ bone_entered p%i, V%i: %2.2f\n', proband_id, try_idx, mean1(frmpos));
%             % ---- end of old code ---- 

        end
        
        
        function success = PlotCharacteristics(obj, arg, val)
            % This function should give an short overview over each experiment (2 attempts)
            % 1) Plot phases
            % 2) Plot X-ray images lat and a.p. (colors or seperated plots)
            % 3) Plot Fluoro movement (all in one)
            % 4) Plot Instrument moves (velocity (3D) or detectet AoMs)
            % 5) Plot Instrument angulation
            
            definitions;
            success = true;
            probands = obj.proband_ids_;
            
            obj.Message("> Plotting Characteristics of Clean Data for p" + probands(1) + " - p" + probands(end) + "...");
            
            % = Debug options =
            debug_plot_aoms = false;
            debug_use_relative_positions = false;
            debug_plot_zoom_instrument_pos = false;
            obj.Message("Setting: debug_plot_aoms == " + string(debug_plot_aoms) + ".");
            obj.Message("Setting: debug_use_relative_positions == " + string(debug_use_relative_positions) + ".");
            obj.Message("Setting: debug_plot_zoom_instrument_pos == " + string(debug_plot_zoom_instrument_pos) + ".");
            
            if((arg == "saveimages") && (val == true))
                save_image = true;
            else
                save_image = false;
            end
            
            % Using loaded clean_data_table_ from SyntrackIo class
            database = obj.SyntrackIo_ref.clean_data_table_;
            
            % Check for clean data
            if(obj.SyntrackIo_ref.flag_xray_evaluation_done_ == false)
                error('No data of xray indices given. Cannot plot. Please execute GetXrayIndices(phases_evaluator) from class XRayEvaluator first.');
                success = 'error';
                return;
            end
            
            % Color definitions
            color_blue = '#0072BD';
            color_orange = '#EDB120';
            
            for p_id = probands
                
                % ****************************************************
                % PLOT results in fullscreen!
                figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
                %export_fig(['xray_p' num2str(i)]);
                
                % Plotting a 4 x 2 plot
                % 1st row: Events/phases-plot
                % 2nd row: Ap/lat images
                % 3rd row: G-arm position relative to final position
                % 4th row: Instrument movements relative to final position
                % (accelerations of path and angle)
                % 5th row: Instrument angulations relative to final
                % position
                for try_id = 1:2
                    % Getting events and labels
                    [annotation_events,num_events,~] = obj.CollectEventsStartEndpoints(database{p_id,try_id});
                    
                    % Some definitions
                    x_zero_datetime = database{p_id, try_id}.datetime_relative(1);
                    x_end_datetime = database{p_id, try_id}.datetime_relative(end);
                    
                    % *************************
                    % 1) Events/phases and labels
                    plot_pos = 0 + try_id;
                    x_axis = database{p_id, try_id}.datetime_relative;
                    y_axis = database{p_id, try_id}.label;
                    subplot(5,2,plot_pos);
                    
                    plot(x_axis, y_axis,'Linewidth', 0.5, 'Color', 'white');
                    title(['Timeline / Labels - try no. ', num2str(try_id)]);
                    datetick('x','MM:SS', 'keepticks');
                    
                    % Plotting defined phases with rectangles
                    % using results from PhasesEvaluator class (saved to SyntrackIo)
                    phases_evaluator_results = obj.SyntrackIo_ref.results_.PhasesEvaluator.phases_indices_;
                    x_pos_skin_cut = phases_evaluator_results{p_id, try_id}.skin_cut;
                    x_pos_bone_entered = phases_evaluator_results{p_id, try_id}.bone_entered_first;
                    
                    % Color definitions
                    color_lightblue = [176 200 221]./256; % '#0074dd'
                    color_midblue = [97 162 221]./256; % '#61a2dd'
                    color_blue = [0 116 221]./256; % '#0074dd'
                    
                    phase_end_idx(1) = 1;
                    phase_color_rgb(1) = {color_blue};
                    phase_end_idx(2) = x_pos_skin_cut;
                    phase_color_rgb(2) = {color_midblue};
                    phase_end_idx(3) = x_pos_bone_entered;
                    phase_color_rgb(3) = {color_lightblue};
                    phase_end_idx(4) = length(x_axis);
                    
                    hold on;
                    for phase_idx = 2:4
                        x_pos_start = x_axis(phase_end_idx(phase_idx-1));
                        x_pos_end = x_axis(phase_end_idx(phase_idx));
                        x = [x_pos_start x_pos_end x_pos_end x_pos_start];
                        y = [0 0 1 1];
                        % draw rectangle
                        fill(x,y, phase_color_rgb{phase_idx-1});
                    end
                    hold off;
                                                         
                    xlim([x_zero_datetime,x_end_datetime]);
                    ylim([0,8]);
                    ylabel('Events (mm:ss)');
                    set(gca,'YTick', []);
                    hold on;
                    % Adding labels to plot
                    for event_idx = 1:num_events
                                                
                        % Label/line position (always visible!)
                        if((annotation_events.label(event_idx) == 1) || (annotation_events.label(event_idx) == 6))
                            % setting new position for label 'Start' or 'End'
                            y_pos_of_text = 7.5;
                            font_weight = 'bold';
                            y_pos_of_text_last = y_pos_of_text;
                        elseif(y_pos_of_text < 3) % if label is set too low (y-axis)
                            y_pos_of_text = 6.5;
                            font_weight = 'normal';
                            y_pos_of_text_last = y_pos_of_text;
                        else
                            y_pos_of_text = y_pos_of_text_last - (8-0)/8;
                            font_weight = 'normal';
                            y_pos_of_text_last = y_pos_of_text;
                        end
                        
                        plot([annotation_events.datetime_relative(event_idx),annotation_events.datetime_relative(event_idx)], [1, y_pos_of_text],...
                            'Color', [0.75 0.75 0.75],...
                            'Linewidth', 1.25);
                        %legend({'Fluoro pos., EP targeting','EP search','Trocar placement'},'Location','north','NumColumns',3);
                        %legend('boxoff');
                        
                        % Event text
                        txt = ({[' ' annotation_name{annotation_events.label(event_idx)} ' (' annotation_events.time_readable{event_idx} ')']});
                        
                        text(annotation_events.datetime_relative(event_idx), y_pos_of_text, txt, 'FontSize', obj.kFontSizeEventAnnotation, 'FontWeight', font_weight)
                    end
                    
                    hold off;
                    
                    % *************************
                    % 2) Fluoroscopy usage
                    plot_pos = 2 + try_id;

                    x_axis = database{p_id, try_id}.datetime_relative;  
                    % using results from XRayEvaluator class
                    xray_evaluator_results = obj.SyntrackIo_ref.results_.XRayEvaluator.xray_indices_;
                    ap_shots = xray_evaluator_results{p_id, try_id}.shots_ap_indices;
                    lat_shots = xray_evaluator_results{p_id, try_id}.shots_lat_indices;
                    % use trick for better visual appearance
                    ap_shots(ap_shots == 0)= 2;
                    ap_shots(ap_shots == 1)= 0;
                    lat_shots(lat_shots == 0)= -2;
                    lat_shots(lat_shots == 1)= 0;
                    
                    y_axis1 = ap_shots;
                    y_axis2 = lat_shots;
                    
                    subplot(5,2,plot_pos);
                                       
                    % Show lat/ap in different colors
                    hold on;
                    plot(x_axis, y_axis1, 'Color', color_blue, 'Linewidth', 2.5);
                    hold off;
                    hold on;
                    plot(x_axis, y_axis2, 'Color', color_midblue, 'Linewidth', 2.5);
                    hold off;
                    
                    % Show lat/ap in same color
                    %plot(x_axis, [y_axis1, y_axis2], 'Color', color_blue, 'Linewidth', 2.5);
                    title(['Fluoroscopy usage (ap und lat) - try no. ', num2str(try_id)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    
                    xlim([x_zero_datetime,x_end_datetime]);
                    ylim([-1;1]);
                    ylabel('lateral < > a.p.');
                    set(gca,'YTick', []);
                    
                    % Adding gray vertical lines where phases end
                    x_values(1) = database{p_id, try_id}.datetime_relative(x_pos_skin_cut);
                    x_values(2) = database{p_id, try_id}.datetime_relative(x_pos_bone_entered);
                    hold on;
                    for x_val = x_values
                        plot([x_val, x_val], [-1, 1], 'Color', [0.75 0.75 0.75],'Linewidth', 1.25);
                    end
                    % plot line at y = 0
                    plot([x_zero_datetime, x_end_datetime], [0,0], 'black', 'LineWidth', 0.1);
                    % Do not add any event text here
                    box on;
                    hold off;
                    
                    % *************************
                    % 3) G-arm movements
                    plot_pos = 4 + try_id;
                    x_axis = database{p_id, try_id}.datetime_relative;
                    
%                   % Calculating velocity
%                     y_axis_garm{1} = [0; diff(database{p_id, try_id}.orbital_angle)];
%                     y_axis_garm{2} = [0; diff(database{p_id, try_id}.tilt_angle)];
%                     y_axis_garm{3} = [0; diff(database{p_id, try_id}.axial_translation)];
                    
                    % Position, not velocity..
                    pos_garm_absolute{1} = database{p_id, try_id}.orbital_angle;
                    pos_garm_absolute{2} = database{p_id, try_id}.tilt_angle;
                    pos_garm_absolute{3} = database{p_id, try_id}.axial_translation;
                    
                    % Calculating relative positions
                    for i = 1:3
                        pos_garm_final{i} = pos_garm_absolute{i}(end);
                        pos_garm_relative{i} = (pos_garm_absolute{i} - pos_garm_final{i});
                    end
                    
                    % Use settings: plot absolute or relative
                    for i = 1:3
                        if(debug_use_relative_positions == true)
                            pos_garm{i} = pos_garm_relative{i};
                        else
                            pos_garm{i} = pos_garm_absolute{i};
                        end
                    end
                    
                    % Retime and interpolate to fixed Sample Rate (24)
                    TT = timetable(x_axis, pos_garm{1}, pos_garm{2}, pos_garm{3});
                    TT = sortrows(TT);
                    TT_retimed = retime(TT, 'regular', 'pchip', 'SampleRate', 24);
                    x_axis = TT_retimed.x_axis;
                    pos_garm{1} = TT_retimed.Var1;
                    pos_garm{2} = TT_retimed.Var2;
                    pos_garm{3} = TT_retimed.Var3;
                    
                    % Normalize and filter data to avoid jittering
                    for i = 1:3
                        median_filter_grade = 25;
                        pos_garm{i} = medfilt1(pos_garm{i}, median_filter_grade);
                        pos_garm{i} = normalize(pos_garm{i}, 'norm', Inf);
                    end
                                        
                    subplot(5,2,plot_pos);
                    hold on;
                    for i = 1:3
                        % movement plots
                        plot(x_axis, pos_garm{i}, 'Color', phase_color_rgb{i}, 'LineWidth', 1.75);
                    end
                    % plot line at y = 0
                    %plot([x_zero_datetime, x_end_datetime], [0,0], 'black', 'LineWidth', 0.1);
                    hold off;
                    title(['G-arm movements (normalized, filtered) - try no. ', num2str(try_id)]);
                    datetick('x','MM:SS', 'keepticks');
                    xlim([x_zero_datetime,x_end_datetime]);
                    %ylim([0.000,1.2]);
                    ylim([-1.2,1.2]);
                    box on;

                    % Adding gray vertical lines where phases end
                    x_values(1) = database{p_id, try_id}.datetime_relative(x_pos_skin_cut);
                    x_values(2) = database{p_id, try_id}.datetime_relative(x_pos_bone_entered);
                    hold on;
                    for x_val = x_values
                        plot([x_val, x_val], [-1.2, 1.2], 'Color', [0.75 0.75 0.75],'Linewidth', 1.25);
                    end
                    hold off;
                    
                    % *************************
                    % 4) Instrument movements (velocity, x/y/z)
                    % ? Left scale = Movements (0 - max_amount)
                    % ? Right scale = Angulation (0 - 90 degree)
                    plot_pos = 6 + try_id;
                    x_axis = database{p_id, try_id}.datetime_relative;
                    
                    poses = database{p_id,try_id}.instrument_pose;
                    positions = [poses.pos];
                    positions = reshape(positions,3,[]);
                    
                    % Calculating relative positions
                    for i = 1:3
                        pos_instr_final{i} = positions(i,end);
                        pos_instr_relative{i} = (positions(i,:) - pos_instr_final{i})*100;
                    end
                    
                    % Use settings to plot absolute or relative
                    %pos_instr = zeros(size(pos_instr_relative));
                    for i = 1:3
                        if(debug_use_relative_positions == true)
                            pos_instr{i} = pos_instr_relative{i};
                        else
                            pos_instr{i} = positions(i,:);
                        end
                    end
                    
                    % Retime and interpolate to fixed Sample Rate
                    TT = timetable(x_axis, pos_instr{1}', pos_instr{2}', pos_instr{3}');
                    TT = sortrows(TT);
                    TT_retimed = retime(TT, 'regular', 'pchip', 'SampleRate', 24);
                    x_axis = TT_retimed.x_axis;
                    pos_instrument{1} = TT_retimed.Var1;
                    pos_instrument{2} = TT_retimed.Var2;
                    pos_instrument{3} = TT_retimed.Var3;
                    
                    % Normalizing
                    for i = 1:3
                        pos_instrument{i} = normalize(pos_instrument{i}, 'norm', Inf);
                    end
                    
                    subplot(5,2,plot_pos);
                    hold on;
                    
                    % TODO(MM): Move this code to function PlotInstrumentMoves
                    %
                    if(debug_plot_aoms)
                        % Getting AoM indices (small / large)
                        aoms_indices_large = obj.SyntrackIo_ref.results_.InstrumentEvaluator.aoms_indices_{p_id, try_id}.aoms_large_indices;
                        aoms_indices_small = obj.SyntrackIo_ref.results_.InstrumentEvaluator.aoms_indices_{p_id, try_id}.aoms_small_indices;
                        
                        % Plot large AoMs
                        for aom_idx = 1:length(aoms_indices_large)-1
                            if(mod(aom_idx,2) == 1)
                                % Mark areas between odd -> even indices = AoMs
                                x_pos_start = x_axis(aoms_indices_large(aom_idx));
                                x_pos_end = x_axis(aoms_indices_large(aom_idx+1));
                                x = [x_pos_start x_pos_end x_pos_end x_pos_start];
                                y = [0 0 1 1];
                                % draw rectangle
                                fill(x,y, phase_color_rgb{1}, 'EdgeColor', 'none');
                            end
                        end
                        hold off;
                        
                        hold on;
                        % Plot small AoMs
                        for aom_idx = 1:length(aoms_indices_small)-1
                            if(mod(aom_idx,2) == 1)
                                % Mark areas between odd -> even indices = AoMs
                                x_pos_start = x_axis(aoms_indices_small(aom_idx));
                                x_pos_end = x_axis(aoms_indices_small(aom_idx+1));
                                x = [x_pos_start x_pos_end x_pos_end x_pos_start];
                                y = [0 0 0.5 0.5];
                                % draw rectangle
                                fill(x,y, phase_color_rgb{2}, 'EdgeColor', 'none');
                            end
                        end
                    end

                    % Setting zoom_value if option is not false
                    if(debug_plot_zoom_instrument_pos)
                        zoom_value = debug_plot_zoom_instrument_pos;
                    else
                        zoom_value = 1;
                    end
                    for i = 1:3
                        % movement plots
                        plot(x_axis, pos_instrument{i}*zoom_value, 'Color', phase_color_rgb{i}, 'LineWidth', 1.75);
                    end

                    hold off;
                    
                    %title(['Instrument movements - AoMs analysis - try no. ', num2str(try_id)]);
                    title(['Instrument movements (normalized, filtered) - try no. ', num2str(try_id)]);
                    datetick('x','MM:SS', 'keepticks');
                    xlim([x_zero_datetime,x_end_datetime]);
                    %ylim([-0.2,0.2]);
                    ylim([-1.2,1.2]);
                    box on;

                    % Adding gray vertical lines where phases end
                    x_values(1) = database{p_id, try_id}.datetime_relative(x_pos_skin_cut);
                    x_values(2) = database{p_id, try_id}.datetime_relative(x_pos_bone_entered);
                    hold on;
                    for x_val = x_values
                        plot([x_val, x_val], [-1.2, 1.2], 'Color', [0.75 0.75 0.75],'Linewidth', 1.25);
                    end
                    hold off;
                    
                    % *************************
                    % 5) Instrument angulation (only acceleration?, phi)
                    plot_pos = 8 + try_id;
                    
                    x_axis = database{p_id, try_id}.datetime_relative;
                    % Compute angulation and return absolute and relative
                    % angles of instrument
                    [~, angle_absolute, angle_relative] = obj.ComputeInstrumentAngulation(p_id, try_id);
                    
                    % Use settings to plot absolute or relative
                    %angle_instr = zeros(size(angle_absolute));
                    for i = 1:3
                        if(debug_use_relative_positions == true)
                            angle_instr{i} = angle_relative{i};
                        else
                            angle_instr{i} = angle_absolute{i};
                        end
                    end
                    
                    % Retime and interpolate to fixed Sample Rate (24)
                    TT = timetable(x_axis, angle_instr{1}, angle_instr{2}, angle_instr{3});
                    TT = sortrows(TT);
                    TT_retimed = retime(TT, 'regular', 'pchip', 'SampleRate', 24);
                    x_axis = TT_retimed.x_axis;
                    angle_instr{1} = TT_retimed.Var1;
                    angle_instr{2} = TT_retimed.Var2;
                    angle_instr{3} = TT_retimed.Var3;
                    
                    % Normalize and filter data to avoid jittering
                    for i = 1:3
                        median_filter_grade = 25;
                        angle_instr{i} = medfilt1(angle_instr{i}, median_filter_grade);
                        angle_instr{i} = normalize(angle_instr{i}, 'norm', Inf);
                    end
                                        
                    subplot(5,2,plot_pos);
                    hold on;
                    for i = 1:3
                        % movement plots
                        plot(x_axis, angle_instr{i}, 'Color', phase_color_rgb{i}, 'LineWidth', 1.75);
                    end
                    title(['Instrument angulation (normalized, filtered) - try no. ', num2str(try_id)]);
                    
                    datetick('x','MM:SS', 'keepticks');
                    xlim([x_zero_datetime,x_end_datetime]);
                    %ylim([0.000,1.2]);
                    ylim([-1.2,1.2]);
                    box on;

                    % Adding gray vertical lines where phases end
                    x_values(1) = database{p_id, try_id}.datetime_relative(x_pos_skin_cut);
                    x_values(2) = database{p_id, try_id}.datetime_relative(x_pos_bone_entered);
                    hold on;
                    for x_val = x_values
                        plot([x_val, x_val], [-1.2, 1.2], 'Color', [0.75 0.75 0.75],'Linewidth', 1.25);
                    end
                    hold off;
                    
                    % Adding title at top of all subplots
                    sgtitle(['p', num2str(p_id), ': Pedicle Screw Placement - Try 1 and 2'], 'FontSize', 12, 'FontWeight', 'bold');
                    
                end     % end of try-loop
                obj.Message("p" + num2str(p_id) + ": Plot of p" + p_id + " plotted.");
                
                if(save_image == true)
                    % Save image
                    figure_handle = gcf;
                    obj.SaveResultsToGraphics(figure_handle, p_id, 'characteristics');
                end
            end     % end of proband loop
        end
        
        % ------------------------------------------
        % Helpers
        function [annotation_events,num_events,experiment] = CollectEventsStartEndpoints(obj, database)
            definitions;
            % Works for one proband_id only
            % Collect all annotation events.
            annotation_events = struct('idx_of_label',[],'frame_number',[],'label',[],'colorcode',[]);
            annotation_events.idx_of_label = find(database.label);
            annotation_events.frame_number = database.frame_nr(annotation_events.idx_of_label);
            annotation_events.label = database.label(annotation_events.idx_of_label);
            annotation_events.colorcode = {'green','blue','cyan','black','yellow','red','magenta','magenta','magenta'};
            num_events = numel(annotation_events.idx_of_label);

            experiment = struct('startpoints',[],'start',[],'endpoints',[],'end',[]);
            % Collect start- and endpoints of experiment.
            %experiment.startpoints = annotation_events.frame_number(annotation_events.label == annotation_label.start);
            experiment.startpoints = annotation_events.idx_of_label(annotation_events.label == annotation_label.start);
            experiment.start = experiment.startpoints(1);
            %experiment.endpoints = annotation_events.frame_number(annotation_events.label == annotation_label.end);
            experiment.endpoints = annotation_events.idx_of_label(annotation_events.label == annotation_label.end);
            experiment.end = experiment.endpoints(end);
            
            ts_start = database.ts(experiment.start);
            ts_end = database.ts(experiment.end);

            % relative vs absolut time: different time annotations
            if (obj.raw_vs_clean_absolute_time_ == true)
                % calculating relative time for events
                annotation_events.datetime_relative = datetime(database.ts(annotation_events.idx_of_label) - ts_start, 'ConvertFrom', 'datenum');
                annotation_events.datetime_relative.Format = 'mm:ss';
                
                for event_idx = 1:num_events
                annotation_events.time_readable{event_idx} =...
                        char(annotation_events.datetime_relative(event_idx));
                            %datestr(...
                        %database(i).ts(annotation_events.frame_number(event_idx)) - ts_start,...
                        %'MM:SS');
                end
            else
                % Converting timestamps @ annotation_events from datenum-format to
                % readable time (relative time counted from 'start' for each try)
                try_idx = 1;
                for event_idx = 1:num_events
                    if(annotation_events.label(event_idx) == annotation_label.start)
                        ts_start = database.ts(experiment.startpoints(try_idx));
                        try_idx = try_idx +1;
                        % re-calculate relative time for every try with new ts_start
                        annotation_events.datetime_relative = datetime(database.ts(annotation_events.idx_of_label) - ts_start, 'ConvertFrom', 'datenum');
                        annotation_events.datetime_relative.Format = 'mm:ss';
                        
                        annotation_events.time_readable{event_idx} =...
                            char(annotation_events.datetime_relative(event_idx));
                    else
                        annotation_events.time_readable{event_idx} =...
                            char(annotation_events.datetime_relative(event_idx));
                    end
                end
            end
        end
        
        % Compute x-ray shots within a given relative timespan from a
        % dataset table
        function [shots_total, shots_lat, shots_ap] = CountXrayShots(obj, dataset_table)
            shots_ap = length(find(dataset_table.cpad_switch_code == 17 & dataset_table.xray_event == 1));
            shots_lat = length(find(dataset_table.cpad_switch_code == 33 & dataset_table.xray_event == 1));
            shots_total = shots_lat + shots_ap;
        end
        
        function [angle_final, angle_absolute, angle_relative] = ComputeInstrumentAngulation(obj, p_id, try_id)
            % TODO(MM): Use InstrumentEvaluator data instead!
            % -->
            % obj.SyntrackIo_ref.results_.InstrumentEvaluator.angulation_absolute_/relative_
            database = obj.SyntrackIo_ref.clean_data_table_;
            poses = database{p_id, try_id}.instrument_pose;
                    rotations = {poses.rot};
                    
                    % pre-allocation
                    instr_angle_x = zeros(1,length(rotations));
                    instr_angle_y = zeros(1,length(rotations));
                    instr_angle_z = zeros(1,length(rotations));
                    
                    % Computing angulations:
                    % Alternative representation using the central
                    % instrument axis ("up_vector"), pointing from the
                    % instrument tip towards the handle.
                    a = 1;
                    for R = rotations
                        up_vector = R{1}(3,:);
                        % Longitudinal axis pointing from inferior to
                        % superior (=-x in the chai3d frame).
                        rotation_around_longitudinal_patient_axis = ...
                            atan2(up_vector(3), -up_vector(2));
                        
                        % Sagittal axis pointing from right to left (=-y
                        % in the chai3d frame).
                        rotation_around_sagittal_patient_axis = ...
                            atan2(up_vector(3), up_vector(1));
                        
                        rot_mesh_to_chai3d = R{1};
                        [~, ~, rot_angle_around_z_mesh] = dcm2angle(rot_mesh_to_chai3d, 'XYZ');
                        
                        instr_angle_x(a) = rotation_around_longitudinal_patient_axis;
                        instr_angle_y(a) = rotation_around_sagittal_patient_axis;
                        instr_angle_z(a) = rot_angle_around_z_mesh;
                        a = a + 1;
                    end
                    
                    % defining axes
                    y_axis_instr_angle{1} = instr_angle_x' * (180.0/pi); %x
                    y_axis_instr_angle{2} = instr_angle_y' * (180.0/pi); %y
                    y_axis_instr_angle{3} = instr_angle_z' * (180.0/pi); %z
                    
                    % Calculating relative angles
                    for i = 1:3
                        angle_final{i} = y_axis_instr_angle{i}(end);
                        angle_relative{i} = (y_axis_instr_angle{i} - angle_final{i});
                        angle_absolute{i} = y_axis_instr_angle{i};
                    end
        end
        
        function success = SaveResultsToGraphics(obj, figure_handles, p_id, title)
            % saving to graphics..
            % using gcf for figure export
            for fh = figure_handles
                obj.Message("Saving plot to 'Plotter_" + title + "_p" + num2str(p_id) + ".png'.");
                exportgraphics(fh,sprintf('Plotter_%s_p%i.png', title, p_id),'Resolution',300)
            end
            success = true;
        end
    end
end