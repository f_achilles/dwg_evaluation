%% X-ray evaluation

% using i because its better for reading..
i = proband_id; 

% Collect all annotation events.
annotation_events.frame_number = find(all_recordings(i).label);
annotation_events.label = all_recordings(i).label(annotation_events.frame_number);
annotation_events.colorcode = {'green','blue','cyan','black','yellow','red','magenta','magenta'};
num_events = numel(annotation_events.frame_number);

% Collect start- and endpoints of experiment.
experiment.startpoints = annotation_events.frame_number(annotation_events.label == annotation_label.start);
experiment.start = experiment.startpoints(1);
experiment.endpoints = annotation_events.frame_number(annotation_events.label == annotation_label.end);
experiment.end = experiment.endpoints(end);

ts_start = all_recordings(i).ts(experiment.start);
ts_end = all_recordings(i).ts(experiment.end);

% Converting timestamps @ annotation_events from datenum-format to
% readable time
try_idx = 1;
for event_idx = 1:num_events
    if(annotation_events.label(event_idx) == annotation_label.start)
        ts_start = all_recordings(i).ts(experiment.startpoints(try_idx));
        try_idx = try_idx +1;
        annotation_events.time_readable{event_idx} =...
        datestr(...
        all_recordings(i).ts(annotation_events.frame_number(event_idx)) - ts_start,...
        'MM:SS');
    else
        annotation_events.time_readable{event_idx} =...
        datestr(...
        all_recordings(i).ts(annotation_events.frame_number(event_idx)) - ts_start,...
        'MM:SS');
    end
end

% We count the number of pedicles by counting the number of endpoint events,
% as the start-label was used more than once, but the endpoint was always clear!
experiment.pedicles = length(experiment.endpoints);

% Count the number of XRay shots for each pedicle.
for try_idx = 1:experiment.pedicles
    % For case: Number of endpoints > Number of startpoints
    if(length(experiment.endpoints) > length(experiment.startpoints) && try_idx == length(experiment.endpoints))
        xray_shots.try(try_idx) =...
            (all_recordings(i).cumulative_xray_events(experiment.endpoints(end))...
            - all_recordings(i).cumulative_xray_events(experiment.startpoints(end)));
    else
        xray_shots.try(try_idx) =...
            (all_recordings(i).cumulative_xray_events(experiment.endpoints(try_idx))...
            - all_recordings(i).cumulative_xray_events(experiment.startpoints(try_idx)));
    end
end

% Count the total number of XRay shots during the experiment.
% => ! This is faulty! (shots in between start / end points)
% => SOLUTION: clean data (main.m) including all data-colums for tech-prob ranges
xray_shots.start = all_recordings(i).cumulative_xray_events(experiment.start);
xray_shots.total = ...
    all_recordings(i).cumulative_xray_events(experiment.end)...
    - xray_shots.start;

% Calculate total number of XRay shots
xray_shots.total = sum(xray_shots.try);

% Count the total number of C-Pad switches (without switches in CT view = code 34)
cpad_switches = 0;
for switches_idx = 2:length(all_recordings(i).cpad_switch_code)
    if ((abs(all_recordings(i).cpad_switch_code(switches_idx-1) - all_recordings(i).cpad_switch_code(switches_idx))) > 2)
        cpad_switches = cpad_switches + 1;
    end
end

% ****************************************************
% PLOT results in fullscreen!
figure('units','normalized','outerposition',[0 0 1 1],'Color','white');
%export_fig(['xray_p' num2str(i)]);

% plot: X-Ray shots
subplot(5,1,1);
    plot(all_recordings(i).frame_nr, all_recordings(i).xray_event,...
        'color', '[0.75,0.75,0.75]', 'Linewidth', 4);
    title(['P', num2str(i), ' X-Ray shots (Total: ', num2str(xray_shots.total),...
        ', 1st: ', num2str(xray_shots.try(1)),...
        ', Last: ' , num2str(xray_shots.try(end)) ,')']);
    xlim([experiment.start,experiment.end]);
    ylim([0.1,1.1]);
    hold on;
    % Adding labels to plot
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [0.1, 1.1],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Event text.
        num_xrayshots =...
            all_recordings(i).cumulative_xray_events(annotation_events.frame_number(event_idx))...
            - xray_shots.start;
        txt = ({[annotation_name{annotation_events.label(event_idx)} ' (' num2str(num_xrayshots) ')'],...
               ['@ ' annotation_events.time_readable{event_idx}]});
        % saving txt-like output for later file output
        out_string{event_idx} = [annotation_name{annotation_events.label(event_idx)} ';'...
            num2str(num_xrayshots) ';' annotation_events.time_readable{event_idx}];
           
        % Label position (always visible!)
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 1;
            y_pos_of_text_last = y_pos_of_text;
        elseif(y_pos_of_text < 0.15) % if label is set too low (y-axis)
            y_pos_of_text = 0.8;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - (1.1-0.1)/5;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;

% plot: C-Pad switches
subplot(5,1,2);
%     is_cpad_lateral_switch_active = all_recordings(i).cpad_switch_code >= 32;
    plot(all_recordings(i).frame_nr, all_recordings(i).cpad_switch_code, 'Linewidth', 1.1);
    title(['C-Pad switches [low = AP view] (Total: ', num2str(cpad_switches) , ')']);
    xlim([experiment.start,experiment.end]);
    ylim([15;35]);
    ylabel('AP <-> LAT')
    hold on; 
    % plotting horizontal lines with different color for every label-event and naming them
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [15, 35],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Label text
        txt = annotation_name{annotation_events.label(event_idx)};
        
        % Label position
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 35;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - (35-15)/5;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;

% plot: G-Arm Axial translation
subplot(5,1,3);
    plot(all_recordings(i).frame_nr, all_recordings(i).axial_translation);
    title('G-Arm Axial translation');
    xlim([experiment.start,experiment.end]);
    hold on; 
    % plotting horizontal lines with different color for every label-event and naming them
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [-0.1, 0.1],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Label text
        txt = annotation_name{annotation_events.label(event_idx)};
        
        % Label position
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 0.1;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - 0.1/5;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % setting text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    
% plot: G-Arm Orbital angle
subplot(5,1,4);
    plot(all_recordings(i).frame_nr, all_recordings(i).orbital_angle);
    title('G-Arm Orbital angle');
    xlim([experiment.start,experiment.end]);
    hold on; 
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [-100, 100],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Label text
        txt = annotation_name{annotation_events.label(event_idx)};
        
        % Label position
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 100;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - 100/5;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    
% plot: G-Arm Tilt
subplot(5,1,5);
    plot(all_recordings(i).frame_nr, all_recordings(i).tilt_angle);
    title('G-Arm Tilt angle');
    xlim([experiment.start,experiment.end]);
    ylim([-30,30]);
    hold on; 
    for event_idx = 1:num_events
        plot([annotation_events.frame_number(event_idx),annotation_events.frame_number(event_idx)], [-30, 30],...
            'Color', annotation_events.colorcode{annotation_events.label(event_idx)},...
            'Linewidth', 1.25);
        % Label text
        txt = annotation_name{annotation_events.label(event_idx)};

        % Label position
        if(annotation_events.label(event_idx) == 1)
            % setting new position for label 'Start'
            y_pos_of_text = 30;
            y_pos_of_text_last = y_pos_of_text;
        else
            y_pos_of_text = y_pos_of_text_last - 30/5;
            y_pos_of_text_last = y_pos_of_text;
        end
        
        % text for label
        text(annotation_events.frame_number(event_idx), y_pos_of_text, txt)
    end
    hold off;
    
% Saving images in PNG with export_fig
if(write_images_to_png == true) 
    export_fig(sprintf('xray_p%i.png', i));
end

% File output for label-values including time (i = proband_id)
if(write_data_to_file == true)
    filename = 'DATA_xray.txt';
    fileID = fopen(filename,'a+');
    
    fprintf(fileID, ['Proband' num2str(i) ';shots;time try\r\n']);
    for line_idx=1:length(out_string)
        fprintf(fileID, '%s\r\n', string(out_string(line_idx)));
    end
    clear out_string;
    
    % adding xray-shots info
    fprintf(fileID,'Total;%s\r\n',num2str(xray_shots.total));
    fprintf(fileID,'1st;%s\r\n',num2str(xray_shots.try(1)));
    fprintf(fileID,'Last;%s\r\n',num2str(xray_shots.try(end)));
    fprintf(fileID,'C-Pad switches;%s\r\n\r\n',num2str(cpad_switches));
    fclose(fileID);
end
