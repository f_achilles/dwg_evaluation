classdef Logger < handle
    
    properties (Abstract, Constant)
        kVerbose;
        kClassName;
    end
    
    properties (Constant)
        kProgrammVersion = '1.0';
        kDate = date();
        kInfo = [Logger.kDate, ' - V', Logger.kProgrammVersion];
    end
    
    methods (Access = protected)
        
        function Message(obj, message_string)
            % Display message sent from class 'kClassName'.
            if(obj.kVerbose), disp([obj.kClassName,': ', char(message_string)]); end
        end
    end
end