%% Adding group column to proband_table (see definitions)

% Using 'groups' array (from definitions) to define a column named 'group'
% in the table and set categorical status for this column

proband_table.group = groups(probands_id_start:probands_id_end)';
proband_table.group = categorical(proband_table.group); % set categorical

% get number of columns
cols = size(proband_table);
cols = cols(2);
proband_table = proband_table(:,[1,cols,2:cols-1]);    % re-arrange columns

clear cols;