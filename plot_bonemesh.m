%% Load and plot bone mesh of case 'mecanix'

bone_mesh = loadawobj('./meshes/mecanix-angio-mesh-lq.obj');
bead_offset_in_rai_coordinates = [0.144, 0.1277, 0.234];
% figure;
bonemesh_handle_patch = patch(...
    'vertices', bsxfun(@minus, bone_mesh.v', bead_offset_in_rai_coordinates),...
    'faces', bone_mesh.f3',...
    'edgecolor', 'none',...
    'facecolor', [0.8 0.8 0],...
    'facealpha', 0.7);
rotate(bonemesh_handle_patch,[1 0 0], 90, [0 0 0])
rotate(bonemesh_handle_patch,[0 0 1], -90, [0 0 0])
axis image vis3d
lighting gouraud
camlight
xlabel('X: sup->inf')
ylabel('Y: links->rechts')
zlabel('Z: ant->post')
switch lumbar_vert_lvl_of_proband(proband_id)
    case 2
        % knapp
%         axis([-0.05 -0.025 -0.04 0.04 0.02 0.12]);
        % mit discs
        axis([-0.05 -0.025 -0.04 0.04 0.02 0.12]);
    case 3
        % knapp
%         axis([-0.0135 0.015 -0.04 0.04 0.02 0.12]);
        % mit discs
        axis([-0.01 0.015 -0.04 0.04 0.02 0.12]);
    case 4
        % knapp:
%         axis([0.029 0.055 -0.04 0.04 0.01 0.11]);
        % mit discs:
        axis([0.029 0.050 -0.04 0.04 0.01 0.11]);
end
view(-90.4000, 5.6);
% view all 3 pedicles:
% axis([-0.07 0.06 -0.04 0.04 0.01 0.12])