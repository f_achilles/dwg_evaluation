classdef XRayEvaluator < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        results_ = table();
        results_xray_indices_ = cell(22,2);
        results_xray_indices_within_pedicle = cell(22,2);
    end
    
    properties (Constant)
        kClassName = 'XRayEvaluator';
        kClassVersion = 'V1.0';
        % Changelog XRayEvaluator.m
        % V1.0 (01-04-2020): Introduced class versioning.
        kFileNameResultsXls = [XRayEvaluator.kClassName,'_results.xlsx'];
        kFileNameResultsTxt = [XRayEvaluator.kClassName,'_results_',date(),' - ',XRayEvaluator.kClassVersion,'.txt'];
        kFileNameResultsSheetName = [date(), ' - ', XRayEvaluator.kClassVersion];
        kVerbose = true;
    end

    methods
        function obj = XRayEvaluator(io_ref, probands)
            obj.SyntrackIo_ref = io_ref;
            % check for modified set of probands ids to evaluate (args)
            if exist('probands','var') && ~isempty(probands)
                obj.proband_ids_ = probands;
                
                % Formatted message output for evaluated probands
                if nnz(diff(diff(probands))) == 0
                    p_id_range_txt = ['p', num2str(probands(1)), ' - p', num2str(probands(end))];
                else
                    p_id_range_txt = strrep(num2str(probands),'  ',', ');
                end
                obj.Message("Initialized to evaluate data for a modified range or probands (" + p_id_range_txt + ").");
            else
                % Standard case without args: Evaluate dataset that was loaded by SyntrackIo
                obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
                obj.Message("Initialized to evaluate data for p" + io_ref.proband_ids_(1) + " - p" + io_ref.proband_ids_(end) + ".");
            end
            
            if(~obj.SyntrackIo_ref.flag_clean_data_successfully_loaded_)
                obj.Message('Error! No clean data available. Please load or clean data first.');
                error('XRayEvaluator not functional. Aborting..');
            else
                obj.Message("> Evaluating X-ray and Fluoroscopy usage...");
            end
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for XRayEvaluator class are all working');
            else
                warning('Not all Tests for XRayEvaluator class are working. Please debug and fix.');
            end
        end
        
        function success = EvaluateProbands(obj, PhasesEvaluator_ref)
            % Evaluates xray shots per phases/tasks and return a table
            % Evaluates G-arm tilt and rotation of cortical perforation @ EP
            definitions;
            proband_ids = obj.proband_ids_;
            var_names = {...
                'p_id', 'try_id', 'shots_total', 'shots_ap', 'shots_lat',...
                'cpad_switches', 'deg90_switches',...
                'shots_start_skin_cut', 'shots_skin_cut_bone', 'shots_bone_end',...
                'shots_pedicle_ap', 'shots_pedicle_lat',...
                'garm_tilt_angle_diff_ideal', 'garm_rot_angle_diff_ideal',...
                };
            var_types(1:length(var_names)) = {'double'};
            
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);
            row_no = 1;
            
            % Calculating shots before/after 'skin_cut' event by using different datasets
            % Phase_indices{n,2} is a  cell of structs
            phase_indices = PhasesEvaluator_ref.phases_indices_;
            
            for p_id = proband_ids
                for try_id = 1:2
                    dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
                    [shots_total, shots_ap, shots_lat, cpad_switches, deg90_switches, ~] = obj.CountXrayShots(dataset_table);
                    
                    % Get indices from PhasesEval
                    idx_cut = phase_indices{p_id, try_id}.skin_cut;
                    idx_bone = phase_indices{p_id, try_id}.bone_entered_first;
                    
                    % Dataset till skin_cut
                    dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}(1:idx_cut,:);
                    [shots_start_skin_cut] = obj.CountXrayShots(dataset_table);
                                     
                    % Dataset from skin_cut to bone_entered
                    dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}(idx_cut:idx_bone,:);
                    [shots_skin_cut_bone] = obj.CountXrayShots(dataset_table);
                    
                    % Dataset from bone_entered to end
                    dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}(idx_bone:end,:);
                    [shots_bone_end] = obj.CountXrayShots(dataset_table);
                    
                    % Count images within pedicle / z-axis of spinal canal
                    [shots_pedicle_ap, shots_pedicle_lat, ~] = obj.CountXrayShotsWithinPedicle(p_id, try_id);
                    
                    % Compute G_arm tilt and rotation angle at time of perforation and
                    % calc difference to ideal tilt/rot angles 
                    % (definitions: garm_ideal_orbital(idx),
                    % garm_ideal_tilt(idx) where L1:idx = 1, L2: idx = 2, ..)
                    idx_ep_perf_pos = phase_indices{p_id, try_id}.bone_perf_ep;
                    vert_lvl = lumbar_true_vert_lvls_of_probands_sorted_by_try_id(p_id+(try_id-1)*22);
                    garm_proband_orbital = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}.orbital_angle(idx_ep_perf_pos);
                    garm_proband_tilt = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}.tilt_angle(idx_ep_perf_pos);
                    garm_tilt_angle_diff_ideal = round(garm_ideal_tilt(vert_lvl) - garm_proband_tilt,2);
                    garm_rot_angle_diff_ideal = round(garm_ideal_orbital(vert_lvl) - garm_proband_orbital,2);
                    
                    T_newrow = {p_id, try_id, shots_total, shots_ap, shots_lat, cpad_switches, deg90_switches, shots_start_skin_cut, shots_skin_cut_bone, shots_bone_end, shots_pedicle_ap, shots_pedicle_lat, garm_tilt_angle_diff_ideal, garm_rot_angle_diff_ideal};
                    T_results(row_no,:) = T_newrow;
                    row_no = row_no + 1;
                end
            end
            
            % Assign groups and saving results into a table
            T_results = obj.AssignGroups(T_results);
            obj.results_ = T_results;
            
            obj.Message("Displaying results of X-ray evaluation:");
            disp(obj.results_);
            
            % TODO(MM): Implement saving function (auto-save) to SyntrackIO
            success = true;
        end
        
        function success = GetXrayIndices(obj, PhasesEvaluator_ref)
            % Evaluate xray shots per try and return indices
            
            obj.Message("Computing indices of ap and lateral xray shots...");
            
            proband_ids = obj.proband_ids_;
            T_results = cell(length(proband_ids),2);    % pre-allocation
            
            for p_id = proband_ids
                for try_id = 1:2
                    dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
                    [~, ~, ~, ~, ~, shots_idxs] = obj.CountXrayShots(dataset_table);
                    T_results{p_id, try_id} = table(shots_idxs.ap, shots_idxs.lat, 'VariableNames', {'shots_ap_indices', 'shots_lat_indices'});                
                end
            end
            
            % Saving resulting indices for later usage
            obj.results_xray_indices_ = T_results;
            obj.Message("Resulting indices saved as 22x2 cell of tables into 'results_xray_indices_'.");
            obj.SyntrackIo_ref.flag_xray_evaluation_done_ = true;
        end
              
        function success = SaveResults2Tables(obj)
            T = obj.results_;
            try
                warning('off', 'MATLAB:xlswrite:AddSheet');
                writetable(T,obj.kFileNameResultsXls, 'sheet', obj.kFileNameResultsSheetName);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsXls + "' with worksheet named after actual date.");
                
                writetable(T,obj.kFileNameResultsTxt);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsTxt + "'.");
                success = true;
            catch
                success = false;
                obj.Message("Error saving results!");
            end
        end
                      
        function success = SaveResults2Io(obj)
            svd = obj.results_xray_indices_;
            results_struct = struct();
            results_struct.xray_indices_ = svd;
            results_struct.data_table_ = obj.results_;
            obj.SyntrackIo_ref.results_.XRayEvaluator = results_struct;
            obj.Message("Data results from '" + obj.kClassName + "' saved into Io.results_.");
        end
    end
    
    methods (Access = private)
        
        function [shots_total, shots_ap, shots_lat, cpad_switches, deg90_switches, shots_idxs] = CountXrayShots(obj, dataset_table)
            % Function is counting lat and ap xray shots (except code 34 => CT)
            % and counting indices for these shots ('_idxs')
            
            % 1) Shots caused by foot pedal
            % 1a) G-arm rotation < 90 (< 65) degree (same fluoro unit)
            % 1b) G-arm rotation ~ 90 (> 65) degree (other fluoro unit)
            % 2) Shots caused by using 90-degree switch
            % 2a) Switching from ap to lat -> lat image and vice versa (< 65 degree)
            % 2b) Switching from ap to lat (> 65 degree) -> ap (!) and vice versa
            
            % 1) Shots caused by pedal (xray_event == 1; code 17 = ap view; code 33 = lat view set)
            % 1a) with orbital rotation < 65 degree
            shots_ap_idxs = (dataset_table.cpad_switch_code == 17  & dataset_table.xray_event == 1 & abs(dataset_table.orbital_angle) < 65);
            shots_ap = length(find(shots_ap_idxs));
            shots_lat_idxs = (dataset_table.cpad_switch_code == 33 & dataset_table.xray_event == 1 & abs(dataset_table.orbital_angle) < 65);
            shots_lat = length(find(shots_lat_idxs));
            
            % 1b) with orbital rotation ~ 90 degree
            shots_ap_90degree_idxs = (dataset_table.cpad_switch_code == 33 & dataset_table.xray_event == 1 & abs(dataset_table.orbital_angle) > 65);
            shots_ap_90degree = length(find(shots_ap_90degree_idxs));
            shots_lat_90degree_idxs = (dataset_table.cpad_switch_code == 17 & dataset_table.xray_event == 1 & abs(dataset_table.orbital_angle) > 65);
            shots_lat_90degree = length(find(shots_lat_90degree_idxs));
                     
            % 2) Shots caused by using the switch
            cpad_switches_idxs = [0; diff(dataset_table.cpad_switch_code)];
            cpad_switches = length(find(cpad_switches_idxs));
                       
            % Shots caused by switch (33 = lat, 17 = ap; code == 33  & switch -> switch to 17 = ap image)
            % 2a) with rotation < 65 degree
            shots_ap_switch_idxs = (cpad_switches_idxs ~= 0 & dataset_table.cpad_switch_code == 17 & abs(dataset_table.orbital_angle) < 65);
            shots_lat_switch_idxs = (cpad_switches_idxs ~= 0 & dataset_table.cpad_switch_code == 33 & abs(dataset_table.orbital_angle) < 65);
            shots_ap_switch = length(find(shots_ap_switch_idxs));
            shots_lat_switch = length(find(shots_lat_switch_idxs));
            % 2b) with rotation ~ 90 degree
            shots_ap_90degree_switch_idxs = (cpad_switches_idxs ~= 0 & dataset_table.cpad_switch_code == 17 & abs(dataset_table.orbital_angle) > 65);
            shots_lat_90degree_switch_idxs = (cpad_switches_idxs ~= 0 & dataset_table.cpad_switch_code == 33 & abs(dataset_table.orbital_angle) > 65);
            shots_ap_90degree_switch = length(find(shots_ap_90degree_switch_idxs));
            shots_lat_90degree_switch = length(find(shots_lat_90degree_switch_idxs));
                      
            % Counting shots caused by rotation of G-arm ~ 90 degree
            % (differentiation and division by 2)
            deg90_switches = floor(length(find(diff(abs(dataset_table.orbital_angle) > 65)))/2);
            
            % Counting all shots with results from logic evaluation
            shots_ap = shots_ap + shots_ap_90degree + shots_ap_switch + shots_ap_90degree_switch;
            shots_lat = shots_lat + shots_lat_90degree + shots_lat_switch + shots_lat_90degree_switch;
            shots_total = shots_lat + shots_ap;
            
            % Save and return indices 'shots_idx' (struct) of identified lat and ap shots
            shots_ap_idxs = shots_ap_idxs + shots_ap_90degree_idxs + shots_ap_switch_idxs + shots_ap_90degree_switch_idxs;
            shots_lat_idxs = shots_lat_idxs + shots_lat_90degree_idxs + shots_lat_switch_idxs + shots_lat_90degree_switch_idxs;
            
            shots_idxs = struct('ap',shots_ap_idxs, 'lat', shots_lat_idxs);
        end
        
        function [shots_pedicle_ap, shots_pedicle_lat, shots_pedicle_idxs] = CountXrayShotsWithinPedicle(obj, p_id, try_id)
            % function counts x-ray shots within z-axis intercept of spinal
            % canal = part of pedicle at risk for medial perf.
            definitions;
            database = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
            
            % get indices of counted shots
            shots_ap_idxs = find(obj.results_xray_indices_{p_id, try_id}.shots_ap_indices);
            shots_lat_idxs = find(obj.results_xray_indices_{p_id, try_id}.shots_lat_indices);
            
            % get instrument (jamshidi) path indices after perforation event when z_axis interval of canal
            % is hit
            % getting idx of perforation from PhasesEvaluator
            idx_ep_perf_pos = obj.SyntrackIo_ref.results_.PhasesEvaluator.phases_indices_{p_id,try_id}.bone_perf_ep;

            % use only indices of instrument pos after final cortical
            % perforation (idx_ep_perf_pos)
            poses = database.instrument_pose;
            positions = [poses.pos];
            positions = reshape(positions,3,[]);
            instr_z_pos = positions(3,:)';
            instr_z_pos(1:idx_ep_perf_pos-1) = nan;
            
            z_axis_spinal_canal_dorsal_border = [spinal_canal_z_axis_top_l2;spinal_canal_z_axis_top_l3;spinal_canal_z_axis_top_l4];
            z_axis_spinal_canal_ventral_border = [spinal_canal_z_axis_bottom_l2;spinal_canal_z_axis_bottom_l3;spinal_canal_z_axis_bottom_l4];
            
            % getting vertebra level idx for each perforation from definitions for each proband and try (sorted by try_id)
            % -1 for usage w/ z_axis_spinal_canal_dorsal_border/ventral_border
            vert_lvl = lumbar_true_vert_lvls_of_probands_sorted_by_try_id(p_id+(try_id-1)*22)-1;
            
            idxs_within_pedicle = find(instr_z_pos < z_axis_spinal_canal_dorsal_border(vert_lvl) & instr_z_pos > z_axis_spinal_canal_ventral_border(vert_lvl));
            idx_within_pedicle_first = idxs_within_pedicle(1);
            idx_within_pedicle_last = idxs_within_pedicle(end);
            
            shots_pedicle_ap_idxs = find(shots_ap_idxs >= idx_within_pedicle_first & shots_ap_idxs <= idx_within_pedicle_last);
            shots_pedicle_lat_idxs = find(shots_lat_idxs >= idx_within_pedicle_first & shots_lat_idxs <= idx_within_pedicle_last);
            shots_pedicle_ap = length(shots_pedicle_ap_idxs);
            shots_pedicle_lat = length(shots_pedicle_lat_idxs);
            shots_pedicle_idxs = struct('ap',shots_pedicle_ap_idxs, 'lat', shots_pedicle_lat_idxs);
        end
        
        function [table_w_groups] = AssignGroups(obj, table)
            % Assigns a column 'groups' to a result table.
            % Cave: Only valid for a connected series of probands
            % e.g. 2:5; not: 2,5,7 !
            
            definitions;
            probands = obj.proband_ids_;
            T = sortrows(table,'try_id');
            % Assigning exp/nov from definitions
            T.group = [groups_sorted_by_try_id(probands(1):probands(end)); groups_sorted_by_try_id(probands(1):probands(end))];
            T.group = categorical(T.group);
            T = T(:,[1,2,end,3:end-1]);
            table_w_groups = T;
        end
    end
end