% Evaluation of SynTrack data recorded in study #1

%% Use Cases (integration tests)
%UseCase = UseCases;
%UseCase.Run(0);

%% Apps (unit tests)
%
% Important! Each App should only test one class.
%
%% App0 Loading
% Load cleaned data from 'clean_data_p1-p22.mat' if available
% e.g. [1,5:7]; using 1:22 (whole dataset) if no args set
p_ids_to_load = [1:22];
p_ids_to_evaluate = []; % Leave empty for eval of ALL loaded data.
data_io = SyntrackIo();
data_io.LoadData(p_ids_to_load);
data_io.GetEvaluationData('Source', 'xlsx');
data_io.SaveEvaluationData;

%% App1 Cleaning (needs pointer to SyntrackIo for data access)
% Todo (MM): Use this for a 'clean vs raw data' plotting
data_cleaner = DataCleaner(data_io);
data_cleaner.CleanData(); %autosaving cleaned data into io.clean_data_;

%% App2 Phases evaluation
% Todo (MM): Normalize phases (percentage of time needed) for each proband
phases_evaluator = PhasesEvaluator(data_io);
phases_evaluator.EvaluateProbands();
phases_evaluator.SaveResults2Io();
%phases_evaluator.SaveResults2Tables();

%% App3 Evaluating x-rays
x_ray_evaluator = XRayEvaluator(data_io);
x_ray_evaluator.GetXrayIndices(phases_evaluator);
x_ray_evaluator.EvaluateProbands(phases_evaluator);
x_ray_evaluator.SaveResults2Io();
x_ray_evaluator.SaveResults2Tables();

%% App4 G-Arm evaluation

%% App5 Instrument/Trocar evaluation
%data_io.GetEvaluationData("Source", "xlsx");
instrument_evaluator = InstrumentEvaluator(data_io,1:22);

% Calling options are
% PlotNeedleEntryPoints(['plot_single']),
% PlotNeedleEntryPoints('plot_all'),
% PlotNeedleEntryPoints(['plot_single'], [true]),
% PlotNeedleEntryPoints('plot_all', false).
%
% Default parameters in []. If called without parameters, there will be
% a new figure for each proband: [plot_single] and trajectories: [true].
instrument_evaluator.PlotNeedleEntryPoints('plot_all', false);
%instrument_evaluator.PlotNeedleEntryPoints('plot_single', true);
instrument_evaluator.PlotNeedleEntryPointsIdealViewAP();
instrument_evaluator.PlotNeedleEntryPointsIdealViewAxial();

instrument_evaluator.EvaluateNeedleAngulation();

% Areas of Movement (AoMs) evaluation
instrument_evaluator.GetAOMIndices('debug', false, 'savefigures', false);
instrument_evaluator.ComputePathLengthCountAOMs('start_end', 'UseAoms', 'true');
instrument_evaluator.ComputePathLengthCountAOMs('start_skin', 'UseAoms', 'true');
instrument_evaluator.ComputePathLengthCountAOMs('skin_bone_first', 'UseAoms', 'true');
instrument_evaluator.ComputePathLengthCountAOMs('bone_first_end', 'UseAoms', 'true');
instrument_evaluator.ComputePathLengthCountAOMs('bone_perf_end', 'UseAoms', 'true');
instrument_evaluator.SaveResults2Io();
instrument_evaluator.SaveResults2Tables();

%% App6 Screw evaluation ;-)
p_ids_to_evaluate = 1:22;
screw_evaluator = ScrewEvaluator(data_io, p_ids_to_evaluate);
screw_evaluator.EvaluateGertzbein(); % Uses PlotGertzbein() and ComputeGertzbein()
screw_evaluator.SaveResults2Io();
%screw_evaluator.SaveResults2Tables();

%% App7 Statistics
% Statistics executes SyntrackIo.GetEvaluationData('Source', 'xlsx') to get summarized data
stats = Statistics(data_io);

% time and shots eval
means_table = stats.CalcMeans({'time_total','shots_total'},{'group', 'try_id'});
stds_table = stats.CalcStds({'time_total','shots_total'},{'group', 'try_id'});
stats.results_ = stats.JoinCalcTables(means_table, stds_table); % shows results of statistical evaluation
stats.results_

% aoms eval
means_table = stats.CalcMeans({'aoms_large','aoms_small'},{'group', 'try_id'});
stds_table = stats.CalcStds({'aoms_large','aoms_small'},{'group', 'try_id'});
stats.results_ = stats.JoinCalcTables(means_table, stds_table); % shows results of statistical evaluation
stats.results_

stats.GroupedMatrixPlot({'time_total', 'aoms_large', 'aoms_small', 'shots_total', 'gertzbein_distance'}, {'group'});
stats.ManovaScatterPlot({'time_total', 'skin_perf', 'aoms_large_count_start_end', 'shots_total', 'gertzbein_distance'}, {'group'});

stats.CorrelationPlot();
stats.ANOVA();
%stats.SaveResults2Tables();

%% App8 Plotting
plotter = SyntrackPlotter(data_io, 1:22);  % call with optional args to evaluate a specific range of probands
%plotter.PlotRawVsCleanData(data_cleaner);
plotter.PlotCharacteristics('saveimages', false);  % needs results of XrayEvaluator
%plotter.PlotXRay();
%plotter.PlotInstrumentMoves();

% TODO(MM): Plot switches 0/1 under xray plot !

%% App9 Boxplotting
boxplotter = Boxplotter(data_io);
boxplotter.PlotGertzbeinStats();
boxplotter.PlotVertebraStats();
boxplotter.PlotAOMData();
boxplotter.PlotDurations();
boxplotter.PlotXrayData();
boxplotter.PlotPathLengths();
%boxplot.SaveResultsToGraphics();

%% Actual Evaluation (using the tested classes to compute new metrics or make new plots)
% TODO: Expertness.

% Visualization of score part curves:
% time
figure;
x = linspace(0.1, 15, 1000);
y = min(191./(x*60), 1);
plot(x, y, 'linewidth', 3);
set(gca, 'xlim', [0 15], 'ylim', [0 1.1])
xlabel('required time in [minutes]');
ylabel('r_{time}');
title('time-part of the score');
% gertzbein
figure;
x_gb = linspace(-2.0, 8, 1000);
y_gb = min(max((4-x_gb)/(4+1.23),0), 1);
plot(x_gb, y_gb, 'linewidth', 3);
set(gca, 'xlim', [-2.0 8], 'ylim', [0 1.1])
xlabel('distance to pedicle wall in [mm]');
ylabel('r_{Gertzbein}');
title('Gertzbein-part of the score');
