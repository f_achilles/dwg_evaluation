classdef ScrewEvaluator < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        io_evaluation_data_table_;      % reference to evaluation dataset to draw graphs
        results_ = table();             % result table including p_id, try_id, groups
        results_gertzbein_classes_mm_;	% GB classes and distances (in mm)
    end
    
    properties (Constant)
        kClassName = 'ScrewEvaluator';
        kClassVersion = 'V1.1';
        kClassVersionDate = '30-04-2020';
        % Changelog InstrumentEvaluator.m
        % 30-04-2020, V1.1: Updated for Outpunt Handling. 
        % Replaced SaveTables() with SaveResults2Io() and SaveResults2Tables()
        kFileNameResultsXls = [ScrewEvaluator.kClassName,'_results.xlsx'];
        kFileNameResultsTxt = [ScrewEvaluator.kClassName,'_results_',date(),' - ',InstrumentEvaluator.kClassVersion,'.txt'];
        kFileNameResultsSheetName = [date(), ' - ', ScrewEvaluator.kClassVersion];
        kVerbose = true;
    end
    
    methods
        function obj = ScrewEvaluator(io_ref, probands)
            obj.SyntrackIo_ref = io_ref;
            
            % check for modified set of probands ids to evaluate (args)
            if exist('probands','var') && ~isempty(probands)
                obj.proband_ids_ = probands;
                
                % Formatted message output for evaluated probands
                if nnz(diff(diff(probands))) == 0
                    p_id_range_txt = ['p', num2str(probands(1)), ' - p', num2str(probands(end))];
                else
                    p_id_range_txt = strrep(num2str(probands),'  ',', ');
                end
                obj.Message("Initialized to evaluate data for a modified range or probands (" + p_id_range_txt + ").");
            else
                % Standard case without args: Plot all available data that
                % was loaded by SyntrackIo
                obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
                obj.Message("Initialized to evaluate data for p" + io_ref.proband_ids_(1) + " - p" + io_ref.proband_ids_(end) + ".");
            end
            
            if(isempty(obj.SyntrackIo_ref.clean_data_table_))
                success = 'error. no clean data available. please execute DataCleaner first';
            end
            obj.Message("Clean data loaded. Ready to evaluate screw positions.");
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;
            
            if success
                disp('Tests for ScrewEvaluator class are all working');
            else
                warning('Not all Tests for ScrewEvaluator class are working. Please debug and fix.');
            end
        end
        
        function success = EvaluateGertzbein(obj)
            proband_ids = obj.proband_ids_;
            obj.Message("> Evaluating Screw positions for p" + proband_ids(1) + " - p" + proband_ids(end) + "...");
                      
            % pre allocating result table
            var_names = {'p_id', 'try_id', 'gertzbein_distance', 'gertzbein_class'};
            var_types(1:length(var_names)-1) = {'double'};
            var_types = [var_types {'string'}];
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);

            % Plot screws
            for p_id = proband_ids
                % Setting dataset for each proband (contains try 1 and 2)
                dataset_table = obj.SyntrackIo_ref.clean_data_table_(p_id,:);
                
                [gertzbein_figure_handle{p_id}, pm_handle_patch{p_id}, screw_meshes{p_id}, screwmesh_handle_patch{p_id}, pediculation_positions{p_id}] = obj.PlotGertzbein(dataset_table, p_id);
            end
            obj.Message("Screw plotting done.");
            
            % Compute Gertzbein distances and class from plots
            obj.Message("Computing Screw Distances to pedicle walls and evaluating Gertzbein Classes...");
            
            row_no = 1;
            for p_id = proband_ids              
                % Compute screw distance and GB class for both attempts
                [gertzbein_distance_mm, gertzbein_class] = obj.ComputeGertzbein(p_id, gertzbein_figure_handle{p_id}, pm_handle_patch{p_id}, screw_meshes{p_id}, screwmesh_handle_patch{p_id}, pediculation_positions{p_id});
                if(exist('gertzbein_distance_mm'))
                    % Creating evaluation result table..
                        for try_id = 1:2                            
                            T_newrow = {p_id, try_id, gertzbein_distance_mm(try_id), gertzbein_class(try_id)};
                            T_results(row_no,:) = T_newrow;
                            row_no = row_no + 1;
                        end
                else
                    error('Gertzbein computation failed!');
                end
            end
            obj.Message("Gertzbein computation done.");
                                  
            % Saving result table plus columns classes and distances for later direct usage
            obj.results_ = AssignGroups(obj, T_results);
            disp(T_results);
            obj.results_gertzbein_classes_mm_ = T_results(:,3:4);
            obj.Message("Result table saved (results_). Computed classes and distances saved into 'results_gertzbein_classes_mm_'.");
            obj.SyntrackIo_ref.flag_gertzbein_evaluation_done_ = true;
        end
                
        function success = SaveResults2Tables(obj)
            T = obj.results_;
            try
                warning('off', 'MATLAB:xlswrite:AddSheet');
                writetable(T,obj.kFileNameResultsXls, 'sheet', obj.kFileNameResultsSheetName);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsXls + "' with worksheet named after actual date.");
                
                writetable(T,obj.kFileNameResultsTxt);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsTxt + "'.");
                success = true;
            catch
                success = false;
                obj.Message("Error saving results!");
            end
        end
        
        function success = SaveResults2Io(obj)
            svd = obj.results_gertzbein_classes_mm_; % GB classes and distances
            T = obj.results_;
            results_struct = struct();
            results_struct.gb_ = svd;
            results_struct.data_table_ = T;
            obj.SyntrackIo_ref.results_.ScrewEvaluator = results_struct;
            obj.Message("Data results from '" + obj.kClassName + "' saved into Io.results_.");
        end
    end
    
    methods (Access = private)
       
        function [gertzbein_figure_handle, pm_handle_patch, screw_meshes, screwmesh_handle_patch, pediculation_positions] = PlotGertzbein(obj, dataset_table, proband_id)
            % Graphical plotting of the placed screw within the
            % corresponding pedicle mesh of the lumbar vertebra level
            % 
            % Variables used: proband_id, try_idx
            % Plot creates the following handles for later computing:
            %
            % gertzbein_figure_handle
            % pm_handle_patch(pedicle_mesh_no)
            % screwmesh_handle_patch(try_idx)
            % pediculation_positions(1:3,try_idx)
                       
            %% Load and plot bone mesh of case 'mecanix'
            
            % 1. Alle objekte laden (mit richtiger skalierung und orientierung s. MeshLab)
            % 2. Position durch Lage des Beads korrigieren
            % 3. Rotation der Meshes
            % 4. Endpunkte ermitteln
            % 5. Schraubenmodell am Endpunkt plotten + rotieren
            
            obj.Message("> Plotting screws of p" + proband_id + "..");
            
            % Defining meshes & Bead offset
            % Loading definition includes pedicle_meshes for each pedicle
            definitions;
            lvl = lumbar_vert_lvl_of_proband(proband_id);
            screw_mesh_path = './meshes/screw.obj';
            bead_offset_in_rai_coordinates = [0.144, 0.1277, 0.234];
            
            % Load bonemesh into figure.
            gertzbein_figure_handle = figure('Color','black');
            axiscolor = [0.75 0.75 0.75];
            set(gca,'Color','k','XColor',axiscolor,'YColor',axiscolor,'ZColor',axiscolor);
            
            % Load pedicle meshes.
            for p = 1:numel(pedicle_meshes)
                pedicle_mesh = loadawobj(pedicle_meshes{p});
                pm_handle_patch(p) = patch(...
                    'vertices', bsxfun(@minus,...
                    pedicle_mesh.v', bead_offset_in_rai_coordinates),...
                    'faces', pedicle_mesh.f3',...
                    'edgecolor', 'none',...
                    'facecolor', [1 0 0],...
                    'facealpha', 1);
                
                rotate(pm_handle_patch(p),[1 0 0], 90, [0 0 0])
                rotate(pm_handle_patch(p),[0 0 1], -90, [0 0 0])
            end
            
            % figure config
            camlight
            xlabel('X: sup->inf')
            ylabel('Y: links->rechts')
            zlabel('Z: ant->post')
            axial_slice_width = 0.06;
            axis equal;
            switch lvl
                case 2
                    % knapp
                    %         axis([-0.05 -0.025 -0.04 0.04 0.02 0.12]);
                    % mit discs
                    %         axis([-0.065 -0.025 -0.04 0.04 0.02 0.12]);
                    % full
                             axis([-0.075+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 3
                    % knapp
                    %         axis([-0.0135 0.015 -0.04 0.04 0.02 0.12]);
                    % mit discs
                    %         axis([-0.0165 0.015 -0.04 0.04 0.02 0.12]);
                    % full
                             axis([-0.0315+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
                case 4
                    % knapp:
                    %         axis([0.029 0.055 -0.04 0.04 0.01 0.11]);
                    % mit discs:
                    %         axis([0.015 0.060 -0.04 0.04 0.01 0.11]);
                    % full
                             axis([0.005+[0 axial_slice_width] -0.06 0.06 0.01 0.11]);
            end
            bone_mesh_path = sprintf('./meshes/mecanix_nice_L%i.obj', lvl);
            bone_mesh = loadawobj(bone_mesh_path);
            bonemesh_handle_patch = patch(...
                'vertices', bsxfun(@minus, bone_mesh.v', bead_offset_in_rai_coordinates),...
                'faces', bone_mesh.f3',...
                'edgecolor', 'none',...
                'facecolor', [1 1 1],...
                'facealpha', 0.5);
            rotate(bonemesh_handle_patch,[1 0 0], 90, [0 0 0])
            rotate(bonemesh_handle_patch,[0 0 1], -90, [0 0 0])
            %axis image vis3d    % cropping view to visible meshes

            view(-90.4000, 5.6);
            % view all 3 pedicles:
            % axis([-0.07 0.06 -0.04 0.04 -0.01 0.12])
            
            % Plotting screw
            hold on
            
            % Creating temporary concat table (like a raw dataset..)
            dataset_table_concat = table();
            dataset_table_concat = [dataset_table{1};dataset_table{2}];
            
            poses = dataset_table_concat.instrument_pose;
            % getting positions and rotations
            positions = [poses.pos];
            positions = reshape(positions,3,[]);
            rotations = {poses.rot};
            % getting end indices
            end_indices = find(dataset_table_concat.label == label_numbers.end);
            % getting position and rotation of try 1 & 2
            pediculation_positions = positions(:,end_indices);
            pediculation_rotations = rotations(end_indices);
            % plot two markers for end positions
            plot3(pediculation_positions(1,:),...
                pediculation_positions(2,:),...
                pediculation_positions(3,:),...
                '.','MarkerIndices',[1 2],'MarkerSize',15,'Color','yellow');
            % plotting of screws at end positions
            screw_mesh_orig = loadawobj(screw_mesh_path);
            color_codes = {[0.1 0.4 1],[0 1 0]}; % 1st try = blue, 2nd = green
            screw_meshes = cell(1,2);
            % screwmesh_handle_patch = zeros(1,2);
            for try_idx = 1:2
                screw_meshes{try_idx} = screw_mesh_orig;
                % rotation of screw
                screw_meshes{try_idx}.v = pediculation_rotations{try_idx}' * screw_meshes{try_idx}.v;
                % positioning of screw
                screw_meshes{try_idx}.v = bsxfun(@plus,...
                    screw_meshes{try_idx}.v, pediculation_positions(:,try_idx));
                
                screwmesh_handle_patch(try_idx) = patch(...
                    'vertices', screw_meshes{try_idx}.v',...
                    'faces', screw_meshes{try_idx}.f3',...
                    'edgecolor', 'none',...
                    'facecolor', color_codes{try_idx},...
                    'facealpha', 0.5);
            end
            
            % Switch on the nicely interpolated light at the end in order to affect all
            % meshes in the figure.
            lighting gouraud
            
            title({sprintf('Proband ID: %i, vertebra: L%i',...
                proband_id, lumbar_vert_lvl_of_proband(proband_id)),...
                '(1st try = blue, 2nd try = green)'}, 'color', 'white');
            
            hold off
        end
                
        function [gertzbein_distance_mm, gertzbein_class] = ComputeGertzbein(obj, proband_id, gertzbein_figure_handle, pm_handle_patch, screw_meshes, screwmesh_handle_patch, pediculation_positions)
            % Computes the Gertzbein distance (in mm) and the corresponding Class
            % Returns gertzbein_distance_mm(2) and gertzbein_class(2) for
            % each try
            obj.Message("> Computing Gertzbein classifications..");
            
            % Definitions.
            definitions;
            gerztbein_vector_maximum_angle = 10;
            
            % drawnow seems to be required, because we need access to the patch object
            % of the pedicle mesh, in order to get the normals. (for now)
            figure(gertzbein_figure_handle);
            drawnow;
            
            % Compute bounding boxes.
            bbox = zeros(6,6);
            % bbox : [min_x max_x min_y max_y min_z max_z];
            for p = 1:6
                bbox(p,1) = min(pm_handle_patch(p).Vertices(:,1));
                bbox(p,2) = max(pm_handle_patch(p).Vertices(:,1));
                bbox(p,3) = min(pm_handle_patch(p).Vertices(:,2));
                bbox(p,4) = max(pm_handle_patch(p).Vertices(:,2));
                bbox(p,5) = min(pm_handle_patch(p).Vertices(:,3));
                bbox(p,6) = max(pm_handle_patch(p).Vertices(:,3));
            end
                
            % Compute bbox centers.
            bbox_centers = zeros(6,3);
            for p = 1:6
                bbox_centers(p,1) = mean(bbox(p,[1 2]));
                bbox_centers(p,2) = mean(bbox(p,[3 4]));
                bbox_centers(p,3) = mean(bbox(p,[5 6]));
                %% DEBUG plot
                %     hold on;
                %     plot3(bbox_centers(p,1), bbox_centers(p,2), bbox_centers(p,3),...
                %         '.','MarkerSize',30,'Color','red');
                %     hold off
                %%
            end
            
            % Assign screws of try 1 and 2 to their matching pedicle.
            % Compute every possible combination: 2 attempts + each 6 possible pedicles
            min_distances = pdist2(pediculation_positions(:, [1 2])', bbox_centers);
            [~, pedicle_indices_for_try] = min(min_distances, [], 2);
            
            for try_idx = 1:2
                c_pedicle_idx = pedicle_indices_for_try(try_idx);
                c_bbox_1x6 = bbox(c_pedicle_idx, :);
                c_screw_vertices_Nx3 = screw_meshes{try_idx}.v';
                indices_of_screw_vertices_inside_bbox_binary =...
                    c_screw_vertices_Nx3(:,3) > c_bbox_1x6(5) &...
                    c_screw_vertices_Nx3(:,3) < c_bbox_1x6(6);
                indices_of_screw_vertices_inside_bbox =...
                    find(indices_of_screw_vertices_inside_bbox_binary);
                %% DEBUG plot
                %     hold on;
                %     for screw_vertex_idx = indices_of_screw_vertices_inside_bbox'
                %         plot3(c_vertices_Nx3(screw_vertex_idx,1),...
                %             c_vertices_Nx3(screw_vertex_idx,2),...
                %             c_vertices_Nx3(screw_vertex_idx,3),...
                %             '.','MarkerSize',30,'Color','blue');
                %     end
                %     hold off
                %%
                filtered_screw_vertices_nx3 =...
                    c_screw_vertices_Nx3(indices_of_screw_vertices_inside_bbox,:);
                
                c_pedicle_vertices_mx3 = pm_handle_patch(c_pedicle_idx).Vertices;
                
                % Get the numbers of vertices for pedicle mesh (m) and screw (n).
                m_pedicle_vertices = size(c_pedicle_vertices_mx3, 1);
                n_screw_vertices = numel(indices_of_screw_vertices_inside_bbox);
                
                % Compute the difference vectors that go from pedicle to screw.
                difference_vectors_nmx3 = bsxfun(@minus,...
                    repmat(filtered_screw_vertices_nx3, m_pedicle_vertices, 1),...
                    reshape(repmat(c_pedicle_vertices_mx3',n_screw_vertices,1), 3, [])');
                
                % Get vertex normals and normalize them to length 1.
                c_pedicle_vertex_normals_mx3 = double(pm_handle_patch(c_pedicle_idx).VertexNormals);
                c_pedicle_vertex_normals_mx3 = bsxfun(@rdivide, c_pedicle_vertex_normals_mx3,...
                    sqrt(sum(c_pedicle_vertex_normals_mx3.*c_pedicle_vertex_normals_mx3, 2)));
                %% DEBUG plot (Shows that the original patch Vertex Normals all point inside!)
                %     xyz = c_pedicle_vertices_mx3;
                %     uvw = c_pedicle_vertex_normals_mx3 * 0.005;
                %     hold on
                %     quiver3(...
                %         xyz(:,1),xyz(:,2),xyz(:,3),...
                %         uvw(:,1),uvw(:,2),uvw(:,3),'b');
                %     hold off
                %%
                c_pedicle_vertex_normals_mx3 = -c_pedicle_vertex_normals_mx3;
                
                % Use 3rd PCA-Component of the pedicle mesh instead of the normals, to
                % define the pedicle-plane. The normal's task is now taken over by the
                % normalized projection of the difference vector onto the pedicle
                % plane.
                principal_component_vectors = pca(c_pedicle_vertices_mx3)';
                pca_vector_long = principal_component_vectors(1,:);
                pca_vector_long = pca_vector_long./norm(pca_vector_long);
                pca_vector_short = principal_component_vectors(2,:);
                pca_vector_short = pca_vector_short./norm(pca_vector_short);
                pedicle_center = mean(c_pedicle_vertices_mx3,1);
                % project the normals onto the pedicle plane
                for i_p_v = 1:m_pedicle_vertices
                    c_normal_vec = c_pedicle_vertex_normals_mx3(i_p_v,:);
                    projection_length_on_short_pedicle_axis = dot(pca_vector_short,c_normal_vec);
                    projection_length_on_long_pedicle_axis = dot(pca_vector_long,c_normal_vec);
                    c_normal_vec_in_pedicle_plane =...
                        projection_length_on_long_pedicle_axis * pca_vector_long +...
                        projection_length_on_short_pedicle_axis * pca_vector_short;
                    c_normal_vec_in_pedicle_plane =...
                        c_normal_vec_in_pedicle_plane ./ norm(c_normal_vec_in_pedicle_plane);
                    c_pedicle_vertex_normals_mx3(i_p_v,:) = c_normal_vec_in_pedicle_plane;
                end
                %% DEBUG plot for PCA
                %     xyz = repmat(pedicle_center,3,1);
                %     uvw = principal_component_vectors;
                %     hold on
                %     quiver3(...
                %         xyz(:,1),xyz(:,2),xyz(:,3),...
                %         uvw(:,1),uvw(:,2),uvw(:,3),'r');
                %     hold off
                %%
                % Normalize the difference vectors in order to compute their angle with
                % the pedicle vertex normals.
                difference_vector_lenghts_nxmpedicle = pdist2(...
                    filtered_screw_vertices_nx3, c_pedicle_vertices_mx3);
                difference_vectors_nmx3_normalized =...
                    difference_vectors_nmx3 ./...
                    repmat(difference_vector_lenghts_nxmpedicle(:),1,3);
                
                % Compute the length of the projection of the difference vectors onto
                % the pedicle vertex normals.
                projected_lengths_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
                normal_and_diffvec_angles_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
                negative_normal_and_diffvec_angles_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
                for i_p_v = 1:m_pedicle_vertices
                    projected_lengths_nxm(:, i_p_v) =...
                        difference_vectors_nmx3((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
                        * c_pedicle_vertex_normals_mx3(i_p_v, :)';
                    
                    normal_and_diffvec_angles_nxm(:, i_p_v) = (180/pi)*acos(...
                        min(ones(n_screw_vertices,1,'double'),...
                        difference_vectors_nmx3_normalized((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
                        * c_pedicle_vertex_normals_mx3(i_p_v, :)')...
                        );
                    
                    negative_normal_and_diffvec_angles_nxm(:, i_p_v) = (180/pi)*acos(...
                        min(ones(n_screw_vertices,1,'double'),...
                        difference_vectors_nmx3_normalized((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
                        * -c_pedicle_vertex_normals_mx3(i_p_v, :)')...
                        );
                end
                
                % Filter lengths that arise because the angle between normal and
                % difference vector is too high.
                projected_lengths_nxm(...
                    ((projected_lengths_nxm>0) & (normal_and_diffvec_angles_nxm > gerztbein_vector_maximum_angle)) |...
                    ((projected_lengths_nxm<0) &(negative_normal_and_diffvec_angles_nxm > gerztbein_vector_maximum_angle)))...
                    = -1000;
                [values, pedicle_mesh_indices] = max(projected_lengths_nxm, [], 2);
                [gertzbein_distance, screw_mesh_index] = max(values);
                pedicle_mesh_index = pedicle_mesh_indices(screw_mesh_index);
                % DEBUG plot
                if gertzbein_distance > 0
                    uvw = difference_vectors_nmx3((pedicle_mesh_index-1) * n_screw_vertices + screw_mesh_index,:);
                    xyz = c_pedicle_vertices_mx3(pedicle_mesh_index,:);
                    uvw_normal = c_pedicle_vertex_normals_mx3(pedicle_mesh_index,:);
                    uvw_normal = uvw_normal * 0.01;
                    hold on
                    quiver3(xyz(1),xyz(2),xyz(3),uvw(1),uvw(2),uvw(3),'g');
                    quiver3(xyz(1),xyz(2),xyz(3),uvw_normal(1),uvw_normal(2),uvw_normal(3),'w');
                    hold off
                    fprintf('Proband %i, Try %i: Angle: %2.2f\n',proband_id, try_idx, ...
                        normal_and_diffvec_angles_nxm(screw_mesh_index, pedicle_mesh_index))
                end
                gertzbein_distance_mm(try_idx) = round(gertzbein_distance*1000,2);
                g = gertzbein_distance_mm(try_idx);
                if g <= 0
                    gertzbein_class(try_idx) = "A";
                elseif 0 < g && g < 2
                    gertzbein_class(try_idx) = "B";
                elseif 2 <= g && g < 4
                    gertzbein_class(try_idx) = "C";
                elseif 4 <= g && g < 6
                    gertzbein_class(try_idx) = "D";
                else % > 6 mm
                    gertzbein_class(try_idx) = "E";
                end
                fprintf('Proband %i, Try %i: Gertzbein-Distance = %2.2f[mm]. Class = %s\n', proband_id, try_idx, gertzbein_distance_mm(try_idx), gertzbein_class(try_idx));
            end
                      
            % Calculating Gertzbein Classification for used screw (d = 5.5 mm ?)
            % ----------------------------------------------------------------
            % 1) Compute the pedicle mesh bounding box in 3D. #done
            % 1a match screw <-> pedicle (closest distance endpoint) w/ pdist() #done
            % 1b -> matching pedicle_mesh (not handle) #done
            % 1c bounding box creation (matched mesh) #done
            % 1c min x/y/z + max x/y/z -> quader (for loop for every coordinate
            % pediclemesh) #done
            % pedicle_bbox = [min x, max x, min y, max y, min z, max z]; #done
            % min-befehl auf matrix (welche col/row) min(matched_mesh.v,[],dim) #done
            % 2) For ALL screw mesh vertices that are INSIDE the bounding box, compute
            % if(screwmehs.v -> x/y/z inside bbox) ..
            % 2.a) The difference vectors to ALL pedicle mesh vertices.
            % diff_vector_matrix[(n x m) x 3]  n: screwmesh.v (in bbox), m: pediclemesh.v
            %
            % 2.b) The angle between the pedicle vertex normal and the difference
            % vector.
            % get normals from mesh handle patches -> handle_normals
            % plot normals witch arrow();
            %
            % length(vec) = sqrt (dot(vec,vec))
            % angle = (acos(dot(handle_normals, diff_vector_matrix/length(diff_vector_matrix))) / pi) * 180 (skalarprod!)
            % 2.c) The length of the projection of difference vector onto normal
            % vector.
            % filter angle for pairs < 10 degree
            % calc for each of em:
            % length = dot(filtered_handle_normals, filtered_diff_vector_matrix)
            % 3) For all difference vectors that have an angle below 10 degrees, store
            % the maximum length unter all projections.
            
            % MATTHIAS:
            % 1. For all normals of the pedicle-vertices try to find the farest
            % distances to the screw outer diameter.
            % If < 0 -> Gertzbein A
            % 0 - 2mm = Gertzbein B
            % 2-4 mm = Gertzbein C
            % 4-6 mm = Gertzbein D
            
            % screwmesh_handle_patch
            % use box bound to pedicle-mesh dimensions to set right region for calc
            % get ISONORMALS for directions
            % n = isonormals(V,vertices) OR n = isonormals(V,p) (mesh.v vs handle)
            % pdist() function for calc distance normals direction
            
            % note: p18 = 2x L2 (order of pedicle meshes: L2 left, L2 right, L3 left..)
            % example: 1st try -> try_idx = 1; p = 2 (L2 right) should be minimal
            % pdist2(pediculation_positions(:,1),pm_handle_patch(2))
            
            % Handles from plot:
            %
            % gertzbein_figure_handle
            % pm_handle_patch(pedicle_mesh_no)
            % screwmesh_handle_patch(try_idx)
            % pediculation_positions(1:3,try_idx)
        end
        
                 
        function [table_w_groups] = AssignGroups(obj, table)
            % Assigns a column 'groups' to a result table.
            % Cave: Only valid for a connected series of probands
            % e.g. 2:5; not: 2,5,7 !
            
            definitions;
            probands = obj.proband_ids_;
            T = sortrows(table,'try_id');
            % Assigning exp/nov from definitions
            T.group = [groups_sorted_by_try_id(probands(1):probands(end)); groups_sorted_by_try_id(probands(1):probands(end))];
            T.group = categorical(T.group);
            T = T(:,[1,2,end,3:end-1]);
            table_w_groups = T;
        end
    end
end



