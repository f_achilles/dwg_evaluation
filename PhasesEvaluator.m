classdef PhasesEvaluator < handle & Logger
    
    properties
        SyntrackIo_ref;
        results_ = table();
        phases_indices_;    % struct of events' indices ('skin cut', 'bone entered')
        proband_ids_;
        count = 0;
    end
    
    properties (Constant)
        kClassName = 'PhasesEvaluator';
        kClassVersion = 'V2.0';
        % Changelog XRayEvaluator.m
        % V1.0 (01-04-2020): Introduced class versioning.
        % V2.0 (19-05-2020): Important changes! 'enter_bone' has now three
        % indices: 1) idx_bone_entered_first 2) idx_bone_entered_last 3) idx_bone_perf_ep <- derived from
        % manually identified EP (via bone mesh, loaded from definitions)
        % time_bone_end = time(idx_bone_entered_first) -> time(end)
        % time_bone_entered_first_perf_ep = time(idx_bone_entered_first) ->
        % time(idx_bone_perf_ep)
        kFileNameResultsXls = [PhasesEvaluator.kClassName,'_results.xlsx'];
        kFileNameResultsTxt = [PhasesEvaluator.kClassName,'_results_',date(),' - ',PhasesEvaluator.kClassVersion,'.txt'];
        kFileNameResultsSheetName = [date(), ' - ', PhasesEvaluator.kClassVersion];
        kVerbose = true;
    end

    methods
        function obj = PhasesEvaluator(io_ref)
            % TODO (MM): Check if clean data exists
            obj.SyntrackIo_ref = io_ref;
            obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
            probands = obj.proband_ids_;
            
            if(isempty(obj.SyntrackIo_ref.clean_data_table_))
                success = 'error. no clean data available. please execute DataCleaner first';
            end
            obj.Message("> Evaluating Phases and Durations for p" + probands(1) + " - p" + probands(end) + "...");
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for PhasesEvaluator class are all working');
            else
                warning('Not all Tests for PhasesEvaluator class are working. Please debug and fix.');
            end
        end
        
        function success = EvaluateProbands(obj)
            proband_ids = obj.proband_ids_;
            var_names = {'p_id', 'try_id', 'time_total', 'time_start_to_skin', 'time_skin_to_bone', 'time_bone_to_end', 'time_bone_entered_first_perf_ep'};
            var_types(1:length(var_names)) = {'double'};
            
            T_results = table('Size', [2*length(proband_ids),length(var_names)], 'VariableTypes', var_types, 'VariableNames', var_names);
            row_no = 1;
            
            for p_id = proband_ids
                for try_id = 1:2
                    [time_total, time_start_to_skin, time_skin_to_bone, time_bone_to_end, time_bone_entered_first_perf_ep, idx_skin_cut, idx_bone_entered_first, idx_bone_perf_ep] = obj.CalcTime(p_id, try_id);
                    
                    obj.phases_indices_{p_id, try_id}.skin_cut = idx_skin_cut;
                    obj.phases_indices_{p_id, try_id}.bone_entered_first = idx_bone_entered_first;
                    obj.phases_indices_{p_id, try_id}.bone_perf_ep = idx_bone_perf_ep;
                    
                    T_newrow = {p_id, try_id, time_total, time_start_to_skin, time_skin_to_bone, time_bone_to_end, time_bone_entered_first_perf_ep};
                    T_results(row_no,:) = T_newrow;
                    row_no = row_no + 1;
                end
            end
            
            % Write label number '9' (manual EP detection / idx_bone_last_entered) to CleanDataTable !
            for p_id = proband_ids
                for try_id = 1:2
                    write_success = obj.WriteLabel2CleanDataTable(p_id, try_id);
                    if(write_success)
                        obj.Message(sprintf("> p%i, try%i: Label '9' written into clean_data_table_ @ 'idx_bone_entered_last'.", p_id, try_id));
                    end
                end
            end
            
            % Assign groups and saving results into a table
            T_results = obj.AssignGroups(T_results);
            obj.results_ = T_results;
            obj.Message("Displaying results of Phases Evaluation:");
            disp(obj.results_);
            
            success = true;
            obj.SyntrackIo_ref.flag_phases_evaluation_done = true;
        end
        
        function success = SaveResults2Tables(obj)
            T = obj.results_;
            try
                warning('off', 'MATLAB:xlswrite:AddSheet');
                writetable(T,obj.kFileNameResultsXls, 'sheet', obj.kFileNameResultsSheetName);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsXls + "' with worksheet named after actual date.");
                
                writetable(T,obj.kFileNameResultsTxt);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsTxt + "'.");
                success = true;
            catch
                success = false;
                obj.Message("Error saving results!");
            end
        end
        
        function success = SaveResults2Io(obj)
            svd = obj.phases_indices_;
            results_struct = struct();
            results_struct.phases_indices_ = svd;
            results_struct.data_table_ = obj.results_;
            obj.SyntrackIo_ref.results_.PhasesEvaluator = results_struct;
            obj.Message("Data results from '" + obj.kClassName + "' saved into Io.results_.");
        end
    end
    
    methods (Access = private)
        
        function write_success = WriteLabel2CleanDataTable(obj, p_id, try_id)
            if(~isempty(obj.phases_indices_))
            idx_bone_perf_ep = obj.phases_indices_{p_id,try_id}.bone_perf_ep;
            obj.SyntrackIo_ref.clean_data_table_{p_id, try_id}.label(idx_bone_perf_ep) = 9;
            write_success = true;
            else
                obj.Message("No phases evaluation found. Please execute!");
                write_success = false;
            end
        end
        
        function [time_total, time_start_to_skin, time_skin_to_bone, time_bone_to_end, time_bone_entered_first_perf_ep, idx_skin_cut, idx_bone_entered_first, idx_bone_perf_ep] = CalcTime(obj, p_id, try_id)
            % Helper function to calculate durations of phases from (datetime) time stamps
            definitions;
            dataset_table = obj.SyntrackIo_ref.clean_data_table_{p_id, try_id};
            
            time_total = seconds(dataset_table.datetime_relative(end) - dataset_table.datetime_relative(1));
            % Get position of first 'skin_cut' event and split durations
            idx_skin_cut = find(dataset_table.label == label_numbers.cut);
            idx_skin_cut = idx_skin_cut(1);
            time_start_to_skin = seconds(dataset_table.datetime_relative(idx_skin_cut) - dataset_table.datetime_relative(1));
            % Calc time from first 'skin_cut' to first 'bone_entered'
            idx_bone_entered = find(dataset_table.label == label_numbers.enter_bone);
            idx_bone_entered_first = idx_bone_entered(1);
            idx_bone_entered_last = idx_bone_entered(end); % (not valid, only 30% of screws ~ idx_bone_perf_ep
            % Getting position via manual determined EP positions (on bone mesh)
            poses = dataset_table.instrument_pose;
            positions = [poses.pos];
            positions = reshape(positions,3,[]);
            %haystack = positions';
            %needle = path_bone_entered_coordinates{p_id, try_id};
            %idx_bone_perf_ep = knnsearch(haystack, needle); %alternative
            %function
            entry_point = path_bone_entered_coordinates{p_id, try_id};
            instr_perf_ep_distances = sqrt(sum((positions' - entry_point).^2, 2));
            [~, idx_bone_perf_ep] = min(instr_perf_ep_distances);
            time_skin_to_bone = seconds(dataset_table.datetime_relative(idx_bone_entered_first) - dataset_table.datetime_relative(idx_skin_cut));
            
            % Calc time from 'bone_entered_last' to 'end'
            time_bone_to_end = seconds(dataset_table.datetime_relative(end) - dataset_table.datetime_relative(idx_bone_entered_first));
            % Calc time from 'bone_entered_first' to 'bone_entered_last'
            time_bone_entered_first_perf_ep = seconds(dataset_table.datetime_relative(idx_bone_perf_ep) - dataset_table.datetime_relative(idx_bone_entered_first));
        end
        
        function [table_w_groups] = AssignGroups(obj, table)
            % Assigns a column 'groups' to a result table.
            % Cave: Only valid for a connected series of probands
            % e.g. 2:5; not: 2,5,7 !
            
            definitions;
            probands = obj.proband_ids_;
            T = sortrows(table,'try_id');
            % Assigning exp/nov from definitions
            T.group = [groups_sorted_by_try_id(probands(1):probands(end)); groups_sorted_by_try_id(probands(1):probands(end))];
            T.group = categorical(T.group);
            T = T(:,[1,2,end,3:end-1]);
            table_w_groups = T;
        end
    end
end