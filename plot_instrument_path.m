%% Plot instrument path in 3D
hold on
poses = all_recordings(proband_id).instrument_pose;
positions = [poses.pos];
positions = reshape(positions,3,[]);

% Use annotations to crop the paths. Plot one path per attempt.
pediculation_positions = [];
cut_framenumbers = annotation_events.frame_number(...
    annotation_label.cut == annotation_events.label);
while (numel(cut_framenumbers) ~= numel(experiment.endpoints))
    latest_endpoints = [];
    for i = 1:(numel(annotation_events.label) - 1)
        if (annotation_events.label(i) == annotation_label.end &&...
            annotation_events.label(i+1) == annotation_label.end)
            latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i+1));
        elseif (annotation_events.label(i) == annotation_label.end)
            latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i));
        elseif (i == (numel(annotation_events.label) - 1) && ...
                annotation_events.label(i+1) == annotation_label.end)
            latest_endpoints = cat(1,latest_endpoints,annotation_events.frame_number(i+1));
        end
    end
    experiment.endpoints = unique(latest_endpoints);
    
    earliest_cutpoints = [];
    candidates = [];
    for endpoint_idx = 1:numel(experiment.endpoints)
        for i = 1:numel(cut_framenumbers)
            if (cut_framenumbers(i) < experiment.endpoints(endpoint_idx))
                candidates = cat(1,candidates,cut_framenumbers(i));
            end
        end
        if (isempty(earliest_cutpoints))
            earliest_cutpoints = cat(1,earliest_cutpoints,min(candidates));
        else
            for j = 1:numel(candidates)
                if(candidates(j) > experiment.endpoints(endpoint_idx-1))
                    earliest_cutpoints = cat(1,earliest_cutpoints,candidates(j));
                    break;
                end
            end
        end
    end
    cut_framenumbers = unique(earliest_cutpoints);
end
for try_idx = 1:numel(experiment.endpoints)
    cut_idx = cut_framenumbers(try_idx);
    end_idx = experiment.endpoints(try_idx);
    pediculation_positions = positions(:,cut_idx:end_idx);
    plot3(pediculation_positions(1,:), pediculation_positions(2,:), pediculation_positions(3,:))
end
% axis image vis3d
% xlabel('X-Achse')
% ylabel('Y-Achse')
% zlabel('Z-Achse')
title(['Trajektorie von Proband ' num2str(proband_id)...
      ', file: ' filename_proband{proband_id}],...
      'interpreter', 'none')

% TODO(FA): Plot the Bonemesh movement as well
% patient_poses = all_recordings.patient_pose;
% patient_positions = [patient_poses.pos];
% patient_positions = reshape(patient_positions,3,[]);
% % figure;
% plot3(patient_positions(1,:), patient_positions(2,:), patient_positions(3,:))
hold off