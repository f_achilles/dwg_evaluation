% Definitions.
gerztbein_vector_maximum_angle = 10;

% drawnow seems to be required, because we need access to the patch object
% of the pedicle mesh, in order to get the normals. (for now)
figure(gertzbein_figure_handle);
drawnow;

% Compute bounding boxes.
bbox = zeros(6,6);
% bbox : [min_x max_x min_y max_y min_z max_z];
for p = 1:6
    bbox(p,1) = min(pm_handle_patch(p).Vertices(:,1));
    bbox(p,2) = max(pm_handle_patch(p).Vertices(:,1));
    bbox(p,3) = min(pm_handle_patch(p).Vertices(:,2));
    bbox(p,4) = max(pm_handle_patch(p).Vertices(:,2));
    bbox(p,5) = min(pm_handle_patch(p).Vertices(:,3));
    bbox(p,6) = max(pm_handle_patch(p).Vertices(:,3));
end
% Compute bbox centers.
bbox_centers = zeros(6,3);
for p = 1:6
    bbox_centers(p,1) = mean(bbox(p,[1 2]));
    bbox_centers(p,2) = mean(bbox(p,[3 4]));
    bbox_centers(p,3) = mean(bbox(p,[5 6]));
%% DEBUG plot
%     hold on;
%     plot3(bbox_centers(p,1), bbox_centers(p,2), bbox_centers(p,3),...
%         '.','MarkerSize',30,'Color','red');
%     hold off
%%
end

% Assign screws of try 1 and 2 to their matching pedicle.
% Compute every possible combination: 2 attempts + each 6 possible pedicles
min_distances = pdist2(pediculation_positions(:, [1 2])', bbox_centers);
[~, pedicle_indices_for_try] = min(min_distances, [], 2);

for try_idx = 1:2
    c_pedicle_idx = pedicle_indices_for_try(try_idx);
    c_bbox_1x6 = bbox(c_pedicle_idx, :);
    c_screw_vertices_Nx3 = screw_meshes{try_idx}.v';
    indices_of_screw_vertices_inside_bbox_binary =...
        c_screw_vertices_Nx3(:,3) > c_bbox_1x6(5) &...
        c_screw_vertices_Nx3(:,3) < c_bbox_1x6(6);
    indices_of_screw_vertices_inside_bbox =...
        find(indices_of_screw_vertices_inside_bbox_binary);
%% DEBUG plot
%     hold on;
%     for screw_vertex_idx = indices_of_screw_vertices_inside_bbox'
%         plot3(c_vertices_Nx3(screw_vertex_idx,1),...
%             c_vertices_Nx3(screw_vertex_idx,2),...
%             c_vertices_Nx3(screw_vertex_idx,3),...
%             '.','MarkerSize',30,'Color','blue');
%     end
%     hold off
%%    
    filtered_screw_vertices_nx3 =...
        c_screw_vertices_Nx3(indices_of_screw_vertices_inside_bbox,:);
    
    c_pedicle_vertices_mx3 = pm_handle_patch(c_pedicle_idx).Vertices;
    
    % Get the numbers of vertices for pedicle mesh (m) and screw (n).
    m_pedicle_vertices = size(c_pedicle_vertices_mx3, 1);
    n_screw_vertices = numel(indices_of_screw_vertices_inside_bbox);
    
    % Compute the difference vectors that go from pedicle to screw.
    difference_vectors_nmx3 = bsxfun(@minus,...
        repmat(filtered_screw_vertices_nx3, m_pedicle_vertices, 1),...
        reshape(repmat(c_pedicle_vertices_mx3',n_screw_vertices,1), 3, [])');
    
    % Get vertex normals and normalize them to length 1.
    c_pedicle_vertex_normals_mx3 = double(pm_handle_patch(c_pedicle_idx).VertexNormals);
    c_pedicle_vertex_normals_mx3 = bsxfun(@rdivide, c_pedicle_vertex_normals_mx3,...
        sqrt(sum(c_pedicle_vertex_normals_mx3.*c_pedicle_vertex_normals_mx3, 2)));
%% DEBUG plot (Shows that the original patch Vertex Normals all point inside!)
%     xyz = c_pedicle_vertices_mx3;
%     uvw = c_pedicle_vertex_normals_mx3 * 0.005;
%     hold on
%     quiver3(...
%         xyz(:,1),xyz(:,2),xyz(:,3),...
%         uvw(:,1),uvw(:,2),uvw(:,3),'b');
%     hold off
%%
    c_pedicle_vertex_normals_mx3 = -c_pedicle_vertex_normals_mx3;
    
    % Use 3rd PCA-Component of the pedicle mesh instead of the normals, to
    % define the pedicle-plane. The normal's task is now taken over by the
    % normalized projection of the difference vector onto the pedicle
    % plane.
    principal_component_vectors = pca(c_pedicle_vertices_mx3)';
    pca_vector_long = principal_component_vectors(1,:);
    pca_vector_long = pca_vector_long./norm(pca_vector_long);
    pca_vector_short = principal_component_vectors(2,:);
    pca_vector_short = pca_vector_short./norm(pca_vector_short);
    pedicle_center = mean(c_pedicle_vertices_mx3,1);
    % project the normals onto the pedicle plane
    for i_p_v = 1:m_pedicle_vertices
        c_normal_vec = c_pedicle_vertex_normals_mx3(i_p_v,:);
        projection_length_on_short_pedicle_axis = dot(pca_vector_short,c_normal_vec);
        projection_length_on_long_pedicle_axis = dot(pca_vector_long,c_normal_vec);
        c_normal_vec_in_pedicle_plane =...
            projection_length_on_long_pedicle_axis * pca_vector_long +...
            projection_length_on_short_pedicle_axis * pca_vector_short;
        c_normal_vec_in_pedicle_plane =...
            c_normal_vec_in_pedicle_plane ./ norm(c_normal_vec_in_pedicle_plane);
        c_pedicle_vertex_normals_mx3(i_p_v,:) = c_normal_vec_in_pedicle_plane;
    end
%% DEBUG plot for PCA
%     xyz = repmat(pedicle_center,3,1);
%     uvw = principal_component_vectors;
%     hold on
%     quiver3(...
%         xyz(:,1),xyz(:,2),xyz(:,3),...
%         uvw(:,1),uvw(:,2),uvw(:,3),'r');
%     hold off
%%
    % Normalize the difference vectors in order to compute their angle with
    % the pedicle vertex normals.
    difference_vector_lenghts_nxmpedicle = pdist2(...
        filtered_screw_vertices_nx3, c_pedicle_vertices_mx3);
    difference_vectors_nmx3_normalized =...
        difference_vectors_nmx3 ./...
        repmat(difference_vector_lenghts_nxmpedicle(:),1,3);
    
    % Compute the length of the projection of the difference vectors onto
    % the pedicle vertex normals.
    projected_lengths_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
    normal_and_diffvec_angles_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
    negative_normal_and_diffvec_angles_nxm = zeros(n_screw_vertices, m_pedicle_vertices);
    for i_p_v = 1:m_pedicle_vertices
        projected_lengths_nxm(:, i_p_v) =...
        difference_vectors_nmx3((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
        * c_pedicle_vertex_normals_mx3(i_p_v, :)';
    
        normal_and_diffvec_angles_nxm(:, i_p_v) = (180/pi)*acos(...
        min(ones(n_screw_vertices,1,'double'),...
        difference_vectors_nmx3_normalized((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
        * c_pedicle_vertex_normals_mx3(i_p_v, :)')...
        );
    
        negative_normal_and_diffvec_angles_nxm(:, i_p_v) = (180/pi)*acos(...
        min(ones(n_screw_vertices,1,'double'),...
        difference_vectors_nmx3_normalized((1:n_screw_vertices) + n_screw_vertices*(i_p_v - 1),:)...
        * -c_pedicle_vertex_normals_mx3(i_p_v, :)')...
        );
    end
    
    % Filter lengths that arise because the angle between normal and
    % difference vector is too high.
    projected_lengths_nxm(...
        ((projected_lengths_nxm>0) & (normal_and_diffvec_angles_nxm > gerztbein_vector_maximum_angle)) |...
        ((projected_lengths_nxm<0) &(negative_normal_and_diffvec_angles_nxm > gerztbein_vector_maximum_angle)))...
        = -1000;
    [values, pedicle_mesh_indices] = max(projected_lengths_nxm, [], 2);
    [gertzbein_distance, screw_mesh_index] = max(values);
    pedicle_mesh_index = pedicle_mesh_indices(screw_mesh_index);
    % DEBUG plot
    if gertzbein_distance > 0
        uvw = difference_vectors_nmx3((pedicle_mesh_index-1) * n_screw_vertices + screw_mesh_index,:);
        xyz = c_pedicle_vertices_mx3(pedicle_mesh_index,:);
        uvw_normal = c_pedicle_vertex_normals_mx3(pedicle_mesh_index,:);
        uvw_normal = uvw_normal * 0.01;
        hold on
        quiver3(xyz(1),xyz(2),xyz(3),uvw(1),uvw(2),uvw(3),'g');
        quiver3(xyz(1),xyz(2),xyz(3),uvw_normal(1),uvw_normal(2),uvw_normal(3),'w');
        hold off
        fprintf('Proband %i, Try %i: Angle: %2.2f\n',proband_id, try_idx, ...
        normal_and_diffvec_angles_nxm(screw_mesh_index, pedicle_mesh_index))
    end
    gertzbein_distance_mm(proband_id, try_idx) = gertzbein_distance*1000;
    g = gertzbein_distance_mm(proband_id, try_idx);
    if g <= 0
        gertzbein_class(proband_id, try_idx) = "A";
    elseif 0 < g && g < 2
        gertzbein_class(proband_id, try_idx) = "B";
    elseif 2 <= g && g < 4
        gertzbein_class(proband_id, try_idx) = "C";
    elseif 4 <= g && g < 6
        gertzbein_class(proband_id, try_idx) = "D";
    else % > 6 mm
        gertzbein_class(proband_id, try_idx) = "E";
    end
    fprintf('Proband %i, Try %i: Gertzbein-Distance = %2.2f[mm]. Class = %s\n', proband_id, try_idx, gertzbein_distance_mm(proband_id, try_idx), gertzbein_class(proband_id, try_idx));
end

% Calculating Gertzbein Classification for used screw (d = 5.5 mm ?)
% ----------------------------------------------------------------
% 1) Compute the pedicle mesh bounding box in 3D. #done
% 1a match screw <-> pedicle (closest distance endpoint) w/ pdist() #done
% 1b -> matching pedicle_mesh (not handle) #done
% 1c bounding box creation (matched mesh) #done
% 1c min x/y/z + max x/y/z -> quader (for loop for every coordinate
% pediclemesh) #done
% pedicle_bbox = [min x, max x, min y, max y, min z, max z]; #done
% min-befehl auf matrix (welche col/row) min(matched_mesh.v,[],dim) #done
% 2) For ALL screw mesh vertices that are INSIDE the bounding box, compute
% if(screwmehs.v -> x/y/z inside bbox) ..
% 2.a) The difference vectors to ALL pedicle mesh vertices.
% diff_vector_matrix[(n x m) x 3]  n: screwmesh.v (in bbox), m: pediclemesh.v
% 
% 2.b) The angle between the pedicle vertex normal and the difference
% vector.
% get normals from mesh handle patches -> handle_normals
% plot normals witch arrow();
%
% length(vec) = sqrt (dot(vec,vec)) 
% angle = (acos(dot(handle_normals, diff_vector_matrix/length(diff_vector_matrix))) / pi) * 180 (skalarprod!)
% 2.c) The length of the projection of difference vector onto normal
% vector.
% filter angle for pairs < 10 degree
% calc for each of em:
% length = dot(filtered_handle_normals, filtered_diff_vector_matrix)
% 3) For all difference vectors that have an angle below 10 degrees, store
% the maximum length unter all projections.

% MATTHIAS:
% 1. For all normals of the pedicle-vertices try to find the farest
% distances to the screw outer diameter. 
% If < 0 -> Gertzbein A
% 0 - 2mm = Gertzbein B
% 2-4 mm = Gertzbein C
% 4-6 mm = Gertzbein D

% screwmesh_handle_patch
% use box bound to pedicle-mesh dimensions to set right region for calc
% get ISONORMALS for directions 
% n = isonormals(V,vertices) OR n = isonormals(V,p) (mesh.v vs handle)
% pdist() function for calc distance normals direction

% note: p18 = 2x L2 (order of pedicle meshes: L2 left, L2 right, L3 left..)
% example: 1st try -> try_idx = 1; p = 2 (L2 right) should be minimal
% pdist2(pediculation_positions(:,1),pm_handle_patch(2))

% Handles from plot:
%
% gertzbein_figure_handle
% pm_handle_patch(pedicle_mesh_no)
% screwmesh_handle_patch(try_idx)
% pediculation_positions(1:3,try_idx)