%% Read a pediculation study recoring into a struct

%proband_id = 7;    % manual usage

rec_file_path = fullfile('recordings_pedikulierung', filename_proband{proband_id});

rec_struct = struct('ts', [], 'frame_nr', [], 'label', [], 'xray_event', [], ...
                          'cumulative_xray_events', [], 'orbital_angle', [], 'tilt_angle', [], ...
                          'axial_translation', [], 'cpad_switch_code', [], 'instrument_pose', [],...
                          'patient_pose', []);

file_handle = fopen(rec_file_path);
% Skip header row.
fgetl(file_handle);

rec_row_idx = 1;

while(~feof(file_handle))
    data_row_part_1 = textscan(file_handle, '%s %s %d %d %d %d %f %f %f %d', 1);
    if (isempty(data_row_part_1{1}))
        break;
    end
    data_row_part_2 = textscan(file_handle, '%s %f, %f, %f (%f| %f, %f, %f) ', 3);
    
    rec_struct.ts(rec_row_idx) = ...
    datenum([data_row_part_1{1}{1} ' ' data_row_part_1{2}{1}], 'yyyy-mm-dd HH:MM:SS.FFF');
    rec_struct.frame_nr(rec_row_idx) = data_row_part_1{3};
    rec_struct.label(rec_row_idx) = data_row_part_1{4};
    rec_struct.xray_event(rec_row_idx) = data_row_part_1{5};
    rec_struct.cumulative_xray_events(rec_row_idx) = data_row_part_1{6};
    rec_struct.orbital_angle(rec_row_idx) = data_row_part_1{7};
    rec_struct.tilt_angle(rec_row_idx) = data_row_part_1{8};
    rec_struct.axial_translation(rec_row_idx) = data_row_part_1{9};
    rec_struct.cpad_switch_code(rec_row_idx) = data_row_part_1{10};
    
    rec_struct.instrument_pose(rec_row_idx).pos = ...
    [data_row_part_2{2}(1) data_row_part_2{3}(1) data_row_part_2{4}(1)];
    rec_struct.instrument_pose(rec_row_idx).rot = ...
        quat2dcm([data_row_part_2{5}(1) data_row_part_2{6}(1) data_row_part_2{7}(1) data_row_part_2{8}(1)]);
    
    rec_struct.patient_pose(rec_row_idx).pos = ...
    [data_row_part_2{2}(3) data_row_part_2{3}(3) data_row_part_2{4}(3)];
    rec_struct.patient_pose(rec_row_idx).rot = ...
        quat2dcm([data_row_part_2{5}(3) data_row_part_2{6}(3) data_row_part_2{7}(3) data_row_part_2{8}(3)]);
    
    rec_row_idx = rec_row_idx + 1;
end

fclose(file_handle);
