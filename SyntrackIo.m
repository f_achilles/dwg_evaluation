% This Class is used for Input-Output operations
% (Load / Save / Update / Create backup)
%
% ToDo:
% - MM: 'GetEvaluatedData' function to join all (available) results (from other classes) within a large table
% - MM: PlotRawVsClean faulty because flags 'raw.. executed' false or
% corrected raw data saved incorrectly.

classdef SyntrackIo < handle & Logger
    % The SYNTRACKIO class handles all reading and writing events of result tables, raw/cleaned database and timeseries.
    %
    % Desired behavior:
    % - We can read in a set of probands from disk, handing over their IDs in a vector.
    % - We can save the read in probands' data, also specifying the IDs. If there is a mismatch wr.t. the read in data,
    % the user is warned and the ID is ignored.
    % - We can read and save result tables and evaluation data.
        
    properties (Constant)
        kClassName = 'SyntrackIo';
        kVerbose = true; % Show debug/info output
        kRawDatabaseVariableName = "raw_data_";
        kRawDataTemplate = struct('ts', [], 'frame_nr', [], 'label', [], 'xray_event', [], ...
                'cumulative_xray_events', [], 'orbital_angle', [], 'tilt_angle', [], ...
                'axial_translation', [], 'cpad_switch_code', [], 'instrument_pose', [],...
                'patient_pose', []);
    end
    
    % TODO (FA): Specify public and private access on these properties.
    properties
        proband_ids_ = 1:22;
        last_used_p_id_;     % last used proband_id when loading/adding/saving proband data
        proband_lumbar_vertebra_lvl_; %TODO (FA): Is this property still required?
        raw_data_filename_list_;    % live recorded (raw) filename list loaded from definitions
        raw_data_labelnumber_;      % annotation label codes loaded from definitions
        raw_data_ = SyntrackIo.kRawDataTemplate;
        clean_data_; %= SyntrackIo.kRawDataTemplate;
        clean_data_table_;          % 'Cleaned' dataset after DataCleaner usage
        evaluation_data_table_;     % Table from GetEvaluationData (44 rows, p_id, try_id, exp/nov)
        evaluation_data_table_joined_with_manual_data_ = table();
        statistics_data_table_;     % Table from statistics calculations results
        flag_clean_data_successfully_loaded_ = false;   % flag eval done yes/no
        flag_xray_evaluation_done_ = false;             % flag eval done yes/no
        flag_gertzbein_evaluation_done_ = false;        % flag eval done yes/no
        flag_aoms_evaluation_done_ = false;             % flag eval done yes/no
        flag_phases_evaluation_done = false;            % flag eval done yes/no
        raw_data_format_version_ = 1;
        
        results_;   % cell to save various results from other classes
    end
    
    properties (Access = private)
        % use this switch to immediately generate a 'data_pN.mat' after a read-in 
        % of original recordings (.txts) was neccessary (reading and converting 
        % takes up to 3 minutes per file!)
        raw_data_autosave_when_txt_loaded = true;
    end

    methods
        function obj = SyntrackIo(raw_data_format_version)
            %SYNTRACKIO Construct an instance of this class.
            obj.LoadDefinitions;
            if(~obj.kVerbose), disp('No output of messages from class (kVerbose = false).'); end
            if exist('raw_data_format_version','var')
                obj.raw_data_format_version_ = raw_data_format_version;
            end
        end
        
        function success = RunTests(obj)
            % Tests all the class methods and returns 'success'.
            success = true;

            success = success & obj.LoadRawData(1:22);
            obj.raw_data_ = [];

            success = success & obj.LoadResultTable("proband_data_retrys.txt");
            obj.result_table_ = [];

            if success
                disp('Tests for SyntrackIo class are all working');
            else
                warning('Not all Tests for SyntrackIo class are working. Please debug and fix.');
            end
        end
        
        function success = LoadDefinitions(obj)
            % Loads definitions.m to pre-assign study relevant values
            definitions;
            obj.raw_data_filename_list_ = filename_proband';    %see definitions.m
            obj.raw_data_labelnumber_ = annotation_label;
            obj.proband_lumbar_vertebra_lvl_ = lumbar_vert_lvl_of_proband;
            success = ~isempty(obj.raw_data_filename_list_);
            obj.Message("Syntrack database definitions loaded.");
        end
        
        function success = LoadData(obj, proband_ids)
            % Main function for loading data
            % Checks if there is cleaned data available
            % else loads raw data
            data_filename = 'clean_data_p1-p22.mat';
            if (exist(data_filename, 'file'))
                %Load everything without cleaning
                obj.LoadCleanData();
            else
                % Using args if provided else using defaults:
                if exist('proband_ids','var')
                    obj.proband_ids_ = proband_ids;
                end
                % Load raw data from recorded .txt or raw data in .mat
                obj.LoadRawData(obj.proband_ids_);
                % Auto execute DataCleaner for processing Raw data
                data_cleaner = DataCleaner(obj);
                data_cleaner.CleanData();
            end
        end

        function success = LoadRawData(obj, proband_ids)
            % Reads raw data from n .txt files and stores them into struct obj.raw_data_(n)
            %   n = p_id_end - p_id_start + 1
            %   Check for saved structs (.mat files) OR read .txt files
            %   into struct-array with number of probands
            %
            % TODO (FA): Refactor, make this work with the probands vector.
            
            % Using args if provided else using defaults:
            if exist('proband_ids','var')
                obj.proband_ids_ = proband_ids;
            end
            
            % Load raw data from .txt files or from saved .mat files
            for p_id = obj.proband_ids_
                data_filename = ['data_p', num2str(p_id), '.mat'];

                try % Error handling for file IO
                % Check if .mat file exists else LOAD it from recorded .txts FILES (! takes up to 3 minutes per .txt!).
                    if (~exist(data_filename, 'file'))
                        obj.ReadDatabaseFromTextFile(p_id);
                        % Save variable for later usage if set in properties
                        if (~exist(data_filename,'file') && obj.raw_data_autosave_when_txt_loaded)                    
                            obj.SaveRawData(data_filename, p_id);
                            obj.Message("Autosaved content of .txt-file into '" + data_filename + "'.");
                        end
                    else
                        % Load data from workspace-saved .mat file.
                        obj.Message("Found saved data for proband p" + p_id + ". Reading .mat file...");
                        temp = load(data_filename, 'raw_data');
                        obj.raw_data_(p_id) = temp.raw_data;
                        clear temp;
                        obj.Message("Raw Data for proband p" + p_id + " loaded from <" + data_filename + ">.");
                    end

                    % Return success.
                    if ~isempty(obj.raw_data_)
                        success = true;
                    end

                % Error handling in case of IO error    
                catch 
                    obj.Message("IO error. Tried to save or load nonexistent file or variable!");
                    success = false;
                end

            end
        end
        
        function success = SaveRawData(obj, data_filename, p_id)
            raw_data = obj.raw_data_(p_id);
            save(data_filename, 'raw_data');
        end
        
        function success = SaveCleanData(obj)
            clean_data = obj.clean_data_;
            clean_data_table = obj.clean_data_table_;
            data_filename = 'clean_data_p1-p22.mat';
            
            save(data_filename, 'clean_data','clean_data_table');
            
            obj.Message("Data successfully saved to 'clean_data_p1-p22.mat'.");
        end
        
        function success = LoadCleanData(obj)
            try
                obj.Message("Cleaned data found!");
                obj.Message("> Loading 'clean_data_' and 'clean_data_table_' for all probands p1 - p22...");
                % Loading variables into class
                temp = load('clean_data_p1-p22.mat','clean_data');
                obj.clean_data_ = temp.clean_data;
                clear temp;
                temp = load('clean_data_p1-p22.mat','clean_data_table');
                obj.clean_data_table_ = temp.clean_data_table;
                clear temp;
            catch
                error('Error. Could not load clean_data from saved .mat file!');
                success = 0;
                obj.flag_clean_data_successfully_loaded_ = false;
            end
            if((~isempty(obj.clean_data_) && ~isempty(obj.clean_data_table_)))
                success = true;
                obj.flag_clean_data_successfully_loaded_ = true;
                obj.Message("Cleaned data successfully loaded.");
            end
        end
        
        function success = LoadResultTable(obj, result_table_filename)
            % Reads a result data table from existing .txt files 
            % Format: [Class name]_results.txt
            try
                obj.result_table_ = readtable(result_table_filename);
                success = 1;
            catch
                success = 0;
                obj.Message("Error reading results from " + result_table_filename + ".");
            end
        end
        
        function success = GetEvaluationData(obj, prop, value)
            % Retrieves all results and puts it into one large table -> result_table_
            % uses recent data ([ClassName].results_ OR use saved data ([ClassName]_results.xlsx)
            obj.Message("Getting results from available evaluation data...");
            if ((prop == "Source") & (value == "xlsx"))
                obj.Message("Using saved data from .xlsx only..");
                obj.JoinResultData("phases","xray","instrument","screw");
                disp(obj.evaluation_data_table_);
                obj.JoinEvaluationDataWithManualEvaluation;
            elseif ((prop == "Source") & (value == "classes"))
                obj.Message("> Retrieving data from each executed evaluator class..");
                % TODO (MM/FA): Register executed evaluator classes with
                % Logger and then retrieve result_ property from them
                % OR
                % retrieve data via SyntrackIo.results_.
                obj.Message("STOP. [-still to be implemented-]");
            else
                error("Error! Unknown properties used. Use 'xlsx' or 'classes' as 'Source' property.");
                success = 0;
            end
        end
                   
        function success = SaveEvaluationData(obj)
            % Saves results of 'evaluation_data_table_' into a .xlsx table
            % and a .txt file
            % Usage: Use 'GetEvaluationData('Source', 'xlsx'/'classes') first. 
            
            obj.Message("Saving results from available evaluation data...");
            T = obj.evaluation_data_table_;
            if(~isempty(T))
                kFileNameResultsXls = 'SyntrackIo_EvaluationData.xlsx';
                kFileNameResultsSheetName = ['EvaluationData_',date()];
                kFileNameResultsTxt = ['SyntrackIo_EvaluationData_',date(),'.txt'];
                try
                    warning('off', 'MATLAB:xlswrite:AddSheet');
                    writetable(T,kFileNameResultsXls, 'sheet', kFileNameResultsSheetName);
                    obj.Message("Results successfully saved into '" + kFileNameResultsXls + "' with worksheet named after actual date.");
                    
                    writetable(T,kFileNameResultsTxt);
                    obj.Message("Results successfully saved into '" + kFileNameResultsTxt + "'.");
                    success = true;
                catch
                    success = false;
                    obj.Message("Error saving results!");
                end
            else
                obj.Message("No evaluation data found. Use GetEvaluationData('Source','xlsx'|'classes') first. Aborting.");
            end
        end
    end
    
    methods (Access = private, Hidden = true)
        
        function [success, rec_struct] = ReadTxt2Struct(obj, p_id)
            % Read a pediculation study raw recording (.txt file) into a struct            
            obj.last_used_p_id_ = p_id;
            rec_file_path = fullfile('recordings_pedikulierung', obj.raw_data_filename_list_{p_id});
            rec_struct = obj.kRawDataTemplate;
            file_handle = fopen(rec_file_path);
            
            % Skip header row.
            fgetl(file_handle);

            rec_row_idx = 1;            
            while(~feof(file_handle))
                data_row_part_1 = textscan(file_handle, '%s %s %d %d %d %d %f %f %f %d', 1);
                if (isempty(data_row_part_1{1}))
                    break;
                end
                % Timestamp data.
                rec_struct.ts(rec_row_idx) = ...
                    datenum([data_row_part_1{1}{1} ' ' data_row_part_1{2}{1}], 'yyyy-mm-dd HH:MM:SS.FFF');
                rec_struct.frame_nr(rec_row_idx) = data_row_part_1{3};
                % Keyboard inputs.
                rec_struct.label(rec_row_idx) = data_row_part_1{4};
                rec_struct.xray_event(rec_row_idx) = data_row_part_1{5};
                rec_struct.cumulative_xray_events(rec_row_idx) = data_row_part_1{6};
                % C-Pad values.
                rec_struct.orbital_angle(rec_row_idx) = data_row_part_1{7};
                rec_struct.tilt_angle(rec_row_idx) = data_row_part_1{8};
                rec_struct.axial_translation(rec_row_idx) = data_row_part_1{9};
                rec_struct.cpad_switch_code(rec_row_idx) = data_row_part_1{10};
                
                % The second part contains pose information about
                % instruments and the patient dataset.
                %
                % TODO (FA): Test what happens if repeating this N=5 times,
                % but there are only 2, 3, 4 entries?
                if obj.raw_data_format_version_ == 1
                    data_row_part_2 = textscan(file_handle, '%s %f, %f, %f (%f| %f, %f, %f) ', 3);
                    rec_struct.instrument_pose(rec_row_idx).pos = ...
                        [data_row_part_2{2}(1) data_row_part_2{3}(1) data_row_part_2{4}(1)];
                    rec_struct.instrument_pose(rec_row_idx).rot = ...
                        quat2dcm([data_row_part_2{5}(1) data_row_part_2{6}(1) data_row_part_2{7}(1) data_row_part_2{8}(1)]);

                    rec_struct.patient_pose(rec_row_idx).pos = ...
                        [data_row_part_2{2}(3) data_row_part_2{3}(3) data_row_part_2{4}(3)];
                    rec_struct.patient_pose(rec_row_idx).rot = ...
                        quat2dcm([data_row_part_2{5}(3) data_row_part_2{6}(3) data_row_part_2{7}(3) data_row_part_2{8}(3)]);
                elseif obj.raw_data_format_version_ == 2
% V2: "attached_instrument, x, y, z, 0.0, E_x_extr, E_y_extr, E_z_extr", OLD: //"attached_instruments [x, y, z, (qw| qx, qy, qz)], ",
% V2: "patient, x, y, z, 0.0, E_x_extr, E_y_extr, E_z_extr" OLD: //"patient [x, y, z, (qw| qx, qy, qz)]"
                    data_row_part_2 = textscan(file_handle, '%s %f, %f, %f, 0.0, %f, %f, %f ', 3);
                    rec_struct.instrument_pose(rec_row_idx).pos = ...
                        [data_row_part_2{2}(1) data_row_part_2{3}(1) data_row_part_2{4}(1)];
                    rec_struct.instrument_pose(rec_row_idx).rot = ...
                        angle2dcm( ...
                        deg2rad(data_row_part_2{5}(1)), ...
                        deg2rad(data_row_part_2{6}(1)), ...
                        deg2rad(data_row_part_2{7}(1)), 'XYZ');
                    
                    rec_struct.patient_pose(rec_row_idx).pos = ...
                        [data_row_part_2{2}(3) data_row_part_2{3}(3) data_row_part_2{4}(3)];
                    rec_struct.patient_pose(rec_row_idx).rot = ...
                        angle2dcm( ...
                        deg2rad(data_row_part_2{5}(3)), ...
                        deg2rad(data_row_part_2{6}(3)), ...
                        deg2rad(data_row_part_2{7}(3)), 'XYZ');
                end
                rec_row_idx = rec_row_idx + 1;
            end
            fclose(file_handle);
            success = ~isempty(rec_struct);
        end
        
        function success = ReadDatabaseFromTextFile(obj, p_id)
            % Reads raw data from a .txt file and stores them in struct obj.raw_data_.
            
            % Force user to provide patient id.
            if ~exist('p_id', 'var')
                success = false;
                return;
            end
            
            try % Error handling for file IO.
                obj.Message("No saved data found. Reading .txt file of proband p" + p_id + "...");
                [~, obj.raw_data_(p_id)] = obj.ReadTxt2Struct(p_id);
                obj.Message("Raw Data for proband p" + p_id + " loaded.");
            catch 
                obj.Message("IO error. Tried to load nonexistent file!");
                success = false;
            end
            
            % Return success.
            if ~isempty(obj.raw_data_)
                success = true;
            end
        end
        
        function success = JoinResultData(obj, eval1, eval2, eval3, eval4)
            % Input = Results of evaluations, e.g. 'xray', 'phases',
            % 'instrument', 'screw'
            % TODO (MM): Write function to dynamically join a cell array of
            % evaluations using eval as arguments!
            xray_results_filename = 'XRayEvaluator_results.xlsx';
            phases_results_filename = 'PhasesEvaluator_results.xlsx';
            instrument_results_filename = 'InstrumentEvaluator_results.xlsx';
            screw_results_filename = 'ScrewEvaluator_results.xlsx';

            evaluations_names = {'Phases','X-ray','Instrument','Screw'};
            evaluations_filenames = {phases_results_filename, xray_results_filename,instrument_results_filename,screw_results_filename};
            
            for e = 1:numel(evaluations_names)
                obj.Message("> Getting results from " + evaluations_names{e} + " evaluation..");
                try
                    % use xlsinfo to get available worksheets 
                    % (= datasets within xlsx file)
                    [~,sheet_name]=xlsfinfo(evaluations_filenames{e});
                catch
                    error('Cannot read .xlsx file.');
                    success = 0;
                end
                for k=4:numel(sheet_name) % first 3 worksheets = empty by standard
                    obj.Message("> " + evaluations_names{e} + " evaluation dataset '" + sheet_name{k} + "' found..");
                end
                last_worksheet = sheet_name{numel(sheet_name)};
                obj.Message("Using last available dataset (" + last_worksheet + ").");
                eval_table{e} = readtable(evaluations_filenames{e}, 'Sheet', last_worksheet);
            end
            % assuming tables with rows 'p_id', 'try_id', 'group' sorted by proband_id and try_id 
            % (do only read full table with columns 1-3 from _first_ result table to avoid repeated colums)
            % ToDo(MM): Use join command here instead. with keys and so on..
            obj.evaluation_data_table_ = table();
            obj.evaluation_data_table_ = eval_table{1};
            for e = 2:numel(evaluations_names)
                obj.evaluation_data_table_ = [obj.evaluation_data_table_, eval_table{e}(:,4:end)];
            end
            
            obj.evaluation_data_table_.group = categorical(obj.evaluation_data_table_.group);
            obj.evaluation_data_table_.vertebra = categorical(obj.evaluation_data_table_.vertebra);
            obj.evaluation_data_table_.side = categorical(obj.evaluation_data_table_.side);
            obj.evaluation_data_table_.gertzbein_class = categorical(obj.evaluation_data_table_.gertzbein_class);

            success = true;
            obj.Message("Results successfully joined and saved into 'evaluation_data_table_'.");
        end
        
        function success = JoinEvaluationDataWithManualEvaluation(obj)
            % This function uses external data (manually evaluated data) to join with matlab
            % evaluated data
            T_eval = obj.evaluation_data_table_;
            obj.Message("> Trying to join evaluated data and manually evaluated data from external source 'manually_evaluated_data_import_matlab.xlsx'...");
            % import manually evaluated data table (sorted by p_id, try_id)
            % setting import options for variable types
            filename = 'manually_evaluated_data_import_matlab.xlsx';
            opts = detectImportOptions(filename);
            opts.VariableTypes = ["double", "double", "categorical", "double", "double", "double", "double", "categorical", "categorical", "categorical"];
            T_manual_evaluated_data = readtable(filename, opts);
            
            obj.evaluation_data_table_joined_with_manual_data_ = join(T_eval, T_manual_evaluated_data, 'Keys', {'p_id', 'try_id', 'group'});
            obj.Message("> Displaying resulting table:");
            disp(obj.evaluation_data_table_joined_with_manual_data_);
            obj.Message("Matlab data and manually evaluated data successfully joined.");
        end
        
        % Useful snippets
        function [success,T_recordings] = LoadRawDataToTables(obj, proband_ids)
            for i = proband_ids
                T_recordings{i} = readtable(['recordings_pedikulierung\',filename_proband{i,1}]);
            end
        end
    end
end

