% Definitions.

% getting end indices 
% bone_entry_indices =...
% cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.enter_bone);
% if numel(bone_entry_indices) > 2
%     warning('MORE THAN 2 bone_entry_indices! Delete one via format_data.m.');
%     xray_evaluation_view
%     return
% end


% poses = all_recordings(proband_id).instrument_pose;
% % getting positions and rotations
% positions = [poses.pos];
% positions = reshape(positions,3,[]);

% Collecting all annotation events from original data for defining start & end
annotation_events.frame_number = cleaned_proband_data.events.framepos;
annotation_events.label = cleaned_proband_data.events.labelcode;

% Collecting start- and endpoints of experiments in frame numbers
experiment.startpoints = annotation_events.frame_number(annotation_events.label == annotation_label.start);
experiment.endpoints = annotation_events.frame_number(annotation_events.label == annotation_label.end);

% --- Extracing data for attempts only ---
for fn = fieldnames(all_recordings(proband_id))'
    attempt(1).(fn{1}) = all_recordings(proband_id).(fn{1})(experiment.startpoints(1):experiment.endpoints(1));   
    attempt(2).(fn{1}) = all_recordings(proband_id).(fn{1})(experiment.startpoints(2):experiment.endpoints(2));
end

% --- Setting relative frame numbers and time stamps ---
for try_idx = 1:2
    attempt(try_idx).ts = attempt(try_idx).ts - all_recordings(proband_id).ts(experiment.startpoints(try_idx));  
    attempt(try_idx).frame_nr = attempt(try_idx).frame_nr - experiment.startpoints(try_idx);  
end

% drawnow seems to be required, because we need access to the patch object
% of the pedicle mesh, in order to get the normals. (for now)
figure(gertzbein_figure_handle);
drawnow;

% Compute bounding boxes.
bbox = zeros(6,6);
% bbox : [min_x max_x min_y max_y min_z max_z];
for p = 1:6
    bbox(p,1) = min(pm_handle_patch(p).Vertices(:,1));
    bbox(p,2) = max(pm_handle_patch(p).Vertices(:,1));
    bbox(p,3) = min(pm_handle_patch(p).Vertices(:,2));
    bbox(p,4) = max(pm_handle_patch(p).Vertices(:,2));
    bbox(p,5) = min(pm_handle_patch(p).Vertices(:,3));
    bbox(p,6) = max(pm_handle_patch(p).Vertices(:,3));
end
% Compute bbox centers.
bbox_centers = zeros(6,3);
for p = 1:6
    bbox_centers(p,1) = mean(bbox(p,[1 2]));
    bbox_centers(p,2) = mean(bbox(p,[3 4]));
    bbox_centers(p,3) = mean(bbox(p,[5 6]));
end
c_pedicle_idx = -1;
for try_idx = 1:2
    % Collect all annotation events again from data regarding attempt only
    annotation_events.frame_number = find(attempt(try_idx).label);
    arraypos_try_starts = find(cleaned_proband_data.events.labelcode == 1);
    arraypos_try_ends = find(cleaned_proband_data.events.labelcode == 6);
    annotation_events.label = cleaned_proband_data.events.labelcode(arraypos_try_starts(try_idx):arraypos_try_ends(try_idx));
    annotation_events.colorcode = {'green','blue','cyan','black','yellow','red','magenta','magenta'};

    eventpos = find(annotation_events.label==label_numbers.enter_bone);
    frmpos = annotation_events.frame_number(eventpos(end));
    
    poses = attempt(try_idx).instrument_pose;
    positions = [poses.pos];
    positions = reshape(positions,3,[]);
    bone_entry_position = positions(:,frmpos);
    
    % Assign screws of try 1 and 2 to their matching pedicle.
    % Compute every possible combination: 2 attempts + each 6 possible pedicles
    min_distances = pdist2(bone_entry_position', bbox_centers);
    [~, pedicle_index_for_try] = min(min_distances, [], 2);

    % plot two markers for entry positions
    hold on
    plot3(...
        bone_entry_position(1),...
        bone_entry_position(2),...
        bone_entry_position(3),...
        '*','MarkerSize',20,'Color','white');
    hold off
    if (c_pedicle_idx == pedicle_index_for_try)
        proband_id
        warning('Two times the same pedicle at bone entry!')
    end
    c_pedicle_idx = pedicle_index_for_try;
    c_bbox_1x6 = bbox(c_pedicle_idx, :);
    c_bbox_center_1x3 = bbox_centers(c_pedicle_idx, :);
    % Find most lateral point of this pedicle.
    if c_bbox_center_1x3(2) > 0 % Y > 0 means it is on the RIGHT side
        c_most_lateral_y = c_bbox_1x6(4);
        from_pedicle_to_bone_entry = bone_entry_position(2) - c_most_lateral_y;
    else
        c_most_lateral_y = c_bbox_1x6(3);
        from_pedicle_to_bone_entry = c_most_lateral_y - bone_entry_position(2);
    end
    % Positive from_pedicle_to_bone_entry means OUTSIDE!
    fprintf('p%i, try:%i Bone entry was %2.2f[mm] more lateral than the outer eye margin.\n',...
        proband_id, try_idx, from_pedicle_to_bone_entry*1000)
    lateral_distances(proband_id, try_idx) = from_pedicle_to_bone_entry*1000;
end


