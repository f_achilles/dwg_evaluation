% Statistics class is meant to calculate means, standard deviations and
% can test for significances by mann whitney-U-test or t-test
% 
% Result tables from other evaluations are joined by JoinTables()
% 

% TODO (MM): - replace 'statistics_dataset_table' with
% [syntrackIo].result_table_
classdef Statistics < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        results_ = table();             % contains calculated/evaluated statistical values
        io_evaluation_data_table_ = table();    % contains a reference to SyntrackIo.evaluation_data_table_
    end
    
    properties (Constant)
        kClassName = 'Statistics';
        kFileNameResultsXls = [Statistics.kClassName,'_results.xlsx'];
        kFileNameResultsTxt = [Statistics.kClassName,'_results_',Logger.kInfo,'.txt'];
        kVerbose = true;
    end

    methods
        function obj = Statistics(io_ref)
            obj.SyntrackIo_ref = io_ref;
            obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
            obj.io_evaluation_data_table_ = obj.SyntrackIo_ref.evaluation_data_table_;
            probands = obj.proband_ids_;
            
            if(isempty(obj.SyntrackIo_ref.clean_data_table_))
                success = 'error. no clean data available. please execute DataCleaner first';
            end
            % Get evaluation data first..
            obj.SyntrackIo_ref.GetEvaluationData('Source','xlsx');
            obj.Message("Ready to calculate statistics for p" + probands(1) + " - p" + probands(end) + "...");
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for Statistics class are all working');
            else
                warning('Not all Tests for Statistics class are working. Please debug and fix.');
            end
        end
        
        function success = SaveResults2Tables(obj)
            T = obj.results_;
            try
                warning( 'off', 'MATLAB:xlswrite:AddSheet');
                writetable(T,obj.kFileNameResultsXls, 'sheet', Logger.kInfo);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsXls + "' with worksheet named after actual date.");
                
                writetable(T,obj.kFileNameResultsTxt);
                obj.Message("Results successfully saved into '" + obj.kFileNameResultsTxt + "'.");
                success = true;
            catch
                success = false;
                obj.Message("Error saving results!");
            end
        end
        
        function mean_table = CalcMeans(obj, table_variables, grouping_variable)
            % calc means for a group (e.g. 'experienced') of a variety of
            % multiple variable-group-combinations (e.g. variables:
            % time_total, shots_ap, grouping by: group and try_id)
                       
            T = obj.io_evaluation_data_table_;
            mean_table = varfun(@mean,T,'InputVariables',table_variables,'GroupingVariables',grouping_variable);
        end
        
        function std_table = CalcStds(obj, table_variable, grouping_variable)
            % calc stds for a group (e.g. 'experienced') of a variety of
            % multiple variable-group-combinations (e.g. variables:
            % time_total, shots_ap, grouping by: group and try_id)

            T = obj.io_evaluation_data_table_;
            std_table = varfun(@std,T,'InputVariables',table_variable,'GroupingVariables',grouping_variable);
        end
        
        function joined_table = JoinCalcTables(obj, T1, T2)
            joined_table = [T1, T2(:, 4:end)];
        end
        
        function rearranged_table = RearrangeTable(obj, T)
            % function should rearrange table columns into format [mean std]
            %
            %find variables with 'mean_' in it
            %find variables with 'std_' in it.
            %for..
            %T_rearr = T.means[mean{i} std{i}];
            %rearranged_table = [T,T_rearr];
            %end
        end
        
        function significances = CalcSignificance(obj, table)
            % -- copied from old code --
%             % compute statistics using proband_table
%             
%             % mark every output line with * at the end if significant
%             significant = "";   % check for significant values p < 0.05
%             
%             x = proband_table.time_total((proband_table.group=='novice'));
%             y = proband_table.time_total((proband_table.group=='experienced'));
%             % Test the null hypothesis that the two data samples are from populations
%             % with equal means.
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: time_total -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.shots_total((proband_table.group=='novice'));
%             y = proband_table.shots_total((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: shots_total -> p = ' + string(p) + significant);
%             
%             x = proband_table.time1((proband_table.group=='novice')) - proband_table.time2((proband_table.group=='novice'));
%             y = proband_table.time1((proband_table.group=='experienced')) - proband_table.time2((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: time1 - time2 differences per group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.time1((proband_table.group=='novice'));
%             y = proband_table.time2((proband_table.group=='novice'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: time1 - time2 difference in novice group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.shots1((proband_table.group=='novice'));
%             y = proband_table.shots1((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: shots1 -> p = ' + string(p) + significant);
%             
%             x = proband_table.shots2((proband_table.group=='novice'));
%             y = proband_table.shots2((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: shots2 -> p = ' + string(p) + significant);
%             
%             x = proband_table.shots1((proband_table.group=='novice')) - proband_table.shots2((proband_table.group=='novice'));
%             y = proband_table.shots1((proband_table.group=='experienced')) - proband_table.shots2((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: shots1 - shots2 differences per group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.path_total((proband_table.group=='novice'));
%             y = proband_table.path_total((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: total path length differences per group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.path2((proband_table.group=='novice'));
%             y = proband_table.path2((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: path length 2nd try differences per group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.path1((proband_table.group=='novice'));
%             y = proband_table.path2((proband_table.group=='novice'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: path1 - path2 difference in novice group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
%             
%             x = proband_table.path1((proband_table.group=='experienced'));
%             y = proband_table.path2((proband_table.group=='experienced'));
%             [h,p,ci,stats] = ttest2(x,y);
%             if(p<0.05) significant = "*"; else significant = ""; end
%             disp('2-sided t-test: path1 - path2 difference in experienced group -> mean nov ' + string(mean(x)) + ', mean exp ' + string(mean(y)) + ' p = ' + string(p) + significant);
        end
        
        function success = GroupedMatrixPlot(obj, table_vars, grouping_param)
            % e.g. vars = [T.time_total T.time_skin_to_end T.shots_total];
            % grouping_param = T.group;
            % GroupedMatrixPlot({'time_total', 'shots_ap'}, {'group'})
            T = obj.io_evaluation_data_table_;
            
            % accessing arrays from Table
            vars = T{:,table_vars};
            grouping = T{:,grouping_param};
            
            figure();
            gplotmatrix(vars,[],grouping,[],'oxs');
            success = 1;
        end
        
        function success = ManovaScatterPlot(obj, table_vars, grouping_param)
            % MANOVA analysis
            % e.g. vars = [T.time_total T.time_skin_to_end T.shots_total];
            % grouping_param = T.group;
            % ManovaScatterPlot({'time_total', 'shots_ap'}, {'group'})
            
            T = obj.SyntrackIo_ref.evaluation_data_table_joined_with_manual_data_;
            
            % accessing arrays from Table
            vars = T{:,table_vars};
            grouping = T{:,grouping_param};

            [d,p,stats] = manova1(vars,grouping);	% (m)anova analysis

            fprintf("MANOVA: d: %.4f, p: %.4f\n", d, p);
            
            c1 = stats.canon(:,1);
            c2 = stats.canon(:,2);
            figure();
            
            gscatter(c2,c1,grouping,[],'oxs');	% grouped scatter plot
            success = 1;
        end
        
        function success = ANOVA(obj)
            T = obj.SyntrackIo_ref.evaluation_data_table_joined_with_manual_data_;
            T = T(T.gertzbein_distance > 0 & T.facet_joint_affected == "fully",:);
            var_names = {'ep_dist_ap', 'ep_dist_ax', 'shots_pedicle'};
            [p,tbl,~] = anovan(...
                T.gertzbein_distance,{...
                T.ep_2d_distance_ap_mm...
                T.ep_2d_distance_axial_mm...
                T.shots_pedicle_ap...
                },'model','interaction','varnames',var_names,'continuous',[1:3]);
            T_results = table();
            T_results.p_var_names = categorical(tbl(2:end-2,1));
            T_results.p = p;
            disp(T_results);
        end
        
        function success = CorrelationPlot(obj)
            
            T = obj.io_evaluation_data_table_;
            
            obj.Message("Testing for correlation of inaccurately placed screws with g-arm settings...");
            
            T_gb_good = T(T.gertzbein_distance < 0,:);
            T_gb_bad = T(T.gertzbein_distance > 0,:);
            titles = {'correlations for gb < 0 (A)', 'correlations for gb > 0 (B - E)'};
            
            tables = {T_gb_good;T_gb_bad};
            for eval_idx =  1:2
                T = tables{eval_idx};
                gb = T.gertzbein_distance;
                gb1 = T.gertzbein_distance(T.try_id == 1);
                gb2 = T.gertzbein_distance(T.try_id == 2);
                angle_rot = T.garm_rot_angle_diff_ideal;
                angle_rot1 = T.garm_rot_angle_diff_ideal(T.try_id == 1);
                angle_rot2 = T.garm_rot_angle_diff_ideal(T.try_id == 2);
                angle_tilt = T.garm_tilt_angle_diff_ideal;
                angle_tilt1 = T.garm_tilt_angle_diff_ideal(T.try_id == 1);
                angle_tilt2 = T.garm_tilt_angle_diff_ideal(T.try_id == 2);
                fh = figure;
                corrplot([gb,angle_rot,angle_tilt],'varNames', {'gb','angle-rot','angle-tilt'},'type','Spearman','testR','on');
                title(titles(eval_idx));
            end
        end
    end
    
    methods (Access = private)
                
        function table_w_groups = AssignGroups(obj, table)
            definitions;
            T = sortrows(table,'try_id');
            T.group = groups_sort_by_try_id;
            T = T(:,[1,2,end,3:end-1]);
            table_w_groups = T;
        end
    end
end