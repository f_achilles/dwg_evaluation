% This Class is used for cleaning up and formatting the raw data
% A) DataCleaner.CorrectRawLabels() loads raw_data_ and corrects its label-array:
% -> raw_data_w_corr_labels_(22) will contain a copy of raw_data but with corrected labels. It is stored in the io
% object.
% B) DataCleaner.CleanData() loads raw_data_w_corr_labels_ from the io object and:
% -> Trims data (e.g. data w/ 'tech probs' is thrown away) and applies other manual corrections.
% -> Deletion or re-computation of the "cumulative x-ray events" timeseries.
% -> Splits data into two attempts (to clearly separate the experiments).
% -> Returns clean_data_(22,2) struct array, adds struct fields.
% 'proband_id' and 'attempt_id' for each attempt (n,1) and (n,2)
%
%
% - MM: �bernahme Teile 'x-ray' betreffend aus 'format.m': X-ray-eval usw. -> XrayEval.m ?
%

classdef DataCleaner < handle & Logger
    
    properties
        SyntrackIo_ref_;
        proband_ids_;  % Range of proband ids (array)
        raw_data_pointer_;
        raw_data_w_corr_labels_ = [];
        clean_data_tables_temp_;
        cleaning_specs_ = table();    % table format (p_id | label_no | label_name | try_no | value)
        flag_specs_loaded_ = false;
        flag_corr_raw_labels_executed_ = false;
        flag_trim_raw_data_executed_ = false;
        flag_data_cleaned_ = false;
    end
    
    properties (Constant)
        kDeleteTechProbs = true;    % set if tech_prob associated data should be deleted
        kClassName = 'DataCleaner';
        kVerbose = true; % Show debug/info output
        kCleanDataEventsTemplate = struct(...
            'framepos', [], 'labelcode', [], 'labelname', [],'time_abs_serial', [],'time_abs_dt', [],'time_rel', []);
        kCleanDataTemplate = struct(...
            'proband_id',[],'attempt_id',[],'ts', [], 'frame_nr', [], 'label', [], 'xray_event', [], ...
            'cumulative_xray_events', [], 'orbital_angle', [], 'tilt_angle', [], ...
            'axial_translation', [], 'cpad_switch_code', [], 'instrument_pose', [],...
            'patient_pose', []);
    end

    properties (Access = private, Hidden = true)
        kCleaningSpecificationsFilename = 'cleaning_specifications.xlsx';
    end
    
    methods
        function obj = DataCleaner(io_ref)
            if nargin == 0
                error("DataCleaner needs a reference to 'SyntrackIo' as argument!");
            end
            obj.SyntrackIo_ref_ = io_ref;
            raw_data_ts_test = io_ref.raw_data_.ts;
            if ~isempty(raw_data_ts_test)
                obj.proband_ids_ = io_ref.proband_ids_;
                obj.raw_data_pointer_ = io_ref.raw_data_;
                obj.Message("...was initialized with raw data from SyntrackIo.");
            else
                obj.Message("...was initialized with NO raw data.");
                obj.Message("Cleaner requires raw data. Loading raw data...");
                obj.SyntrackIo_ref_.LoadRawData();
            end
            
            % TODO MM: Discuss with FA about automated calls..
            obj.LoadCleaningSpecs();
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;
            
            success = success & obj.CleanRawData();

            if success
                disp('Tests for DataCleaner class are all working');
            else
                warning('Not all Tests for DataCleaner class are working. Please debug and fix.');
            end
            
            % TODO (FA): @MM could you put a test for DataStruct2Table here? Thanks.
        end
               
        function success = LoadCleaningSpecs(obj)
            % Load specifications for data label cleaning / correction from .xlsx file.
            cleaning_specs = readtable(obj.kCleaningSpecificationsFilename,'ReadVariableNames',true);
                    
            if ~isempty(cleaning_specs)
                obj.Message("Cleaning specifications loaded.");
                obj.cleaning_specs_ = cleaning_specs;
                success = true;
            else
                success = false;
                obj.flag_corr_raw_labels_executed_ = false;
            end
        end
        
        function success = CleanData(obj)
            % 0. ! Check flag 'CorrectRawLabels' -> was already executed? If yes, clean anyway but give a warning.
            % 1. Compute raw_data_w_corr_labels.
            % 2. Trims data (between 'tech probs') of option set.
            % 3. Apply manual values if neccessary (?)
            % 4. Re-Calc cumulative x-ray events or delete column
            % 5. Split data into two attempts (using template)
            % 6. Store clean_data_(22,2) in io object.
      
            % Copy raw data to new struct.
            obj.raw_data_w_corr_labels_ = obj.raw_data_pointer_;
            % Compute corrected labels
            obj.CorrectRawLabels();
            % Trim data (if option is set delete tech_probs, too!
            obj.TrimRawData();
            % Adding datetime_relative field / variable to clean data table
            obj.AddDatetimeToDataTable(obj.clean_data_tables_temp_);
            % Converting from table to struct again:
            clean_data_struct_temp = cell(max(obj.proband_ids_), 2);
            for p_id = obj.proband_ids_
                for try_id = 1:2
                    clean_data_struct_temp{p_id, try_id} = obj.DataTable2Struct(obj.clean_data_tables_temp_{p_id, try_id});
                end
            end
            
            if(~isempty(obj.clean_data_tables_temp_))
                obj.flag_data_cleaned_ = true;
                obj.SyntrackIo_ref_.clean_data_ = clean_data_struct_temp;
                obj.SyntrackIo_ref_.clean_data_table_ = obj.clean_data_tables_temp_;
                success = true;
                obj.Message("Cleaned data successfully and saved into SyntrackIo.clean_data_");
            else
                success = false;
                obj.Message('! WARNING ! Cleaning data partially failed.');
            end
        end
  
        function success = CorrectRawLabels(obj)
            % 1. Load labels into 'event' struct
            % 2. Using cleaning specifications
            % 2a. Apply all del and corr specifications to all p_ids in a loop
            % 2b. Apply all ins specifications to all p_ids in a loop
            % 4. Save labels to raw_data_w_corr_labels_ (DataCleaner only!)

            success = false;
            
            definitions;
            
            for p_id = obj.proband_ids_
                cleaning_tasks = obj.cleaning_specs_(obj.cleaning_specs_.p_id == p_id, :);
                clear event;
                event.framepos = find(obj.raw_data_pointer_(p_id).label~=0)';
                event.labelcode = obj.raw_data_pointer_(p_id).label(event.framepos)';
                event.time_abs_serial = obj.raw_data_pointer_(p_id).ts(event.framepos)';
                event.time_abs_dt = datetime(event.time_abs_serial,'ConvertFrom','datenum');

                % Applying label deletions and corrections
                for i_task = 1:height(cleaning_tasks)
                    switch (cleaning_tasks.action{i_task})
                        case 'del'
                            label_to_be_deleted = cleaning_tasks.label_no(i_task);
                            obj.raw_data_w_corr_labels_(p_id).label(...
                                event.framepos(label_to_be_deleted)...
                            ) = 0;
                        case 'corr'
                            label_to_be_corrected = cleaning_tasks.label_no(i_task);
                            value = cleaning_tasks.value{i_task};
                            if ((isnan(label_to_be_corrected)) || (isempty(value)))
                                obj.Message("Beware! Empty specifications found for p" + p_id + "!");
                                continue;
                            end
                            current_frame = event.framepos(label_to_be_corrected);
                            labelcode = event.labelcode(label_to_be_corrected);
                            obj.raw_data_w_corr_labels_(p_id).label(current_frame) = 0;
                            
                            datetime_of_current_label = event.time_abs_dt(label_to_be_corrected);
                            datetime_of_new_label = datetime_of_current_label + value;
                            timestamp_of_new_label = datenum(datetime_of_new_label);
                            %correction_in_days = cleaning_tasks.value(i_task) / (60 * 60 * 24);
                            %timestamp_of_new_label = timestamp_of_current_label + correction_in_days;
                            [~, new_frame] = min(abs(obj.raw_data_pointer_(p_id).ts - timestamp_of_new_label));
                            obj.raw_data_w_corr_labels_(p_id).label(new_frame) = labelcode;
                        case 'ins'
                        otherwise
                            error('DataCleaner::CorrectRawLabels: Unrecognized cleaning action!');
                    end
                end
                % Look inside of the new labels, to find the correct relative starting point.
                clear event;
                event.framepos = find(obj.raw_data_w_corr_labels_(p_id).label~=0)';
                event.labelcode = obj.raw_data_w_corr_labels_(p_id).label(event.framepos)';
                clean_start_labels_idx_bool = event.labelcode == annotation_label.start;
                clean_start_labels_frames = event.framepos(clean_start_labels_idx_bool);
                % check for invalid start / end combinations
                if (sum(clean_start_labels_idx_bool) ~= 2)
                    error("More than or less than 2 attempts in data of proband %i!", p_id);
                end

                % Applying label insertions after deletion and corrections
                for i_task = 1:height(cleaning_tasks)
                    switch (cleaning_tasks.action{i_task})
                        case 'del'
                        case 'corr'
                        case 'ins'
                            value = cleaning_tasks.value{i_task};
                            if isempty(value)
                                error("Beware! Empty insert specifications found for p" + p_id + "!");
                            end
                            try_number = cleaning_tasks.try_no(i_task);
                            reference_start_frame = clean_start_labels_frames(try_number);
                            datetime_of_start = datetime(obj.raw_data_w_corr_labels_(p_id).ts(reference_start_frame), 'ConvertFrom', 'datenum');
                            datetime_of_insertion = datetime_of_start + value;
                            timestamp_of_insertion = datenum(datetime_of_insertion);
                            [~, new_frame] = min(abs(obj.raw_data_w_corr_labels_(p_id).ts - timestamp_of_insertion));
                            labelcode_to_be_inserted = find(strcmp(label_names, cleaning_tasks.label_name{i_task}));                                                     
                            obj.raw_data_w_corr_labels_(p_id).label(new_frame) = labelcode_to_be_inserted;
                        otherwise
                            error('DataCleaner::CorrectRawLabels: Unrecognized cleaning action!');
                    end
                end
            end
            
            success = true;
            if success
                obj.Message("Raw data labels corrected and stored in 'raw_data_w_corr_labels_'.");
                obj.flag_corr_raw_labels_executed_ = true;
            end
        end
        
        function success = TrimRawData(obj)
            % Trimming 'raw_data_w_corr_labels_' to relevant parts:
            % 1. trimming data parts before start events, after end events  
            % 2. restructure into 2 attempts
            % 3. cleaning tech probs by removing data in between
            
            % Verify using raw_data_w_corr_labels
            if (~obj.flag_corr_raw_labels_executed_) 
                error('Could not execute trimming! Correct labels first!');
            end
            
            T_clean_data_trimmed = cell(1);
            
            for p_id = obj.proband_ids_
                [T_raw_data,~] = obj.DataStruct2Table(obj.raw_data_w_corr_labels_(p_id));
                
                % 1. trimming data parts before start events, after end events
                %    Remember: Col 'frame_nr' starts with '0' not with '1'
                %    Table row index = using find
                start_events =  find(T_raw_data.label == 1);
                end_events =  find(T_raw_data.label == 6);
                % deleting column for cumulative x-ray events:
                T_raw_data.cumulative_xray_events = [];

                % 2. Restructure data into single attempts:
                for try_idx = 1:2
                    % start_events/end_events contain indices (not frame_numbers)
                    T_try = T_raw_data([start_events(try_idx):end_events(try_idx)],:);
                    T_try.p_id = ones(height(T_try),1) * p_id;
                    T_try.try_id = ones(height(T_try),1) * try_idx;
                    % re-arrange cols to get p_id and try_id up front
                    T_try = T_try(:,[end-1 end 1:end-2]);
                
                    % 3. Cleaning tech probs by removing data in between affected attempts
                    %   including correcting timestamps
                    %   OR
                    %   set a flag for affected data only
                    tech_prob_events = find(T_try.label == 7);
                    if(tech_prob_events ~= 0)
                        if(obj.kDeleteTechProbs == true)
                            % Take data only from rows containing _no_ tech_probs
                            for event_idx = length(tech_prob_events)/2
                                % getting indices of table rows
                                tech_probs_idx1 = tech_prob_events(event_idx);
                                tech_probs_idx2 = tech_prob_events(event_idx+1);
                                T_try = T_try([1:tech_probs_idx1-1,tech_probs_idx2+1:end],:);
                                % correcting frame numbers
                                frame_nr_correction = T_try.frame_nr(tech_probs_idx1) - T_try.frame_nr(tech_probs_idx1-1);
                                T_try.frame_nr(tech_probs_idx1:end) = T_try.frame_nr(tech_probs_idx1:end) - frame_nr_correction + 1;
                                % correcting timestamps (add 33.3 ms = average time per frame)
                                time_correction = T_try.ts(tech_probs_idx1) - T_try.ts(tech_probs_idx1-1);
                                T_try.ts(tech_probs_idx1:end) = datenum(datetime(T_try.ts(tech_probs_idx1:end), 'ConvertFrom', 'datenum') - time_correction + milliseconds(33.3));
                            end
                        else
                            % flag data containing tech_probs with '1' (new
                            % column)
                            for event_idx = length(tech_prob_events)/2
                                T_try.tech_prob = zeros(height(T_try),1);
                                T_try.tech_prob(tech_prob_events(event_idx):tech_prob_events(event_idx+1)) = 1;
                            end
                        end
                    end
                    
                    T_clean_data_trimmed{p_id, try_idx} = T_try;
                    % continue loop for 2nd try
                end
                clear T_try;
                clear T_raw_data;
                % continue loop for new proband_id
            end

            % check if flag is set
            if (~isempty(T_clean_data_trimmed))
                obj.clean_data_tables_temp_ = T_clean_data_trimmed;
                obj.Message("Trimming successful! Saved data into 'clean_data_tables_temp_'.");
                obj.flag_trim_raw_data_executed_ = true;
                success = 1;
            else
                error("! WARNING ! Data error. No trimming happened.");
                success = 'error';
            end
        end
    
        % Helpers
        function data_table = AddDatetimeToDataTable(obj, data_table)
            for p_id = obj.proband_ids_
                for try_id = 1:2
                    data_table{p_id, try_id}.datetime_relative = datetime(data_table{p_id, try_id}.ts - data_table{p_id, try_id}.ts(1), 'ConvertFrom', 'datenum');
                    % format without date - minutes only
                    data_table{p_id, try_id}.datetime_relative.Format = 'mm:ss.SSS';
                    % re-arrange table columns to get datenum right beside p_id | try_id | ts
                    data_table{p_id, try_id} = data_table{p_id, try_id}(:,[1 2 3 end 4:end-1]);
                end
            end
            obj.clean_data_tables_temp_ = data_table;
        end
        
        function [data_table,success] = DataStruct2Table(obj, data_struct)
            % Generates table from struct
            fn = fieldnames(data_struct);
            size_of_fn = size(fn,1);
            if(~isempty(data_struct.ts) && size(data_struct,2) == 1)
                for i = 1:size_of_fn
                    raw_data_transp.(fn{i}) = data_struct.(fn{i})';
                end
                T = struct2table(raw_data_transp);
                data_table = T;
                success = 1;
            else
                success = 'error';
                data_table = [];
            end
        end
        
        function [data_struct,success] = DataTable2Struct(obj, data_table)
            % Generates struct from table
            if(~isempty(data_table))
                data_struct = table2struct(data_table,'ToScalar',true);
                data_struct = data_struct';
                
                %transpose every field name.. for better memory usage?
                fn = fieldnames(data_struct);
                size_of_fn = size(fn,1);
                for i = 1:size_of_fn
                    data_transp.(fn{i}) = data_struct.(fn{i})';
                end
                data_struct = data_transp;
                success = 1;
            else
                success = 'error';
                data_struct = [];
            end
        end
        
    end
end

