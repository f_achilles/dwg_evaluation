%% Plot without error bars
T = readtable('cleaned_proband_data_shots.txt','Delimiter','\t');

% box plot
x = cellstr(cat(2,char(T.group),char(' (n=4)',' (n=6)')));
T.group = categorical(x);
x = T.group;
y = [T.mean_shots_total T.mean_shots1 T.mean_shots2];
bar(x, y);
legend('Both attempts', '1st attempt', '2nd attempt');

%% Plot stacked
T = readtable('cleaned_proband_data_shots.txt','Delimiter','\t');
% bot plot stacked
x = cellstr(cat(2,char(T.group),char(' (n=4)',' (n=6)')));
T.group = categorical(x);
T.mean_xrays_lat = abs(T.mean_xrays_lat1 + T.mean_xrays_lat2);
T.mean_xrays_ap = abs(T.mean_xrays_ap1 + T.mean_xrays_ap2);
x = T.group;
y = [T.mean_xrays_lat T.mean_xrays_ap];
figure();
bar(x, y, 'stacked');
legend('AP images', 'Lateral images');

% plot grouped by attempts (?)
x2 = categorical({'Both attempts','1st attempt','2nd attempt'});
% create 3x 12 array (exp lat, exp ap, nov lat, nov ap | exp lat1 exp ap1
y2 = [T.mean_xrays_lat T.mean_xrays_lat1 T.mean_xrays_lat2; T.mean_xrays_ap T.mean_xrays_ap1 T.mean_xrays_ap2]';
figure();
bar(x2, y2, 'stacked');

%% Plotting with Barweb simple example

%barweb(barvalues, errors, width, groupnames, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend)
T = readtable('cleaned_proband_data_shots.txt','Delimiter','\t');

x = cellstr(cat(2,char(T.group),char(' (n=4)',' (n=6)')));  % cell array neccessary
y = [T.mean_shots_total T.mean_shots1 T.mean_shots2];
err = [T.std_shots_total T.std_shots1 T.std_shots2];
y_axis = 'shots';
title = 'shots per group (means)';
barweb(y,err,1,x,title,'',y_axis,winter);
legend('Both attempts', '1st attempt', '2nd attempt');

%% Plotting with BARWEB plugin

% Updated BARWEB code to be compatible with new graphs since R2014

%barweb(barvalues, errors, width, groupnames, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend)
addpath('./thirdparty_code');

% Creating struct and fill with data for later plotting
plotting = struct('datafile',[],'y_values',[],'err_values',[],'title',[],'y_axis',[],'legend',[]);

% setting static data
plotting.datafile = {'cleaned_proband_data_retrys.txt','cleaned_proband_data_shots.txt','cleaned_proband_data_times.txt'};
plotting.title = {'Retrys neccessary for pedicle screw placement (means per group)','X-ray shots used within procedure (means per group)','Durations of pedicle screw placement (means per group)'};
plotting.y_axis = {'Number of retrys','X-ray shots','Time in seconds)'};
plotting.legend = {'Both screws','1st screw','2nd screw'};

% setting dynamic data
    for t=1:3
        T = readtable(char(plotting.datafile(t)),'Delimiter','\t');
        switch t
            case 1
                plotting.y_values = [T.mean_retrys_total T.mean_retrys1 T.mean_retrys2];
                plotting.err_values = [T.std_retrys_total T.std_retrys1 T.std_retrys2];
            case 2
                plotting.y_values = [T.mean_shots_total T.mean_shots1 T.mean_shots2];
                plotting.err_values = [T.std_shots_total T.std_shots1 T.std_shots2];
            case 3
                plotting.y_values = [T.mean_time_total T.mean_time1 T.mean_time2];
                plotting.err_values = [T.std_time_total T.std_time1 T.std_time2];
        end
        
        x = cellstr(cat(2,char(T.group),char(' (n=4)',' (n=6)')));
        y = plotting.y_values;
        err = plotting.err_values;
        title = char(plotting.title(t));
        y_axis = char(plotting.y_axis(t));
        figure();
        barweb(y,err,1,x,title,'',y_axis,winter);
        legend(plotting.legend);
    end