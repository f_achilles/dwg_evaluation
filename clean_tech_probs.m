%% Try to clean tech probs associated data by deleting every data column between range of 1st and 2nd tech_prob label
clear lbl_tech_probs
clear fn
clear dur_probs
% make sure every recorded attempt has a 2*n number of tech probs labels
% (this should be done in 'format_data.m')

% delete all associated data in between tech probs label range
%all_recordings(i) = all_recordings(i).(fld{4})(1:10)

% delete 'tech probs' labels
lbl_tech_probs = find(strcmp(cleaned_proband_data.events.labelname, 'Tech probs'));
for fn = fieldnames(cleaned_proband_data.events)'
    cleaned_proband_data.events.(fn{1})(lbl_tech_probs(1)) = [];
    cleaned_proband_data.events.(fn{1})(lbl_tech_probs(2)) = [];   
end

% calc duration of data that has been deleted:
dur_probs = duration([0 0 0], 'Format','s');    %reset
tech_probs = cleaned_proband_data.events.time_abs_dt(strcmp(cleaned_proband_data.events.labelname, 'Tech probs'));
dur_probs = duration(tech_probs(2) - tech_probs(1),'Format','s');

% determine if data is being deleted in 1st or 2nd attempt (a = number of attempt)
tech_probs_ser = find(cleaned_proband_data.events.labelcode==7);
tech_probs_ser = cleaned_proband_data.events.time_abs_serial(tech_probs_ser(1)); %1st tech prob label serial time
if(cleaned_proband_data.attempts(2).start > tech_probs_ser)
    a = 1;
else
    a = 2;
end
sprintf('Duration of deleted data (p%i, attempt %i): %s', proband_id, a, dur_probs);