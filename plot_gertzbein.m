%% Load and plot bone mesh of case 'mecanix'

% 1. Alle objekte laden (mit richtiger skalierung und orientierung s. MeshLab)
% 2. Position durch Lage des Beads korrigieren
% 3. Rotation der Meshes
% 4. Endpunkte ermitteln
% 5. Schraubenmodell am Endpunkt plotten + rotieren

% Defining meshes & Bead offset
lvl = lumbar_vert_lvl_of_proband(proband_id);
pedicle_meshes = {...
    sprintf('./meshes/Mecanix_L%i_left.obj', lvl),...
    sprintf('./meshes/Mecanix_L%i_right.obj', lvl)};
screw_mesh_path = './meshes/screw.obj';
bead_offset_in_rai_coordinates = [0.144, 0.1277, 0.234];

% Load bonemesh into figure.
gertzbein_figure_handle = figure('Color','black');
axiscolor = [0.75 0.75 0.75];
set(gca,'Color','k','XColor',axiscolor,'YColor',axiscolor,'ZColor',axiscolor);

% Load pedicle meshes.
for p = 1:numel(pedicle_meshes)
pedicle_mesh = loadawobj(pedicle_meshes{p});
pm_handle_patch(p) = patch(...
    'vertices', bsxfun(@minus,...
    pedicle_mesh.v', bead_offset_in_rai_coordinates),...
    'faces', pedicle_mesh.f3',...
    'edgecolor', 'none',...
    'facecolor', [1 0 0],...
    'facealpha', 1);

rotate(pm_handle_patch(p),[1 0 0], 90, [0 0 0])
rotate(pm_handle_patch(p),[0 0 1], -90, [0 0 0])
end

% figure config
camlight
xlabel('X: sup->inf')
ylabel('Y: links->rechts')
zlabel('Z: ant->post')
axial_slice_width = 0.06;
switch lvl
    case 2
        % knapp
%         axis([-0.05 -0.025 -0.04 0.04 0.02 0.12]);
        % mit discs
%         axis([-0.065 -0.025 -0.04 0.04 0.02 0.12]);
        % full
%         axis([-0.075+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
    case 3
        % knapp
%         axis([-0.0135 0.015 -0.04 0.04 0.02 0.12]);
        % mit discs
%         axis([-0.0165 0.015 -0.04 0.04 0.02 0.12]);
        % full
%         axis([-0.0315+[0 axial_slice_width] -0.06 0.06 0.02 0.12]);
    case 4
        % knapp:
%         axis([0.029 0.055 -0.04 0.04 0.01 0.11]);
        % mit discs:
%         axis([0.015 0.060 -0.04 0.04 0.01 0.11]);
        % full
%         axis([0.005+[0 axial_slice_width] -0.06 0.06 0.01 0.11]);
end
bone_mesh_path = sprintf('./meshes/mecanix_nice_L%i.obj', lvl);
bone_mesh = loadawobj(bone_mesh_path);
bonemesh_handle_patch = patch(...
    'vertices', bsxfun(@minus, bone_mesh.v', bead_offset_in_rai_coordinates),...
    'faces', bone_mesh.f3',...
    'edgecolor', 'none',...
    'facecolor', [1 1 1],...
    'facealpha', 0.5);
rotate(bonemesh_handle_patch,[1 0 0], 90, [0 0 0])
rotate(bonemesh_handle_patch,[0 0 1], -90, [0 0 0])
axis image vis3d
view(-90.4000, 5.6);
% view all 3 pedicles:
% axis([-0.07 0.06 -0.04 0.04 -0.01 0.12])

% Plotting screw
hold on
poses = all_recordings(proband_id).instrument_pose;
% getting positions and rotations
positions = [poses.pos];
positions = reshape(positions,3,[]);
rotations = {poses.rot};
% getting end indices 
end_indices = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.end);
% getting position and rotation of try 1 & 2
pediculation_positions = positions(:,end_indices);
pediculation_rotations = rotations(end_indices);
% plot two markers for end positions
plot3(pediculation_positions(1,:),...
    pediculation_positions(2,:),...
    pediculation_positions(3,:),...
    '.','MarkerIndices',[1 2],'MarkerSize',15,'Color','yellow');
% plotting of screws at end positions
screw_mesh_orig = loadawobj(screw_mesh_path);
color_codes = {[0.1 0.4 1],[0 1 0]}; % 1st try = blue, 2nd = green
screw_meshes = cell(1,2);
% screwmesh_handle_patch = zeros(1,2);
for try_idx = 1:2
    screw_meshes{try_idx} = screw_mesh_orig;
    % rotation of screw
    screw_meshes{try_idx}.v = pediculation_rotations{try_idx}' * screw_meshes{try_idx}.v;
    % positioning of screw
    screw_meshes{try_idx}.v = bsxfun(@plus,...
        screw_meshes{try_idx}.v, pediculation_positions(:,try_idx));
    
    screwmesh_handle_patch(try_idx) = patch(...
        'vertices', screw_meshes{try_idx}.v',...
        'faces', screw_meshes{try_idx}.f3',...
        'edgecolor', 'none',...
        'facecolor', color_codes{try_idx},...
        'facealpha', 0.5);
end

% Switch on the nicely interpolated light at the end in order to affect all
% meshes in the figure.
lighting gouraud

title({sprintf('Proband ID: %i, vertebra: L%i',...
    proband_id, lumbar_vert_lvl_of_proband(proband_id)),...
    '(1st try = blue, 2nd try = green)'}, 'color', 'white');

hold off