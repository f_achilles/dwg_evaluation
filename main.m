%% apps %%

%% TODOs FA:
%must have
% 1 Compute average pediculation angle for novices and experts per level. (FA)
%
%nice to have
% 2 Write some utility functions, to quicker convert from index to timestamp and from days to seconds etc. (FA)
% 3 Convert the scripts that are working well into functions. (FA)
% 4 Convert the tool-tip paths into the moving coordinate frame of the
% anatomy. (FA)
%

%% TODOs MM:
% - Gegen�berstellung: Bilder je Phase (grob: [0] X-ray eingestellt [1] Einstich in Haut bis H�mmern [3] H�mmern bis Ende)
%   -> Faktor Erfahrene z.B. 4x h�here Rate an Bildern pro Phase
% - Check:  'cleaned_proband_data.attempts.time_correction' applied to 'proband_table' ?
%   -> IMPORTANT! Clean data including all data-colums for tech-probs
%   (switch for visualization yes or no)
% - Calculate average Dose Area Product (DAP in Gy x cm�) in AP and Lat per pedicle or per level = two pedicles
% (nice to have: - Adjust number of xray shots to c-pad switch-command)
%

%% General HINTs: In order to jump forward in a certain duration in seconds, use:
%   [~,index_of_frame_in_1_second] = min(abs(all_recordings.ts - (ts_now + 1/(24*3600))));
%   , where 
%   "ts_now" = your current timestamp in datenum format
%   "index_of_frame_in_1_second" = the index you are looking for

%% DONE:
% - Computation of path lengths (App 9)
% - Definitions: Added filenames and vert. levels for P18-P22
% - Definitions: Added and correctd model numbers S1-S8 for all probands
% - Main: Modified App 0 (Loading) for merging new proband data with existent
% - Definitions: Added filenames and vert. levels for P11-P15
% - Count number of lateral x-ray shots when rotation of the g-arm > 70 degree
% - Computed number of x-ray AP and Lat shots
% - Assigned x-ray shots and retrys to corresponding attempts
% - Output-Format after formatting data => Table 'proband_table'
% - plot bonemesh and paths in 3D (FA)
% - mapping function of subject index and file (MA)
% - Parse start time and end time from all_recordings (MM).
% --> HINT: Use "index = find(all_recordings == label_numbers.start, 1, 'first');" in
% order to find the index of the first usage of the 'start' button.
% -- Parse entry point and other labels form the struct (MM).
% -- Count XRays between labels (MM).
% - Compute durations bewteen certain labels (MA).
% --> HINT: Remember to use
%   datestr(all_recordings.ts(1000),'yyyy-mm-dd HH:MM:SS.FFF') for
%   conversion of datenum to human readable timestamps.
% - ZIP all recording files and send to FA via mail. (MM)
% - Compute time elapsed, xray shots, cpad switches -> DATA_xray.txt (MM)
% - For each level, find a good standard viewpoint to evaluate breaches. (FA)

%% App 0: Load, clean and format data into structs to output data in table format to evaluate
%FA: Works (March 22nd, 2019)
% Loading data from recorded FILES OR load from .mat that contain formatted all_recordings
% script does:
% 1) Select which files to read and to save as .mat
% 2) Optional: manual merging data from p1-p10 or p1-p15 with new data
% 3) apply format_data to save selection from (1) into struct 'cleaned_proband_data'
% 4) setting novice and experienced status and save selection into table
%    'proband_table' for further calculations and output
%
definitions % (includes pre-set option to output data as .txt or save images as .png)
clear proband_table;
clear all_recordings;

% Settings which data to read/load and to output as cleaned_proband_data_.txts
probands_id_start = 1;
probands_id_end = 22;

% generating filename to check
data_filename = ['data_p', num2str(probands_id_start), '-p', num2str(probands_id_end), '.mat'];
% check if .mat file exists else LOAD it from recorded .txts FILES (! takes up to 3 minutes per .txt!)
if(exist(data_filename,'file') == 0)
    % read data from recorded text-files
    for proband_id = probands_id_start:probands_id_end
        read_recording  % saves data into rec_struct;
        all_recordings(proband_id) = rec_struct;
    end
    % Save variable for later usage
    data_filename = ['data_p', num2str(probands_id_start), '-p', num2str(probands_id_end), '.mat'];
    save(data_filename, 'all_recordings');
else
    % load data from workspace-saved file
    load(data_filename, 'recording_struct');
end
all_recordings = recording_struct;

disp('App0: Part 1 successful, all_recordings was filled.')

%% App 0 (optional): Merging arrays manually to do calculations and save .mat file
%
probands_id_start = 1;
probands_id_end = 2;
%rec_temp = all_recordings(probands_id_start:probands_id_end);
load('data_p1-p17.mat');
rec_temp(1:17) = all_recordings(1:17);
clear all_recordings;
load('data_p18-p24.mat');
rec_temp(18:24) = all_recordings(18:24);
all_recordings = cat(2, rec_temp(1:17), rec_temp(18:24));
data_filename = ['data_p', num2str(probands_id_start), '-p', num2str(probands_id_end), '.mat'];
save(data_filename, 'all_recordings');
clear rec_temp;
disp('App0 (optional): Recorded data merged into -> ' + string(data_filename) + '.')

%% App 0 part 2: Cleaning up data
%FA: Where are the cleaned up data stored? (March 22nd, 2019)
% Clean recorded data 'all_recordings(proband_id)' and format data to table format
definitions;
write_data = false; % output cleaned data to .txt yes/no
clear proband_table;

for proband_id = probands_id_start:probands_id_end
    format_data         % format_data uses proband_id (-> no function..)
    %clean_tech_probs    % clean data colums for range within tech_probs
end

% Adding column 'group' to proband_table (exp OR nov)
format_proband_table_groups;

% display table data in matlab
disp(proband_table);

if(write_data == false)

    % Write tables to file (data separated by tab)
    filelist = {
        'cleaned_proband_data.txt';
        'cleaned_proband_data_times.txt';
        'cleaned_proband_data_shots.txt';
        'cleaned_proband_data_retrys.txt'};
    % Write complete data into table 'cleaned_proband_data.txt'
    writetable(proband_table,filelist{1},'WriteRowNames',true,'Delimiter','\t');

    % Calculating mean, min/max and stddeviation of retrys, shots, times by groups
    % setting different input vars for different calculations
    input(1).vars = {'time_total';'time1';'time2'};
    input(2).vars = {'shots_total';'shots1';'shots2';'xrays_lat1';'xrays_ap1';'xrays_lat2';'xrays_ap2'};
    input(3).vars = {'retrys_total';'retrys1';'retrys2'};
    for e = 1:3   % for all evaluations
        % Calculating means, std-deviation and min/max values by GROUPS (!)
        T_mean = varfun(@mean,proband_table(:,[2:end]),'InputVariables',input(e).vars,'GroupingVariables','group');
        T_stddev = varfun(@std,proband_table(:,[2:end]),'InputVariables',input(e).vars,'GroupingVariables','group');
        T_min = varfun(@min,proband_table(:,[2:end]),'InputVariables',input(e).vars,'GroupingVariables','group');
        T_max = varfun(@max,proband_table(:,[2:end]),'InputVariables',input(e).vars,'GroupingVariables','group');
        T = [T_mean(:,:) T_stddev(:,3:end) T_min(:,3:end) T_max(:,3:end)];
        % Write each table to file (variable contents separated by tab)
        writetable(T,char(filelist(e+1)),'WriteRowNames',true,'Delimiter','\t');
    end

    % Create files '_xls.txt' for german excel readable data -> replace "." with "," for numbers
    for fn=filelist'       % important to use transposed cell array here to loop through every index!
        file = char(fn);
        wholefile = fileread(file);
        newfiledata = strrep(wholefile,'.',',');
        file = [file(1:end-4), '_xls.txt'];
        fileid = fopen(file,'w');
        fprintf(fileid,'%s',newfiledata);
        fclose(fileid);
    end

    % free memory
    clear('file','wholefile','newfiledata','fileid');
    disp('App0: Part 2 successful! Data cleaned, formatted and saved in Excel compatible format.')
else
    disp('App0: Part 2 successful! Data formatted in Table "proband_table".')
end

%% App 1: Make an xray evaluation
%FA: Works (March 22nd, 2019)
definitions

% Settings which data to read/load and to output as cleaned_proband_data_.txts
probands_id_start = 1;
probands_id_end = 2;

% use for fast single evaluation (dirty..): loading from single file
% --------------------
%proband_id = 12;
%read_recording
%all_recordings(proband_id) = all_recordings;
% use for multiple evaluations (.mat files)
% -----------------------
%load 'data_p1-p10_all.mat' %contains all_recordings(1x10) only for probands p1 to p10
%
% plot xray evaluation with loaded data
for proband_id=probands_id_start:probands_id_end
    clear experiment
    clear xray_shots
    write_data_to_file = false;  % switch for output to file - delete old file 'DATA_xrax.txt' first!
    write_images_to_png = false; % switch for image saving to file
    xray_evaluation     % ! requires recordings_struct(proband_id)
end
%close all;
disp('App1: Success! Recorded data were evaluated and plotted.')

%% App 2: Plot the path of the trocar and the bone mesh.
%FA: Works (March 22nd, 2019)
proband_id = 17;
definitions
%read_recording

% Plot the bone.
path_fig = figure;
plot_bonemesh

% Parse the annotation events.
xray_evaluation

% Use parsed events in order to plot the most important part of the path.
figure(path_fig);
plot_instrument_path
disp('App2: Success! Tool trajectories were plotted for p' + string(proband_id) + '.')

%% App 3: Plot all vertebral bodies (L2 - L4) with screws by proband
%FA: Works and looks awesome (March 22nd, 2019)
definitions;
proband_id = 14;
clear proband_table;

format_data;
plot_gertzbein;
set(gcf, 'position', [200, 200, 700, 700])
disp('App3: Success! Predicted screws were plotted for p' + string(proband_id) + '.')

%% All 3.5 (optional): Make Gertzbein movie.
axis off
duration_in_seconds = 10;
h_vid = VideoWriter(fullfile(sprintf('screw_placement_movie_proband%i_L%i.avi', proband_id, lvl)));
h_vid.FrameRate = 30;
open(h_vid);
figure(gertzbein_figure_handle);
for azimuth = linspace(0, 360, duration_in_seconds * h_vid.FrameRate)
    view(azimuth, 0.0); %el: 5.6
    currFrame = getframe(gertzbein_figure_handle);
    writeVideo(h_vid, currFrame);
end
close(h_vid);

%% App 4: Plot box plots with standard error bars for retrys, shots, times
%FA: Does not work, the "group" variable is missing (March 22nd, 2019)

% Updated BARWEB code (plugin from Matlab Community -> FileExchange) to be compatible with new graphs since R2014
% barweb(barvalues, errors, width, groupnames, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend)
%
% The following files must exist (run App 0 to create them)
% cleaned_proband_data_retrys.txt
% cleaned_proband_data_shots.txt
% cleaned_proband_data_times.txt

% SETTINGS
% ---------------------
plot_group_by_groups = true;   % for barplot: group by GROUPS or group by ATTEMPTS
save_images = true;     % save images als png

% Creating struct and fill with data for later plotting by groups
plotting = struct('datafile',[],'y_values',[],'err_values',[],'title',[],'y_axis',[],'legend',[]);

% setting static data
plotting.datafile = {'cleaned_proband_data_retrys.txt','cleaned_proband_data_shots.txt','cleaned_proband_data_times.txt'};
plotting.title = {'Retrys needed for procedure (means per group)','X-ray images needed for procedure (means per group)','Time needed for procedure (means per group)'};
plotting.y_axis = {'Number of retrys','X-ray images (lateral and AP)','Time (min)'};
plotting.legend = {'Both sides','1st side','2nd side'};

groupcount_nov = length(find(proband_table.group=='novice'));
groupcount_exp = length(find(proband_table.group=='experienced'));

% setting dynamic data
for t=1:3
    T = readtable(char(plotting.datafile(t)),'Delimiter','\t');
    switch t
        case 1
            % table retrys
%             plotting.y_values = [T.mean_retrys_total T.mean_retrys1 T.mean_retrys2];
plotting.y_values = [T.median_retrys_total T.median_retrys1 T.median_retrys2];
            plotting.err_values = [T.std_retrys_total T.std_retrys1 T.std_retrys2];
            disp('Showing means of retry values:');
            disp(plotting.y_values);
        case 2
            % table shots
            plotting.y_values = [T.median_shots_total T.median_shots1 T.median_shots2];
            plotting.err_values = [T.std_shots_total T.std_shots1 T.std_shots2];
            disp('Showing means of X-ray images values:');
            disp(plotting.y_values);
        case 3
            % table times
            % reformatting seconds to minutes for better reading
            plotting.y_values = [T.median_time_total T.median_time1 T.median_time2];
            plotting.y_values = plotting.y_values/60;
            plotting.err_values = [T.std_time_total T.std_time1 T.std_time2];
            plotting.err_values = plotting.err_values/60;
            disp('Showing means of time values:');
            disp(plotting.y_values);
    end

    if(plot_group_by_groups == true)
    %Plotting by groups (experienced, novices)
    g = char(sprintf(' (n=%i)',groupcount_exp),sprintf(' (n=%i)',groupcount_nov)); %certain format
    x = cellstr(cat(2,char(T.group),g));
    y = plotting.y_values;
    err = plotting.err_values;
    title = char(plotting.title(t));
    y_axis = char(plotting.y_axis(t));
    %subplot(3,1,t);
    figure('Color','white');
    barweb(y,err,1,x,title,'',y_axis,winter);
    legend(plotting.legend);

    else
    %Plotting by attempts (sides)! (both, 1st, 2nd)
    x = cellstr(plotting.legend);
    leg = {sprintf('Group experienced (n=%i)',groupcount_exp),sprintf('Group novice (n=%i)',groupcount_nov)};
    y = plotting.y_values';     %transposed values
    err = plotting.err_values'; %transposed values
    title = char(plotting.title(t));
    y_axis = char(plotting.y_axis(t));
    %subplot(3,1,t);
    figure('Color','white');
    barweb(y,err,1,x,title,'',y_axis,winter);
    legend(leg);
    end

    % Saving images in PNG with export_fig
    if(save_images == true) 
        if(plot_group_by_groups == true)
            export_fig(sprintf('data_eval_groups_p%i_p%i_fig_%i.png', probands_id_start, probands_id_end, t));
        else
            export_fig(sprintf('data_eval_attempts_p%i_p%i_fig_%i.png', probands_id_start, probands_id_end, t));
        end
    end
end
disp('App4: Success! Performance statistics were computed and plotted.')

%% App 5: Load Recording of specific proband and show the screw end position and its Gertzbein score.
%FA: Works and looks awesome (March 22nd, 2019)

% This app is similar to App 3, it also shows the screws, but in App 5 you show the screw end positions AND output the
% directional Gertzbein distances in the terminal.

% Run only once to setup all data structures.
%%%
% Execute "App 0" here.
%%%
probands_id_start = 1;
probands_id_end = 2;
% Run repeatedly to update study subject.
gertzbein_distance_mm = [];
write_gertzbein_table = false;
clear gertzbein_class;
clear proband_table;

for proband_id = probands_id_start:probands_id_end
    format_data;        % --> constructs new cleaned_proband_data
    plot_gertzbein;     % --> should use events from cleaned 'cleaned_proband_data'! not from 'all_recordings'
    compute_gertzbein;  % --> should use events from cleaned 'cleaned_proband_data'!
    %savefig(sprintf('gertzbein_fig_p%i', proband_id));
end
T.gertzbein_try1 = gertzbein_distance_mm(probands_id_start:probands_id_end, 1);
T.gertzbein_try2 = gertzbein_distance_mm(probands_id_start:probands_id_end, 2);
T.gertzbein_class1 = gertzbein_class(probands_id_start:probands_id_end, 1);
T.gertzbein_class2 = gertzbein_class(probands_id_start:probands_id_end, 2);

if(write_gertzbein_table == false)
    % writing Table to .txt
    file = 'gertzbein_evaluation_table.txt';
    writetable(T,file,'WriteRowNames',true,'Delimiter','\t');
    wholefile = fileread(file);
    newfiledata = strrep(wholefile,'.',',');
    file = [file(1:end-4), '_xls.txt'];
    fileid = fopen(file,'w');
    fprintf(fileid,'%s',newfiledata);
    fclose(fileid);
end
        
%% App 6: Calculate x-ray image frequency, deviaton from 'ideal' x-ray view and gertzbein_distance [mm]
%FA: Does not work (March 22nd, 2019) Error: Undefined variable "T" or class "T.gertzbein_try1".

clear dev_orbital
clear dev_tilt
clear dev_trans
clear image_avg_bone
clear gertzbein_distance_mm
clear y1
clear x1
clear x2
clear x3
clear proband_table;
definitions

verbose = true; %true -> graphical plotting and x-ray mean evaluation

for proband_id = 1:2
    format_data;
    xray_evaluation_view;   %including image frequency moving mean
end

% DISPLAY summarized results in columns
disp('dev_orbital_bone_entered_V1   dev_orbital_bone_entered_V2');
disp([dev_orbital(:,1),dev_orbital(:,2)]);
disp('dev_tilt_bone_entered_V1   dev_tilt_bone_entered_V2');
disp([dev_tilt(:,1),dev_tilt(:,2)]);
disp('images_avg_bone_entered_V1   images_avg_bone_entered_V2');
disp([images_avg_bone(:,1),images_avg_bone(:,2)]);

% Scatter-Plot: Deviation from 'ideal' x-ray view against gertzbein classification

% all probands
y1 = [T.gertzbein_try1 T.gertzbein_try2];
y1 = y1(:);
x1 = dev_orbital(:);
x2 = dev_tilt(:);
%x3 = dev_trans(:);

figure;
gb_ymin = -4;
gb_ymax = 0;
subplot(4,2,1:2)
    scatter(x1,y1,'b','filled');
    hold on;
    scatter(x2,y1,'g','filled');
    hold off;
    ylim([gb_ymin,gb_ymax]);
    xlim([-20,20]);
    title('All: GB distance over orbital deviation (blue) and tilt deviation (green)');
subplot(4,2,3:4)
    scatter(abs(x1),y1,'b','filled');
    hold on;
    scatter(abs(x2),y1,'g','filled');
    hold off;
    ylim([gb_ymin,gb_ymax]);
    xlim([0,20]);
    title('All: GB distance over ABSOLUTE orbital deviation (blue) and tilt deviation (green)');
subplot(4,2,5:6)
    scatter(sqrt(x1.^2 + x2.^2),y1,'k','filled');
    ylim([gb_ymin,gb_ymax]);
    xlim([0,20]);
    title('All: GB distance over ABSOLUTE spherical deviation angle (black)');
subplot(4,2,7)
    hist(x1, 20)
    xlim([-20,20]);
    title('spread of orbital deviation');
subplot(4,2,8)
    hist(x2, 20)
    xlim([-20,20]);
    title('spread of tilt deviation');


% group novice (1:6,10,12:15)
y_novice = y1([1:6,10,12:15, (22+[1:6,10,12:15])]);
x1_novice = x1([1:6,10,12:15, (22+[1:6,10,12:15])]);
x2_novice = x2([1:6,10,12:15, (22+[1:6,10,12:15])]);
figure;
scatter(x1_novice,y_novice,'b','filled');
hold on;
scatter(x2_novice,y_novice,'g','filled');
hold off;
ylim([-4,10]);
xlim([-20,20]);
title('Novice: GB distance over orbital deviation (blue) and tilt deviation (green)');

% group expert (7:9,11,16:22)
y_exp = y1([7:9,11,16:22, (22+[7:9,11,16:22])]);
x1_exp = x1([7:9,11,16:22, (22+[7:9,11,16:22])]);
x2_exp = x2([7:9,11,16:22, (22+[7:9,11,16:22])]);
figure;
scatter(x1_exp,y_exp,'b','filled');
hold on;
scatter(x2_exp,y_exp,'g','filled');
hold off;
ylim([-4,10]);
xlim([-20,20]);
title('Experienced: GB distance over orbital deviation (blue) and tilt deviation (green)');

x4 = -lateral_distances(:);
figure
scatter(x4, y1, 'filled');
title('All: GB distance over medial entry point shift');
y_nov = y1([1:6,10,12:15, (22+[1:6,10,12:15])]);
x4_nov = x4([1:6,10,12:15, (22+[1:6,10,12:15])]);
figure
scatter(x4_nov,y_nov,'g','filled');
title('Novice: GB distance over medial entry point shift');
y_exp = y1([7:9,11,16:22, (22+[7:9,11,16:22])]);
x4_exp = x4([7:9,11,16:22, (22+[7:9,11,16:22])]);
figure
scatter(x4_exp,y_exp,'g','filled');
title('Experienced: GB distance over medial entry point shift');


%% App 7: Determine 'ideal' AP x-ray viewing angles for each vertebra level
%FA: Does not work (March 22nd, 2019) Error: Invalid file identifier. Use fopen to generate a valid file identifier.
% --> it seems to not find some input file.

definitions
clear rec_struct;

proband_id = 23; % fake proband_id for AP x-ray evaluation
read_recording
all_recordings(proband_id) = rec_struct;
% no endpoint label set, therefore:
all_recordings(proband_id).label(end) = label_numbers.end;
xray_evaluation
xray_evaluation_labels = find(all_recordings(proband_id).label == label_numbers.start);
rec.rotation = all_recordings(proband_id).orbital_angle(xray_evaluation_labels);
rec.tilt_angle = all_recordings(proband_id).tilt_angle(xray_evaluation_labels);
% AP x-ray views (tilt and rotation) were recorded in the order
% L4, L3, L2 -> that's why its 7 6 3 ;)
% using vertebra level as array index (hint: i=1 -> L1 does not exist)
xray_ideal_orbital = [0,rec.rotation([7,6,3])];
xray_ideal_tilt = [0,rec.tilt_angle([7,6,3])];
%xray_ideal_trans = [0,rec.tilt_angle([7,6,3])];
fprintf('Ideal rotation: L2 = %i�, L3 = %i�, L4 = %i�\n', xray_ideal_orbital(2), xray_ideal_orbital(3), xray_ideal_orbital(4));
fprintf('Ideal tilt: L2 = %i�, L3 = %i�, L4 = %i�\n', xray_ideal_tilt(2), xray_ideal_tilt(3), xray_ideal_tilt(4));
%fprintf('Ideal translation: L2 = %i�, L3 = %i�, L4 = %i�\n', xray_ideal_trans(2), xray_ideal_trans(3), xray_ideal_trans(4));


%% App 8: Measure the outward distance to the lateral side of the pedicle for when the trocar enters the bone (the EYE).
%FA: Works and looks awesome (March 22nd, 2019)

%%%
% Execute "App 0" here.
%%%
clear proband_table;

probands_id_start = 1;
probands_id_end = 22;
% Run repeatedly to update study subject.
lateral_distances = [];
for proband_id = probands_id_start:probands_id_end
    format_data; % --> constructs new cleaned_proband_data
    plot_gertzbein; % --> should use events from cleaned cleaned_proband_data! not all_recordings
    compute_eye_distance; % --> should use events from cleaned cleaned_proband_data!
end

%% App 9: Compute path lengths of instrument tip
%FA: Does not works (March 22nd, 2019), Error: Index exceeds the number of array elements (1).

% remember to eventually use App 0 part 2 first

definitions;
probands_id_start = 1;
probands_id_end = 22;
clear proband_table;
clear path_length;

length_setting = 'start_skin'; % [path: start -> skin_cut]

for proband_id = probands_id_start:probands_id_end
    format_data;
    compute_path_lenghts;
end

%%
% FA: App 10? Does not work (March 22nd, 2019), Error: "Error using format_proband_table_groups (line 6)
% To assign to or create a variable in a table, the number of rows must match the height of the table."

% Adding column 'group' (categorical) to proband_table (exp OR nov)
format_proband_table_groups;

% Adding computed path lengths
proband_table.path_total = (path_length(1,:) + path_length(2,:))';
proband_table.path1 = path_length(1,:)';
proband_table.path2 = path_length(2,:)';
% setting units
proband_table.Properties.VariableUnits{'path_total'} = 'm';
proband_table.Properties.VariableUnits{'path1'} = 'm';
proband_table.Properties.VariableUnits{'path2'} = 'm';

disp('Path lenghts of p' + string(probands_id_start) + ' - p' + string(probands_id_end) + ' successfully calculated and saved in "proband_table".');
disp(proband_table);