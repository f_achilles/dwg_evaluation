classdef UseCases < handle & Logger
    
    properties
        
    end
    
    properties (Constant)
        kClassName = 'UseCases';
        kVerbose = true;
    end

    methods
        function obj = UseCases()
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for UseCases class are all working');
            else
                warning('Not all Tests for UseCases class are working. Please debug and fix.');
            end
        end
        
        function success = Run(obj, use_case_id)
            success = true;
            switch use_case_id
                case 0
                    % In this use case, the framework should (from Google Spreadsheet):
                    % "0) Show cleaned data:
                    %   Read raw txt files,
                    %   Convert to mat structs,
                    %   Clean those structs,
                    %   Plot them.
                    %   BONUS: Plot uncleand and then cleaned data ;) For comparison."
                    
                    proband_ids = [5, 6, 7, 9];
                    data_handler = SyntrackIo();
                    success = data_handler.LoadRawData(proband_ids);
                    
                    cleaner = DataCleaner(data_handler);
                    success = success & cleaner.CleanData();
                    
                    plotter = SyntrackPlotter(data_handler, proband_ids);
                    
                    plotter.PlotXRay();
                    
                    plotter.PlotRawVsCleanData(cleaner);
                    
                    if success
                        disp('Use Case 0 is working!');
                    else
                        warning('Use Case 0 is not working. Please debug and fix.');
                    end
                case 1
                case 2
                    % From Google Spreadsheet:
                    % 2) Plot Gertzbein-mm versus ImageFreq@BoneEntry: IO_object:Load(Gertzbein::evaldata, XRayEval::evaldata)
                    %    --> Plotter::Corrplot(Gertzbein-mm, ImageFreq@BoneEntry)
                    proband_id = 5;
                    
                    % Load X-Ray evaluation.
                    
                    % Load Gertzbein evaluation.
                    
                    % Compute X-Ray evaluation, if none available for loading.
                    data_handler = SyntrackIo();
                    success = data_handler.LoadRawData();
                    xray_evaluation = XRayEvaluator();
                    xray_evaluation.EvaluateProband(data_handler.raw_data_, proband_id);
                    
                    % Hand over both evaluation results to the plotter.
                    
                    % Plotter: Create correlation plot.
                    
                otherwise
                    warning('No use case with this ID is implemented.');
            end  
        end
    end
end