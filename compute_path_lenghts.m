%% computes path lenghts of instrument tip (standard setting = total path)

if exist('length_setting') == 0
    length_setting = 'start_end';   % standard setting
end

% Settings determine which path length to calculate
% maybe problematic: more than 1 try.. -> very large paths (solution: path / no. of attempts?)

switch length_setting
    case 'start_end'    % [start -> end] = path_total
        % getting start and end indices 
        start_indices = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.start);
        end_indices = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.end);
    
    % --- [start -> skin cut] ------------------------
    case 'start_skin'
        % getting start indices 
        start_indices = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.start);
        % only take first 'skin cut' event after start_indices(n)
        framepos_cut = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.cut);
        % if skin cut not found, take bone entered instead
        if numel(framepos_cut) < 2
            % check where skin cut was not definded (there is always at
            % least one event..)
            disp("only one 'skin cut' event found.. ");
            if framepos_cut > start_indices(2)
                % replace 'skin cut' with 'bone entered' for 1st try:
                disp("..using 'bone entered' instead @ 1st try.");
                framepos_bone = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.enter_bone);
                framepos_cut_1st(1,:) = framepos_bone(framepos_bone > start_indices(1) & framepos_bone < start_indices(2));
            else
                disp("..using 'bone entered' instead @ 2nd try.");
                % replace 'skin cut' with 'bone entered' for 2nd try:
                framepos_bone = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.enter_bone);
                framepos_cut_1st(2,:) = framepos_bone(framepos_bone > start_indices(2));
            end
        else
            % standard-case for 2 or more 'skin cut' events:
            framepos_cut_1st(1) = framepos_cut(framepos_cut > start_indices(1) & framepos_cut < start_indices(2));
            framepos_cut_1st(2) = framepos_cut(framepos_cut > start_indices(2));
        end
        % definde both first 'skin cut' events as end_indices for calc
        end_indices(:) = framepos_cut_1st(:,1);
        
    % --- [skin cut -> bone entered] ------------------------
    case 'skin_bone'
        
    % --- [bone entered -> end] ------------------------
    case 'bone_end'
        
end

% Error handling
% getting position of try 1 & 2
%if (numel(start_indices) ~= 2 || numel(end_indices) ~= 2)
%	error('! wrong number of indices');
%end

% Do calculation after indices are set:

poses = all_recordings(proband_id).instrument_pose;
% getting positions and rotations
positions = [poses.pos];
positions = reshape(positions,3,[]);
        
path_first_try = positions(:,start_indices(1):end_indices(1));
path_second_try = positions(:,start_indices(2):end_indices(2));
diff_first_try = diff(path_first_try,1,2);
diff_second_try = diff(path_second_try,1,2);
steplength_first_try = zeros(1,size(diff_first_try,2));
steplength_second_try = zeros(1,size(diff_first_try,2));
for vec_idx = 1:size(diff_first_try,2)
    steplength_first_try(vec_idx) = norm(diff_first_try(:,vec_idx));
end

for vec_idx = 1:size(diff_second_try,2)
    steplength_second_try(vec_idx) = norm(diff_second_try(:,vec_idx));
end

% 1st try
path_length(1,proband_id) = sum(steplength_first_try);
% 2nd try
path_length(2,proband_id) = sum(steplength_second_try);

disp('path length (p' + string(proband_id) + ') = ' + string(path_length(1,proband_id)) + ' m | ' + string(path_length(2,proband_id)) + ' m');

clear start_indices;
clear end_indices;
clear framepos_cut;
clear framepos_bone;
clear framepos_cut_1st;

%% test

definitions;
proband_id = 1;
clear proband_table;
clear path_length;

    format_data;
        % getting end indices 
        start_indices = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.start);
        % only take first 'skin cut' event after start_indices(n)
        framepos_cut = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.cut);
        % if skin cut not found, take bone entered instead
        if numel(framepos_cut) < 2
            % check where skin cut was not definded (there is always at
            % least one event..)
            if framepos_cut > start_indices(2)
                % replace 'skin cut' with 'bone entered' for 1st try:
                framepos_bone = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.enter_bone);
                framepos_cut_1st(1,:) = framepos_bone(framepos_bone > start_indices(1) & framepos_bone < start_indices(2));
            else
                % replace 'skin cut' with 'bone entered' for 2nd try:
                framepos_bone = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelcode == label_numbers.enter_bone);
                framepos_cut_1st(2,:) = framepos_bone(framepos_bone > start_indices(2));
            end
        else
            % standard-case for 2 or more 'skin cut' events:
            framepos_cut_1st(1,:) = framepos_cut(framepos_cut > start_indices(1) & framepos_cut < start_indices(2));
            framepos_cut_1st(2,:) = framepos_cut(framepos_cut > start_indices(2));
        end
        
        % definde both first 'skin cut' events as end_indices for calc
        end_indices(:) = framepos_cut_1st(:,1);