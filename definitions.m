%% Path definitions
addpath('.\thirdparty_code')
addpath('.\thirdparty_code\export_fig')
addpath('.\meshes')

%% Label and Labelname definitions
annotation_label.start = 1;
annotation_label.xray_set = 2;
annotation_label.cut = 3;
annotation_label.enter_bone = 4;
annotation_label.retry = 5;
annotation_label.end = 6;
annotation_label.tech_prob = 7;
annotation_label.wrong_pos = 8;
annotation_label.ep_manual = 9; % Marked by PhasesEvaluator!

label_numbers = annotation_label;    % for better understanding label -> numbers

%% Numbers to Labelnames
annotation_name = {'Start','Fluoro adjusted','Skin perforation','Needle advancement','Retry','End','Tech probs','Wrong pos','Cortical perforation'};
label_names = {'start','garm_set','cut','enter_bone','retry','end','tech_prob','Wrong pos','ep_manual'};
number_labels = annotation_name;    % for better understanding numbers -> labelname

%% Filenames and Proband name associations
filename_proband{1,1} = '2017_6_26_13_11_24_recording.txt';
filename_proband{2,1} = '2017_6_26_13_53_9_recording.txt';
filename_proband{3,1} = '2017_6_26_14_21_46_recording.txt';
filename_proband{4,1} = '2017_6_26_15_34_16_recording.txt';
filename_proband{5,1} = '2017_6_26_17_8_42_recording.txt';
filename_proband{6,1} = '2017_6_26_18_57_50_recording.txt';
filename_proband{7,1} = '2017_6_28_18_18_2_recording.txt';
filename_proband{8,1} = '2017_6_28_17_5_50_recording.txt';
filename_proband{9,1} = '2017_6_28_17_19_57_recording.txt';
filename_proband{10,1} = '2017_6_28_17_34_11_recording.txt';
filename_proband{11,1} = '2017_9_22_10_24_31_recording.txt';
filename_proband{12,1} = '2017_9_22_13_18_47_recording.txt';
filename_proband{13,1} = '2017_9_22_14_3_40_recording.txt';
filename_proband{14,1} = '2017_9_22_16_56_23_recording.txt';
filename_proband{15,1} = '2017_9_22_17_36_2_recording.txt';
filename_proband{16,1} = '2017_9_26_16_5_57_recording.txt';
filename_proband{17,1} = '2017_9_26_16_43_19_recording.txt';
filename_proband{18,1} = '2017_12_5_17_11_39_recording.txt';
filename_proband{19,1} = '2017_12_5_19_26_7_recording.txt';
filename_proband{20,1} = '2017_12_7_17_14_58_recording.txt';
%filename_proband{21,1} = '2017_12_21_17_2_3_recording.txt'; % p21 (fail)
%filename_proband{21,1} = '2017_12_29_12_1_42_recording.txt';% p21_wdh fail
filename_proband{21,1} = '2017_12_29_12_21_4_recording_2nd.txt';% p21_wdh_2 (best)
filename_proband{22,1} = '2018_1_5_8_10_24_recording.txt';
% Ideal x-ray view recording by felix:
filename_proband{23,1} = '2018_2_28_xray_view_recording.txt';
% dummy recording:
filename_proband{24,1} = '2017_6_26_13_9_6_recording.txt';

%
% WRONG FILENAME ASSOCs (p7 - p10)!
% old       |       correct
% P7        ->      P8
% P8        ->      P9
% P9        ->      P10
% P10       ->      P7

%% Proband number and vertebra level = task; model number (Sx), true level (if different from task)
lumbar_vert_lvl_of_proband(1) = 3; %S1, 1st and 2nd try = L3 - 2x fail
lumbar_vert_lvl_of_proband(2) = 4; %S1, 1st and 2nd try = L3 - 2x fail
lumbar_vert_lvl_of_proband(3) = 3; %S1, 1st try = L2 - fail, 2nd try: L3
lumbar_vert_lvl_of_proband(4) = 2; %S2, 1st try = L1 ;-) fail, 2nd try: L2
lumbar_vert_lvl_of_proband(5) = 4; %S2, 1st try = L3 - fail; 2nd try: L4
lumbar_vert_lvl_of_proband(6) = 3; %S2
lumbar_vert_lvl_of_proband(7) = 2; %S4
lumbar_vert_lvl_of_proband(8) = 2; %S3
lumbar_vert_lvl_of_proband(9) = 3; %S3
lumbar_vert_lvl_of_proband(10) = 4; %S3
% study day 22.09.2017:
lumbar_vert_lvl_of_proband(11) = 4; %S4
lumbar_vert_lvl_of_proband(12) = 3; %S4
lumbar_vert_lvl_of_proband(13) = 2; %S5
lumbar_vert_lvl_of_proband(14) = 3; %S5
lumbar_vert_lvl_of_proband(15) = 4; %S5
% study day 26.09.2017:
lumbar_vert_lvl_of_proband(16) = 4; %S6
lumbar_vert_lvl_of_proband(17) = 3; %S6
% study day 05.12.2017:
lumbar_vert_lvl_of_proband(18) = 2; %S8
lumbar_vert_lvl_of_proband(19) = 4; %S8, 1st and 2nd try = L3 - 2x fail
% study day 07.12.2017:
lumbar_vert_lvl_of_proband(20) = 4; %S8 (task = L4 - correct)
% study day 21.12.2017:
%lumbar_vert_lvl_of_proband(21) = 4; %S7  = p21 (fail)
% study day 29.12.2017:
%lumbar_vert_lvl_of_proband(22) = 2; %S7  = p21_wdh (nur R, failed)
lumbar_vert_lvl_of_proband(21) = 2; %S7  = p21_wdh_2 (R + L good)
% study day 05.01.2018:
lumbar_vert_lvl_of_proband(22) = 3; %S7  = p22

%% Probands and vertebra_levels (p1, try1, p2, try1, p3, try1, ..)
% p4, try1: Pediculated L1...  -> NaN
lumbar_true_vert_lvls_of_probands_sorted_by_try_id = [2,3,2,2,3,3,2,2,3,4,4,3,2,3,4,4,3,2,3,4,2,3,2,3,3,2,4,3,2,2,3,4,4,3,2,3,4,4,3,2,3,4,2,3];

%% Probands who interchanged side of vertebra (wrong side for 1st try)
proband_ids_start_left_instead_of_right = [3,5,6,11:15,19,22];

%% Grouping probands -> Experienced and Novices (array 'groups')
% Setting property 'NOVICE' and 'EXPERIENCED' for column 'group'
% --------------------------------------------------------------------
% create cell for group-associations
% Note: Proband 6 = experienced but very low OR experience (2 interventions only)
% Note: Proband 10 = experienced but very low OR experience (1 intervention only)
% Note: Proband 19 = experienced but low OR experience (5-10 interventions only)
groups([1:6,10]) = {'novice'};  % probands 1-6 + 10 = novice
groups(7:9) = {'experienced'};  % probands 7-9 = experienced
groups(11) = {'experienced'};   % proband 11 = experienced
groups(12:15) = {'novice'};     % probands 12-15 = novice
groups(16:22) = {'experienced'};     % probands 16-22 = experienced
% Weitere Experten: p18, p20, p21, p22 (experts by numbers)
% p1-p22: 11 Experienced vs 11 Novice

% 44x1 cell (column-vector) for table (sorted by try_id!)
groups_sorted_by_try_id = [groups groups]';

% Experienced probands with more than 20 surgeries involving psp ('experts')
proband_ids_exp_10_or_more_surgeries = [11,20,18,16,8,9,17,21,7,22]; % 10 surgeries x 2-4 screws = 20-40 screws

%% Config flags
write_data_to_file = false;
write_images_to_png = false;

%% Ideal views

% Ideal rotation: L2 = 9�, L3 = 7�, L4 = 4�
% Ideal tilt: L2 = -12�, L3 = -12�, L4 = -3�

garm_ideal_orbital = [0,9,7,4];
garm_ideal_tilt = [0,-12,-12,-3];
% garm_ideal_trans = []???

%% Ideal transverse angulation
% (L2 =�12.85 +- 2.75, L3 =�15.25 +- 3.55, L4 =�18.9 +- 3.5) (Lien, Liou 2007, 15 specimen)
% (L2 =�12 +- 3.5, L3 =�14.4 +- 3.8, L4 =�17.7 +- 5.2) (Zindrick 1987, 85 specimen)
% rounding angles of both measurements:
pedicle_ideal_transverse_angles =  [12.4, 14.8, 18.3];
[~, idx_b] = ismember(lumbar_true_vert_lvls_of_probands_sorted_by_try_id, [2 3 4]);
pedicle_ideal_ml_angles_sorted_by_try_id = pedicle_ideal_transverse_angles(idx_b);
clear pedicle_ideal_transverse_angles;
clear idx_b;

%% Pedicle ring mesh filenames
pedicle_meshes = {
    './meshes/Mecanix_L2_left.obj',...
    './meshes/Mecanix_L2_right.obj',...
    './meshes/Mecanix_L3_left.obj',...
    './meshes/Mecanix_L3_right.obj',...
    './meshes/Mecanix_L4_left.obj',...
    './meshes/Mecanix_L4_right.obj'};

%% Pedicle meshes titles for use in plots
pedicle_meshes_titles_en = {
    'L2 left',...
    'L2 right',...
    'L3 left',...
    'L3 right',...
    'L4 left',...
    'L4 right'};
pedicle_meshes_titles_de = {
    'L2 links',...
    'L2 rechts',...
    'L3 links',...
    'L3 rechts',...
    'L4 links',...
    'L4 rechts'};

%% Path coordinates before perforating the cortical bone (probands' entry points)
% format: path_bone_entered_coordinates{p_id, try_id}
% format of coordinates (3x1) = [-0.0267,0.0886,-0.0256];
% Definition: Point in time short before acceleration into z-axis on bone surface or
% short below the surface (accuracy of intrument tip due to sensor?)
% Note: Valid position includes the last attempt before final position is reached.
% Note: Y pos of 1st try should be > 0, if proband did start with right
% pedicle as instructed
% TODO (MM): Compare pos with _first_ positions when there are several
% attempts.
path_bone_entered_coordinates{1,1} = [-0.04272,0.01283,0.0780];
path_bone_entered_coordinates{1,2} = [-0.04798,-0.01047,0.08812];
path_bone_entered_coordinates{2,1} = [0.03098,0.01687,0.06916];
path_bone_entered_coordinates{2,2} = [0.03074,-0.01473,0.07177];
path_bone_entered_coordinates{3,1} = [-0.04353,-0.01762,0.07511];
path_bone_entered_coordinates{3,2} = [-0.002734,0.01586,0.07762];
path_bone_entered_coordinates{4,1} = [-0.04402,0.02286,0.07203];
path_bone_entered_coordinates{4,2} = [-0.04094,-0.02096,0.07775];
path_bone_entered_coordinates{5,1} = [-0.008419,-0.01669,0.08003];
path_bone_entered_coordinates{5,2} = [0.03896,0.02068,0.06166];
path_bone_entered_coordinates{6,1} = [-0.004282,-0.014975,0.080759];
path_bone_entered_coordinates{6,2} = [-0.000177,0.021693,0.07751];
path_bone_entered_coordinates{7,1} = [-0.042296,0.021598,0.075564];
path_bone_entered_coordinates{7,2} = [-0.040357,-0.016585,0.079713];
path_bone_entered_coordinates{8,1} = [-0.038664,0.025939,0.076434];
path_bone_entered_coordinates{8,2} = [-0.042533,-0.01393,0.087068];
path_bone_entered_coordinates{9,1} = [-0.002333,0.020939,0.081577];
path_bone_entered_coordinates{9,2} = [-0.004361,-0.016743,0.081282];
path_bone_entered_coordinates{10,1} = [0.030171,0.018207,0.074315];
path_bone_entered_coordinates{10,2} = [0.032158,-0.017614,0.074847];
path_bone_entered_coordinates{11,1} = [0.037639,-0.020987,0.06475];
path_bone_entered_coordinates{11,2} = [0.029064,0.022818,0.075057];
path_bone_entered_coordinates{12,1} = [-0.004021,-0.019133,0.081839];
path_bone_entered_coordinates{12,2} = [-0.006871,0.016446,0.081978];
path_bone_entered_coordinates{13,1} = [-0.045692,-0.015608,0.08972];
% no idea why 13,2 is so deep below the cortical bone
% large amount of movement within solid bone makes no sense!
path_bone_entered_coordinates{13,2} = [-0.042952,0.017443,0.073793];
path_bone_entered_coordinates{14,1} = [-0.003409,-0.024576,0.067855];
path_bone_entered_coordinates{14,2} = [0.002778,0.023567,0.067905];
path_bone_entered_coordinates{15,1} = [0.032505,-0.01906,0.074891];
path_bone_entered_coordinates{15,2} = [0.032003,0.019269,0.073883];
path_bone_entered_coordinates{16,1} = [0.03872,0.019198,0.07008];
path_bone_entered_coordinates{16,2} = [0.03628,-0.0206,0.06432];
path_bone_entered_coordinates{17,1} = [0.000781,0.022599,0.06375];
path_bone_entered_coordinates{17,2} = [-0.001051,-0.01923,0.071959];
path_bone_entered_coordinates{18,1} = [-0.033934,0.023424,0.075364];
path_bone_entered_coordinates{18,2} = [-0.040926,-0.018647,0.076056];
path_bone_entered_coordinates{19,1} = [-0.001531,-0.012537,0.073532];
path_bone_entered_coordinates{19,2} = [-0.000808,0.022099,0.080905];
path_bone_entered_coordinates{20,1} = [0.03497,0.021038,0.060858];
path_bone_entered_coordinates{20,2} = [0.03638,-0.02246,0.0671];
path_bone_entered_coordinates{21,1} = [-0.044593,0.019617,0.090677];
path_bone_entered_coordinates{21,2} = [-0.044045,-0.017838,0.087896];
% p22,1 but especially 2 makes awkward 'loops' when advancing the needle.
% Video confirms: Sensor was loose!
path_bone_entered_coordinates{22,1} = [0.00005,-0.024097,0.068836];
path_bone_entered_coordinates{22,2} = [0.001083,0.020401,0.069542];

%% Mesh coordinates for Ideal Entry Points
% Definition (AO Surgery Reference): Intersection of Mamillary process
% (medio-lateral) and Mid transverse process 
% Remember: Posterior view, parallel vertebra endplates
entry_point_l2_left = [-0.03994,-0.01779,0.08091];
entry_point_l2_right = [-0.03937,0.02116,0.08062];
entry_point_l3_left = [-0.001238,-0.0196,0.06924];
entry_point_l3_right = [-0.000592,0.02188,0.07092];
entry_point_l4_left = [0.03534,-0.02256,0.06597];
entry_point_l4_right = [0.03807,0.02275,0.06341];

% array for looping through to get closes neighbor
entry_points_ideal_array = [entry_point_l2_left; entry_point_l2_right; entry_point_l3_left; entry_point_l3_right; entry_point_l4_left; entry_point_l4_right];

%% Mesh coordinates for top (dorsal edge) of spinal canal
% manually identified
spinal_canal_z_axis_top_l2 = 0.07375;
spinal_canal_z_axis_top_l3 = 0.06686;
spinal_canal_z_axis_top_l4 = 0.06174;

%% Mesh coordinates for bottom (ventral edge) of spinal canal
% manually identified
spinal_canal_z_axis_bottom_l2 = 0.0614;
spinal_canal_z_axis_bottom_l3 = 0.0549;
spinal_canal_z_axis_bottom_l4 = 0.0499;

%% Colormaps
color_orange_light = [253,187,132]./255; % '#FDBB84'
color_blue_brewer2 = [107,174,214]./255;
color_gray = [189,189,189]./255;
colormap_phases_gob = [color_gray;color_orange_light;color_blue_brewer2];
color_green_brewer = [102,194,166]./255;
color_blue_brewer = [49,130,189]./255;
colormap_groups_gb = [color_green_brewer;color_blue_brewer];