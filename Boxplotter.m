classdef Boxplotter < handle & Logger
    
    properties
        SyntrackIo_ref;
        proband_ids_;
        io_statistics_data_table_;  % reference to statistical calc dataset
        io_evaluation_data_table_;  % reference to evaluation dataset to draw graphs
        dataset_reformatted_;
        graphs_ = struct();
        language_;
    end
    
    properties (Constant)
        kClassName = 'Boxplotter';
        kVerbose = true;
        % see definitions.m for colormaps
        kColormapPhasesGob = [[189,189,189]./255;[253,187,132]./255;[107,174,214]./255];
        kColormapGroupsGb = [[102,194,166]./255;[49,130,189]./255];
    end

    methods
        function obj = Boxplotter(io_ref, lang)
            obj.SyntrackIo_ref = io_ref;
            obj.proband_ids_ = obj.SyntrackIo_ref.proband_ids_;
            probands = obj.proband_ids_;
            definitions;

            if(isempty(obj.SyntrackIo_ref.evaluation_data_table_))
                obj.Message("No evaluation data found. Trying to load saved data from .xlsx files..");
                obj.SyntrackIo_ref.GetEvaluationData('Source', 'xlsx');
            else
                obj.io_evaluation_data_table_ = obj.SyntrackIo_ref.evaluation_data_table_;
            end
            obj.Message("> Boxplotter loaded with evaluation_data_table_.");
            
            if(~exist('lang', 'var'))
                obj.Message("No language set. Default output language: 'EN'.");
                obj.language_ = 'EN';
            else
                obj.language_ = lang;
                obj.Message("Default output language set to: '" + string(lang) + "'.");
            end
        end
        
        function success = RunTests(obj)
            % RUNTESTS Test all the class methods here.
            success = true;

            if success
                disp('Tests for Boxplotter class are all working');
            else
                warning('Not all Tests for Boxplotter class are working. Please debug and fix.');
            end
        end
        
        function struct_reformatted = ReformatDatasetTimes(obj)
            % Helper function to reformat the dataset to be compatible with
            % gramm-Boxplotter (sub-grouping of variables)
            dataset = obj.io_evaluation_data_table_;
            subset1(1:44) = {'C-arm positioning, EP targeting'};
            subset2(1:44) = {'Cortical perforation'};
            subset3(1:44) = {'Trocar placement'};
            phases = [subset1';subset2';subset3'];
            %groups_subset = [dataset.group,num2cell(dataset.try_id)];
            %group_exp1 = (dataset.group == 'experienced' && dataset.try_id == 1)
            %group_nov1 = (dataset.group == 'novice' && dataset.try_id == 1)
            % idea: get indices -> add new column with 'exp try', 'nov
            % try1' etc.
            groups_subset = [dataset.group];
            groups = [groups_subset;groups_subset;groups_subset];
            times = [dataset.time_start_to_skin;dataset.time_skin_to_bone;dataset.time_bone_to_end];
            dataset_return.phases = phases;
            dataset_return.groups = groups;
            dataset_return.times = times;
            struct_reformatted = dataset_return;
        end
        
        function struct_reformatted = ReformatDatasetXray(obj)
            % Helper function to reformat the dataset to be compatible with
            % gramm-Boxplotter (sub-grouping of variables)
            dataset = obj.io_evaluation_data_table_;
            subset1(1:44) = {'C-arm positioning, EP targeting'};
            subset2(1:44) = {'Cortical perforation'};
            subset3(1:44) = {'Trocar placement'};
            phases = [subset1';subset2';subset3'];
            %groups_subset = [dataset.group,num2cell(dataset.try_id)];
            %group_exp1 = (dataset.group == 'experienced' && dataset.try_id == 1)
            %group_nov1 = (dataset.group == 'novice' && dataset.try_id == 1)
            % idea: get indices -> add new column with 'exp try', 'nov
            % try1' etc.
            groups_subset = [dataset.group];
            groups = [groups_subset;groups_subset;groups_subset];
            xrays = [dataset.shots_start_skin_cut;dataset.shots_skin_cut_bone;dataset.shots_bone_end];
            trys = [dataset.try_id;dataset.try_id;dataset.try_id];
            dataset_return.phases = phases;
            dataset_return.groups = groups;
            dataset_return.xrays = xrays;
            dataset_return.trys = trys;
            struct_reformatted = dataset_return;
        end
        
        function figure_handle = PlotScoreExperienced(obj)
            % Graphical stats for breaches with Gertzbein & Robins Classification
            % Version 1 (stacked barplot Grade A, Grade B, > B)
            a = 'A (0 mm)';
            b = 'B (0-2 mm)';
            c = '> B (> 2 mm)';
            exp = 'experienced';
            nov = 'novice';
            clear g;
            breaches_plot.x = {exp,exp,exp,nov,nov,nov}';
            breaches_plot.y = [7/8,1/8,0/8,7/12,1/12,4/12]'*100;
            breaches_plot.class = {a,b,c,a,b,c}';
            
            g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'lightness',breaches_plot.class);
            g.geom_bar('stacked',true,'width',0.4);
            g.set_names('x','Gruppe','y','Anzahl an Schrauben (%)','lightness','Klassifikation');
            g.set_title({'Klassifikation der Schraubenendlage','(nach Gertzbein & Robbins)'},'FontSize',13);
            g.axe_property('YLim',[0 100]);
            g.set_order_options('lightness',0); % Order x-axis as raw order
            g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
            % format text styles
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            g.draw();
            figure_handle = gcf;
            % saving figure to class
            obj.graphs_.ScoreExperienced = figure_handle;
            
            % saving figure to file
            savefig('Figure_ScoreExperienced.fig');
        end
        
        function figure_handle = PlotScoreNovices(obj)
            dataset = obj.ReformatDataset();
            g = gramm('x', dataset.groups, 'y', dataset.times, 'group', dataset.phases);
            g.stat_summary('geom',{'bar' 'black_errorbar'},'width',0.5,'dodge',0);
            figure;
            g.draw()
            figure_handle = gcf;
            
            % saving figure to class
            obj.graphs_.PhasesStats= figure_handle;
            
            % saving figure to file
            savefig('Figure_PhasesStats.fig');
        end
              
        function figure_handle = PlotGertzbeinStats(obj)
            % Graphical stats for breaches with Gertzbein & Robins Classification
            % Version 1 (stacked barplot Grade A, Grade B, > B)
            % colors: e81f42 (red), ffdb00 (orange), 5cd680 (green)
            a = 'A (0 mm)';
            b = 'B (0-2 mm)';
            c = '> B (> 2 mm)';
            exp = 'experienced';
            nov = 'novice';
            clear g;
            breaches_plot.x = {exp,exp,exp,nov,nov,nov}';
%             breaches_plot.y = [20/22,1/22,1/22,11/22,4/22,7/22]'*100;
            breaches_plot.y = [20,1,1,11,5,6];
            breaches_plot.class = {a,b,c,a,b,c}';
            
            g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'lightness',breaches_plot.class);
            g.geom_bar('stacked',true,'width',0.4);
            g.set_names('x','Gruppe','y','Anzahl an Schrauben (%)','lightness','Klassifikation');
            g.set_title({'Klassifikation der Schraubenendlage','(nach Gertzbein & Robbins)'},'FontSize',13);
            %g.axe_property('YLim',[0 100]);
            g.axe_property('YLim',[0 22], 'YTick', (0:2:22));
            g.set_order_options('lightness',0); % Order x-axis as raw order
            g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
            % format text styles
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            g.draw();
            figure_handle = gcf;
            % saving figure to class
            obj.graphs_.GertzbeinStats = figure_handle;
            
            % saving figure to file
            %savefig('Figure_GertzbeinStats.fig');
            
                       
            % Graphical stats for breaches with Gertzbein & Robins Classification
            % Alternative view
            % colors: e81f42 (red), ffdb00 (orange), 5cd680 (green)
            a = 'A (< 0 mm)';
            b = 'B (0-2 mm)';
            c = 'C (2-4 mm)';
            d = 'D (4-6 mm)';
            e = 'E (> 6 mm)';
            exp = 'experienced';
            nov = 'novice';
            clear g;
            
            T_eval = sortrows(obj.io_evaluation_data_table_, "gertzbein_class");
            breaches_classes = categorical(T_eval.gertzbein_class);
            
            breaches_plot.x = categorical(T_eval.group);
            breaches_plot.class = categorical(breaches_classes,categorical({'A','B','C','D','E'}), {a, b, c, d, e});
            
            g2=gramm('x',breaches_plot.x,'y',breaches_plot.class,'lightness',breaches_plot.class);
            g2.stat_bin('width',0.6);
            g2.set_names('x','Gruppe','y','Schrauben','lightness','Klassifikation');
            g2.set_title({'Klassifikation der Schraubenendlage','(nach Gertzbein & Robbins)'},'FontSize',13);
            g2.axe_property('YLim',[0 20]);
            g2.set_order_options('lightness',0); % Order x-axis as raw order
            g2.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
            % format text styles
            g2.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            %g2.geom_label('color','b','dodge',0.7,'VerticalAlignment','bottom','HorizontalAlignment','center');
            g2.draw();
            figure_handle = gcf;
            % saving figure to class
            obj.graphs_.GertzbeinStats2 = figure_handle;
            
            % saving figure to file
            savefig('Figure_GertzbeinStats2.fig');
            
            
            T_eval = sortrows(obj.io_evaluation_data_table_, "gertzbein_class");
            plotting.x = categorical(T_eval.group);
            plotting.y = T_eval.gertzbein_distance;
            colormap_groups_gb = obj.kColormapGroupsGb;
            
            g3 = gramm('x', plotting.x, 'y', plotting.y, 'color', plotting.x);
            g3.axe_property('YLim',[-4 10]);
            g3.stat_boxplot('width',0.4,'dodge',0.6);
            
            g3.set_names('x', 'Groups','y', 'Distance of screw to pedicle wall /mm','color', 'Group');
            g3.set_title({'Screw placement:','Distance from screw thread to pedicle wall'});
            g3.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g3.set_order_options('x', 0, 'color', {'novice', 'experienced'});
            g3.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',0.9,...
                'title_scaling',1.0);
            g3.set_point_options('base_size',3);
            figure('Units','normalized','Position',[0.325 0.25 0.35 0.5]);
            g3.draw();
            graph(3) = g3;
            figure_handles(3) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
        end
        
        function figure_handle = PlotVertebraStats(obj)
            % Graphical stats for wrong level
            t = 'Richtig';
            f = 'Falsch';
            exp = 'experienced';
            nov = 'novice';
            clear g;
            
            ident_plot.x = {'experienced','experienced','novice','novice'}';
            ident_plot.y = [8/8,0/8,5/12,7/12]'*100;
            ident_plot.class = {t,f,t,f};
            
            g=gramm('x',ident_plot.x,'y',ident_plot.y,'lightness',ident_plot.class);
            g.geom_bar('stacked',true,'width',0.4);
            g.set_names('x','Gruppe','y','Pedikulierte Wirbelk�rper (%)','lightness','Identifikation');
            g.set_title({'Identifikation des korrekten','Wirbelk�rpers'},'FontSize',13);
            g.axe_property('YLim',[0 100]);
            g.set_order_options('lightness',0); % Order x-axis as raw order
            g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
            % format text styles
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            g.draw();
            figure_handle = gcf;
            
            % saving figure to class
            obj.graphs_.VertebraStats = figure_handle;
            % saving figure to file
            savefig('Figure_VertebraStats.fig');
        end
        
        function success = PlotAOMData(obj)
            % AoM evaluation - Data must be reformatted to used with Gramm
            % e.g. 
            % cols: phase | aom_type | number | surgeon_experience
            % rows: bone_to_perf | aom_large |140 | nov
            % e.g.
            % cols: attribute | attribute | value
            % rows: exp | aom_type | 100
            
            obj.Message("> Plotting AoM evaluation..");
            
            T_evaldata = obj.io_evaluation_data_table_;
            
            colormap_phases_gob = obj.kColormapPhasesGob;
            colormap_groups_gb = obj.kColormapGroupsGb;
            
            % Language selection for output
            lang = 'EN'; % EN or DE
            lang_struct = struct();
            
            save_image = true;
            
            if(lang == 'EN')
                % English
                lang_struct.exp_name = 'experienced';     %use for subset selection!
                lang_struct.nov_name = 'novice';                %use for subset selection!
                lang_struct.th_level_name = 'Thoracic';         %use for subset selection!
                lang_struct.l_level_name = 'Lumbar';            %use for subset selection!
                lang_struct.view_ap_name = 'AP';                %use for subset selection!
                lang_struct.view_lat_name = 'Lateral';          %use for subset selection!
                lang_struct.title1 = {'Instrument usage:';'Detected Areas of Movement (AoMs) per Screw and Group'};
                lang_struct.x_name1 = 'Pedicle screw';
                lang_struct.y_name1 = 'Counted AoMs';
                lang_struct.try1_name = '1st';
                lang_struct.try2_name = '2nd';
                lang_struct.color_name1 = 'Group';
                lang_struct.title2 = {'Instrument usage:';'Areas of Movement comparison (both screws)'};
                lang_struct.x_name2 = 'AoM type';
                lang_struct.y_name2 = 'Counted AoMs';
                lang_struct.color_name2 = 'Group';
                lang_struct.title3 = {'Simulation study:';'Flouroscopy Analysis (Sum of Lateral and AP images)'};
                lang_struct.x_name3 = lang_struct.color_name1;
                lang_struct.y_name3 = lang_struct.y_name1;
                lang_struct.color_name3 = lang_struct.color_name2;
                lang_struct.phase_name = 'Procedure';
            elseif (lang == 'DE')
                % German
                lang_struct.exp_name = 'Facharzt/Oberarzt';     %use for subset selection!
                lang_struct.nov_name = 'Assistent';             %use for subset selection!
                lang_struct.th_level_name = 'Thorakal';         %use for subset selection!
                lang_struct.l_level_name = 'Lumbal';            %use for subset selection!
                lang_struct.view_ap_name = 'AP';                %use for subset selection!
                lang_struct.view_lat_name = 'Lateral';          %use for subset selection!
                lang_struct.title1 = {'Reale OP-Beobachtungen:';'Analyse der Zeitdauer je Prozedur'};
                lang_struct.x_name1 = 'Prozedur';
                lang_struct.y_name1 = 'Zeitdauer /Sekunden';
                lang_struct.try1_name = '1. Schraube';
                lang_struct.try2_name = '2. Schraube';
                lang_struct.color_name1 = 'Wirbels�ulenabschnitt';
                lang_struct.title2 = lang_struct.title1;
                lang_struct.x_name2 = lang_struct.x_name1;
                lang_struct.y_name2 = lang_struct.y_name1;
                lang_struct.color_name2 = 'Gruppe';
                lang_struct.title3 = {'Reale OP-Beobachtungen:';'Analyse der Zeitdauer bis zur K-Draht Platzierung'};
                lang_struct.x_name3 = lang_struct.color_name1;
                lang_struct.y_name3 = lang_struct.y_name1;
                lang_struct.color_name3 = lang_struct.color_name2;
                lang_struct.phase_name = 'Prozedur';
            end
                       
            % Format table according to Language and Categorical columns
            T = obj.TableLanguageAndCategories(T_evaldata, lang_struct);
            
            % Large AoMs per Try and AoM eval
            clear g;
            plotting.x = [T.try_id; T.try_id];
            plotting.y = [T.aoms_large_count_start_end; T.aoms_small_count_start_end];
            plotting.groups = [T.group; T.group];
            subset_cell(1:length(T.aoms_large_count_start_end)) = {'large'}; % is also column name (plot title)!
            subset_large = [subset_cell'];
            clear subset_cell;
            subset_cell(1:length(T.aoms_small_count_start_end)) = {'small'};
            subset_small = [subset_cell'];
            plotting.aoms = [subset_large; subset_small];
            
            g = gramm('x', plotting.x, 'y', plotting.y, 'color', plotting.groups);
            g.facet_grid([], plotting.aoms, 'scale','independent');
            g.axe_property('YLim',[0 300]); % general setting for both plots
            g.stat_boxplot('width',0.4,'dodge',0.6);
            
            g.set_names('x',lang_struct.x_name1,'y',lang_struct.y_name1,'color',lang_struct.color_name1, 'column', 'AoMs');
            g.set_title(lang_struct.title1);
            g.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g.set_order_options('x', 0, 'color', {'novice', 'experienced'});
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',0.9,...
                'title_scaling',1.0);
            g.set_point_options('base_size',3);
            figure('Units','normalized','Position',[0.25 0.25 0.5 0.5]);
            g.draw();
            set(g.facet_axes_handles(1),'YLim', [0 80]); % for left plot
            graph(1) = g;
            figure_handles(1) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            
            % 2nd evaluation.. AoMs            
            g2 = gramm('x', plotting.aoms, 'y', plotting.y, 'color', plotting.groups);
            g2.axe_property('YLim',[0 250]);
            g2.stat_boxplot('width',0.4,'dodge',0.6);
            
            g2.set_names('x',lang_struct.x_name2,'y',lang_struct.y_name2,'color',lang_struct.color_name1);
            g2.set_title(lang_struct.title2);
            g2.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g2.set_order_options('x', 0, 'color', {'novice', 'experienced'});
            g2.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',0.9,...
                'title_scaling',1.0);
            g2.set_point_options('base_size',3);
            figure('Units','normalized','Position',[0.325 0.25 0.35 0.5]);
            g2.draw();
            graph(2) = g2;
            figure_handles(2) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            
            if(save_image == true)
                % Save images
                title = 'aoms';
                obj.SaveResultsToGraphics(figure_handles, lang, graph, title);
            end
            
            obj.Message("Plotting done.");
        end
        
        function figure_handle = PlotDurations(obj)
            save_images = true;
            
            lang = 'EN';
            boxplot = false;
                   
            exp = 'experienced';
            nov = 'novice';
            
            colormap_phases_gob = obj.kColormapPhasesGob;
            colormap_groups_gb = obj.kColormapGroupsGb;
            
            T_eval = obj.io_evaluation_data_table_;
            
            plot.attempts = [categorical(T_eval.try_id);categorical(T_eval.try_id);categorical(T_eval.try_id)];
            plot.y = [T_eval.time_start_to_skin;T_eval.time_skin_to_bone;T_eval.time_bone_to_end];
            subset1(1:44) = {'Start -> Skin perf.'};
            subset2(1:44) = {'Skin perf. -> Needle adv.'};
            subset3(1:44) = {'Needle adv. > End'};
            plot.phases = [subset1';subset2';subset3'];
            plot.groups = [T_eval.group;T_eval.group;T_eval.group];
            
            g=gramm('x',plot.attempts,'y',plot.y,'color', plot.phases);
            %g.stat_boxplot('width',0.6,'dodge',0.8);
            g.stat_summary('type', 'ci', 'geom',{'bar','black_errorbar'}, 'width',0.4,'dodge',0.6);
            % set patch: EdgeColor..
            g.set_line_options('base_size', 1);
            g.axe_property('YLim',([0 550]));
            g.axe_property('YTick', 0:50:550);
            
            
            g.set_names('x','Screw no.','y','Duration /sec', 'color', 'Phases', 'column', 'Group');
            g.set_title({'Simulation: Durations','Phases and Groups by Screw number - 95% CI'},'FontSize',13);
            g.set_color_options('map', colormap_phases_gob, 'n_color', length(colormap_phases_gob), 'n_lightness', 1);
            %g.set_order_options();

            % Grid-Plot (with 2 colums because facet_warp([cols-separated]))
            g.facet_wrap(plot.groups, 'scale','fixed');
            g.set_order_options('x', 0, 'color', 0, 'column', [2 1]);
            
            % format text styles            
            g.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.175 0.25 0.65 0.5]);
            g.draw();
            graph(1) = g;
            figure_handles(1) = gcf;
            % saving figure to class
            obj.graphs_.DurationData = figure_handles(1);
            
            plot.attempts = categorical(T_eval.try_id);
            plot.y = T_eval.time_total;
            plot.groups = categorical(T_eval.group);
            
            g2=gramm('x',plot.attempts,'y',plot.y,'color', plot.groups);
            
            if(boxplot)
                g2.stat_boxplot('width',0.5,'dodge',0.75);
                g2.axe_property('YLim',([0 1100]));
                g2.axe_property('YTick', 0:100:1100);
            else 
                g2.stat_summary('type', 'ci', 'geom',{'bar','black_errorbar'}, 'width',0.4,'dodge',0.6);
                g2.set_line_options('base_size', 1);
                g2.axe_property('YLim',([0 900]));
                g2.axe_property('YTick', 0:100:900);
            end
            
            
            g2.set_names('x','Screw no.','y','Duration /sec', 'color', 'Group');
            g2.set_title({'Simulation: Durations','Groups by Screw number'},'FontSize',13);
            g2.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g2.set_order_options('x',0, 'color', 0);
            
            % format text styles            
            g2.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.35 0.25 0.3 0.5]);
            g2.draw();
            graph(2) = g2;
            figure_handles(2) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            % saving figure to class
            obj.graphs_.DurationData = figure_handles(2);
            
            plot.y = T_eval.time_total;
            plot.groups = T_eval.group;
            
            g3=gramm('x',plot.groups,'y',plot.y,'color', plot.groups);
            
            if(boxplot)
                g3.stat_boxplot('width',0.5,'dodge',0.75);
                g3.axe_property('YLim',([0 1100]));
                g3.axe_property('YTick', 0:100:1100);
            else 
                g3.stat_summary('type', 'ci', 'geom',{'bar','black_errorbar'}, 'width',0.4,'dodge',0.6);
                g3.set_line_options('base_size', 1);
                g3.axe_property('YLim',([0 700]));
                g3.axe_property('YTick', 0:100:700);
            end
            
            
            g3.set_names('x',' ','y','Duration /sec', 'color', 'Group');
            g3.set_title({'Simulation: Durations','Placement of 2 Screws by Group'},'FontSize',13);
            g3.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g3.set_order_options('color', 0, 'x', [2 1]);

            % format text styles            
            g3.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.35 0.25 0.3 0.5]);
            g3.draw();
            graph(3) = g3;
            figure_handles(3) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            % saving figure to class
            obj.graphs_.DurationData = figure_handles(3);
            
            if(boxplot)
                title = 'duration_boxplot';
            else
                title = 'duration_bars';
            end
            
            if(save_images)
                obj.SaveResultsToGraphics(figure_handles, lang, graph, title);
            end
        end
        
        function figure_handle = PlotPathLengths(obj)
            exp = 'experienced';
            nov = 'novice';
            
            T_eval = obj.io_evaluation_data_table_;
            
            % 1) Plot path lengths for _large_ AOMs
            
            plot.x = [categorical(T_eval.try_id);categorical(T_eval.try_id);categorical(T_eval.try_id)];
            plot.y = [T_eval.instr_path_aoms_small_start_skin;T_eval.instr_path_aoms_small_skin_bone_first;T_eval.instr_path_aoms_small_bone_first_end];
            subset1(1:44) = {'Start -> Skin perforation'};
            subset2(1:44) = {'Skin perforation -> Cortical perforation'};
            subset3(1:44) = {'Cortical perforation -> End'};
            plot.phases = [subset1';subset2';subset3'];
            plot.groups = [T_eval.group;T_eval.group;T_eval.group];
            
            g=gramm('x',plot.x,'y',plot.y,'color', plot.phases);
            g.stat_boxplot('width',0.6,'dodge',0.8);
            g.set_names('x','Screw No.','y','Zeitdauer /s', 'color', 'Phases','column', 'Group');
            g.set_title({'Pfadl�nge (nur small AOMs)','(je Phase und Gruppe)'},'FontSize',13);
            g.set_order_options('x', 0, 'color', [3 2 1]);
            %g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');

            % Grid-Plot (with 2 colums because facet_grid([row-separated], [cols-separated]))
            g.facet_grid([], plot.groups, 'scale','fixed');
            g.axe_property('YLim',[0 250]); % general setting for both plots
            %g.set_order_options('x', 0, 'color', [3 2 1]);
            
            % format text styles            
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            g.draw();
            figure_handle = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            %figure_handle.
            % saving figure to class
            obj.graphs_.PathLengths = figure_handle;
            
            % saving figure to file
            savefig('Figure_PathLengths_aoms_small.fig');
            
            
            % 2) Plot path lengths for _large_ AOMs
            clear g;
            plot.y = [T_eval.instr_path_aoms_large_start_skin;T_eval.instr_path_aoms_large_skin_bone_first;T_eval.instr_path_aoms_large_bone_first_end];
            subset1(1:44) = {'Start > Skin perforation'};
            subset2(1:44) = {'Skin perforation -> Cortical perforation'};
            subset3(1:44) = {'Cortical perforation > End'};
            plot.phases = [subset1';subset2';subset3'];
            plot.groups = [T_eval.group;T_eval.group;T_eval.group];
            
            g=gramm('x',plot.x,'y',plot.y,'color', plot.phases);
            g.stat_boxplot('width',0.6,'dodge',0.8);
            g.set_names('x','Screw No.','y','Zeitdauer /s', 'color', 'Phases','column', 'Group');
            g.set_title({'Pfadl�nge (nur large AOMs)','(je Phase und Gruppe)'},'FontSize',13);
            g.set_order_options('x', 0, 'color', [3 2 1]);
            %g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');

            % Grid-Plot (with 2 colums because facet_grid([row-separated], [cols-separated]))
            g.facet_grid([], plot.groups, 'scale','fixed');
            g.axe_property('YLim',[0 250]); % general setting for both plots
            g.set_order_options('x', 0, 'color', [3 2 1]);
            
            % format text styles            
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            figure;
            g.draw();
            figure_handle = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            %figure_handle.
            % saving figure to class
            obj.graphs_.PathLengths = figure_handle;           
            
            % saving figure to file
            savefig('Figure_PathLengths_aoms_large.fig');
        end
        
        function figure_handle = PlotXrayData(obj)
            color_orange_light = [253,187,132]./255; % '#FDBB84'
            color_blue_brewer2 = [107,174,214]./255;
            color_gray = [189,189,189]./255;
            colormap_phases_gob = [color_gray;color_orange_light;color_blue_brewer2];
            color_green_brewer = [102,194,166]./255;
            color_blue_brewer = [49,130,189]./255;
            colormap_groups_gb = [color_green_brewer;color_blue_brewer];
            colormap_xrays = [[255,237,160]./255; [254,178,76]./255];
            
            exp = 'experienced';
            nov = 'novice';
            
            save_images = true;
            boxplot = true;
            
            T_eval = obj.io_evaluation_data_table_;
            
            plot.x = [categorical(T_eval.try_id);categorical(T_eval.try_id);categorical(T_eval.try_id)];
            plot.y = [T_eval.shots_start_skin_cut;T_eval.shots_skin_cut_bone;T_eval.shots_bone_end];
            subset1(1:44) = {'Start -> Skin perf.'};
            subset2(1:44) = {'Skin perf. -> Needle adv.'};
            subset3(1:44) = {'Needle adv. -> End'};
            plot.phases = [subset1';subset2';subset3'];
            plot.groups = [T_eval.group;T_eval.group;T_eval.group];
            
            g=gramm('x',plot.x,'y',plot.y,'color', plot.phases);
            g.stat_boxplot('width',0.4,'dodge',0.6);
            g.set_names('x','Screw No.','y','X-ray shots', 'color', 'Phases','column', 'Group');
            g.set_title({'X-ray shots per Screw','(by Groups and Phases)'},'FontSize',13);
            g.set_order_options('x', 0, 'color', [3 2 1]);
            g.set_color_options('map', colormap_phases_gob, 'n_color', length(colormap_phases_gob), 'n_lightness', 1);
            %g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');

            % Grid-Plot (with 2 colums because facet_grid([row-separated], [cols-separated]))
            g.facet_grid([], plot.groups, 'scale','fixed');
            g.axe_property('YLim',[0 80]); % general setting for both plots
            g.axe_property('YTick',0:10:80); % general setting for both plots
            g.set_order_options('x', 0, 'color', [3 2 1], 'column', [2 1]);
            
            % format text styles                    
            g.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.175 0.25 0.65 0.5]);
            g.draw();
            graph(1) = g;
            figure_handles(1) = gcf;
            obj.set_marker_size_outliers(gcf, 2.5);
            
            % saving figure to class
            obj.graphs_.XrayData = figure_handles(1);
            
            plot.attempts = categorical(T_eval.try_id);
            plot.y = T_eval.shots_total;
            plot.groups = categorical(T_eval.group);
            
            g2=gramm('x',plot.attempts,'y',plot.y,'color', plot.groups);
            
            if(boxplot)
                g2.stat_boxplot('width',0.5,'dodge',0.75);
                g2.axe_property('YLim',([0 90]));
                g2.axe_property('YTick', 0:10:90);
            else 
                g2.stat_summary('type', 'ci', 'geom',{'bar','black_errorbar'}, 'width',0.4,'dodge',0.6);
                g2.set_line_options('base_size', 1);
                g2.axe_property('YLim',([0 90]));
                g2.axe_property('YTick', 0:10:90);
            end
            
            
            g2.set_names('x','Screw no.','y','X-rays sum', 'color', 'Group');
            g2.set_title({'Simulation: Fluoroscopy usage','Groups by Screw number'},'FontSize',13);
            g2.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g2.set_order_options('x',0, 'color', 0);
            
            % format text styles            
            g2.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.35 0.25 0.3 0.5]);
            g2.draw();
            graph(2) = g2;
            figure_handles(2) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            % saving figure to class
            obj.graphs_.XrayData = figure_handles(2);
            
            plot.y = T_eval.shots_total;
            plot.groups = T_eval.group;
            
            g3=gramm('x',plot.groups,'y',plot.y,'color', plot.groups);
            
            if(boxplot)
                g3.stat_boxplot('width',0.5,'dodge',0.75);
                g3.axe_property('YLim',([0 90]));
                g3.axe_property('YTick', 0:10:90);
            else 
                g3.stat_summary('type', 'ci', 'geom',{'bar','black_errorbar'}, 'width',0.4,'dodge',0.6);
                g3.set_line_options('base_size', 1);
                g3.axe_property('YLim',([0 90]));
                g3.axe_property('YTick', 0:10:90);
            end
            
            
            g3.set_names('x',' ','y','X-rays /Screw', 'color', 'Group');
            g3.set_title({'Simulation: Fluoroscopy usage','Placement of 2 Screws by Group'},'FontSize',13);
            g3.set_color_options('map', colormap_groups_gb, 'n_color', length(colormap_groups_gb), 'n_lightness', 1);
            g3.set_order_options('color', 0, 'x', [2 1]);

            % format text styles            
            g3.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.35 0.25 0.3 0.5]);
            g3.draw();
            graph(3) = g3;
            figure_handles(3) = gcf;
            handles = findobj(gcf, 'MarkerSize', 6);
            set(handles, 'MarkerSize', 2.5);
            % saving figure to class
            obj.graphs_.XrayData = figure_handles(3);
            
            plot.attempts = [categorical(T_eval.try_id);categorical(T_eval.try_id)];
            plot.y = [T_eval.shots_ap;T_eval.shots_lat];
            subset1(1:44) = {'AP'};
            subset2(1:44) = {'Lateral'};
            plot.view = [subset1';subset2'];
            plot.groups = [T_eval.group;T_eval.group];
            
            g4=gramm('x',plot.attempts,'y',plot.y,'color', plot.view);
            g4.stat_boxplot('width',0.4,'dodge',0.6);
            g4.set_names('x','Screw No.','y','X-ray shots', 'color', 'Phases','column', 'Group');
            g4.set_title({'X-ray shots per Screw','(by Groups and Phases)'},'FontSize',13);
            g4.set_order_options('x', 0, 'color', [3 2 1]);
            g4.set_color_options('map', colormap_xrays, 'n_color', length(colormap_xrays), 'n_lightness', 1);
            %g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');

            % Grid-Plot (with 2 colums because facet_grid([row-separated], [cols-separated]))
            g4.facet_wrap(plot.groups, 'scale','fixed');
            g4.axe_property('YLim',[0 80]); % general setting for both plots
            g4.axe_property('YTick',0:10:80); % general setting for both plots
            g4.set_order_options('x', 0, 'color', 0, 'column', [2 1]);
            
            % format text styles                    
            g4.set_text_options('font','Arial',...
                'base_size',11,...
                'label_scaling',1.2,...
                'legend_scaling',1.1,...
                'legend_title_scaling',1.1,...
                'facet_scaling',1.2,...
                'title_scaling',1.0);
            figure('Units','normalized','Position',[0.25 0.25 0.5 0.5]);
            g4.draw();
            graph(4) = g4;
            figure_handles(4) = gcf;
            obj.set_marker_size_outliers(gcf, 2.5);
            
            % saving figure to class
            obj.graphs_.XrayData = figure_handles(4);
            
            if(boxplot)
                title = 'xray_boxplot';
            else
                title = 'xray_bars';
            end
            
            if(save_images)
                lang = 'EN';
                obj.SaveResultsToGraphics(figure_handles, lang, graph, title);
            end
        end
        
        function success = SaveResultsToGraphics(obj, figure_handles, lang, graph, title)
            % saving to graphics..
            % using 'graph' handle from Gramm-Boxplotting
            % using gcf for figure export
            for fn = 1:length(figure_handles)
                disp("Saving image of figure '" + fn + "' to Boxplot_image_" + title + "_" + fn + "_" + lang + ".png/.svg/.fig");
                figure_handles(fn);
                filename_gramm_export = sprintf('Boxplot_image_%s_%s_%s', title, string(fn), lang);
                graph(fn).export('file_name', filename_gramm_export);   % default = svg
                graph(fn).export('file_name', filename_gramm_export, 'file_type','png');    % png
                savefig(sprintf('Boxplot_image_%s_%s.fig', string(fn), lang));
            end
            success = true;
        end
    end
    
    methods (Access = private)
        
        % helpers
        function subset = CreateSubsetColumn (obj, column_name, column_array)
            name = column_name;
            length_subset = column_array;
            subset_cell(1:length_subset) = {name};
            subset = [subset_cell'];
        end
        
        function T_formatted = TableLanguageAndCategories(obj, T, lang_struct)
            % Language renaming
            if(ismember('surgeon_experience', T.Properties.VariableNames))
                T.surgeon_experience = replace(T.surgeon_experience, 'exp', lang_struct.exp_name);
                T.surgeon_experience = replace(T.surgeon_experience, 'nov', lang_struct.nov_name);
            end
            if(ismember('vertebra_level', T.Properties.VariableNames))
                T.vertebra_level = replace(T.vertebra_level, 'thoracic', lang_struct.th_level_name);
                T.vertebra_level = replace(T.vertebra_level, 'lumbar', lang_struct.l_level_name);
            end
            if(ismember('view', T.Properties.VariableNames))
                T.view = replace(T.view, 'ap', lang_struct.view_ap_name);
                T.view = replace(T.view, 'lateral', lang_struct.view_lat_name);
            end
            if(ismember('try_id', T.Properties.VariableNames))
                T.try_id = cellstr(string(T.try_id));
                T.try_id = replace(T.try_id, '1', lang_struct.try1_name);
                T.try_id = replace(T.try_id, '2', lang_struct.try2_name);
            end
            if(ismember('group', T.Properties.VariableNames))
                T.group = replace(T.group, 'experienced', lang_struct.exp_name);
                T.group = replace(T.group, 'novice', lang_struct.nov_name);
            end
            
            % Applying categories (for sorting/grouping)
            if(ismember('try_id', T.Properties.VariableNames))
                T.try_id = categorical(T.try_id);
            end
            if(ismember('group', T.Properties.VariableNames))
                T.group = categorical(T.group);
            end
            if(ismember('surgeon_experience', T.Properties.VariableNames))
                T.surgeon_experience = categorical(T.surgeon_experience);
            end
            if(ismember('vertebra_level', T.Properties.VariableNames))
                T.vertebra_level = categorical(T.vertebra_level);
            end
            if(ismember('view', T.Properties.VariableNames))
                T.view = categorical(T.view);
            end
            
            T_formatted = T;
        end
        
        function figure_handle = Boxplot(obj, table, x, y)
            
            % FUNCTION NOT YET WORKING
            % TODO (MM): Ported from old code -> Change/ Adapat here
            %loading 'Gramm' thirdparty-code
            
            % Graphical stats for Time and X-ray images
            
            clear g;
            % x = Groups
            % y = time (total) OR xrays (sum ap & lat)
            % color = ?
            % subset = mean both / 1st try / 2nd try)
            
            T = T_all;
            plotting2.x = T.group;
            plotting2.y = T.mean_time_total/60; % converting sec -> min
            plotting2.y2 = T.shots_total;
            
            % Time evaluation
            g(1,1)=gramm('x',plotting2.x,'y',plotting2.y,'color',plotting2.x);
            g(2,1)=copy(g(1,1));
            g(2,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
            g(2,1).set_names('x','Gruppe','y','Zeit (min)','color',' ');
            g(2,1).set_title({'Zeitdauer f�r zwei Pedikulierungen','(MW + 95 % CI pro Gruppe)'},'FontSize',13);
            g(2,1).axe_property('YLim',[0 45]);
            % Text f�r MW
            %text('experienced',10,{'Important' 'event'},'Parent',g.facet_axes_handles(1),'FontName','Courier');
            
            g(1,1).stat_boxplot();
            g(1,1).set_names('x','Gruppe','y','Zeit (min)','color',' ');
            g(1,1).set_title('Zeitdauer f�r zwei Pedikulierungen','FontSize',13);
            g(1,1).axe_property('YLim',[0 45]);
            
            % X-ray evaluation
            g(1,2)=gramm('x',plotting2.x,'y',plotting2.y2,'color',plotting2.x);
            g(2,2)=copy(g(1,2));
            g(2,2).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
            g(2,2).set_names('x','Gruppe','y','R�ntgenbilder','color',' ');
            g(2,2).set_title({'Anzahl ben�tigter R�ntgenbilder','(MW + 95 % CI pro Gruppe)'},'FontSize',13);
            g(2,2).axe_property('YLim',[0 250]);
            % Text f�r MW
            
            g(1,2).stat_boxplot();
            g(1,2).set_names('x','Gruppe','y','R�ntgenbilder','color',' ');
            g(1,2).set_title('Anzahl ben�tigter R�ntgenbilder','FontSize',13);
            g(1,2).axe_property('YLim',[0 250]);
            
            g(1,1).no_legend();
            g(1,2).no_legend();
            g(2,1).no_legend();
            g(2,2).no_legend();
            
            g.set_text_options('font','Arial',...
                'base_size',13,...
                'label_scaling',0.9,...
                'legend_scaling',0.9,...
                'legend_title_scaling',0.9,...
                'facet_scaling',1,...
                'title_scaling',1.0);
            
            g.draw();
            figure_handle = gcf;
        end
        
        function success = set_marker_size_outliers(obj, figure_handle, size)
            % default MarkerSize for Gramm = 6
            handles = findobj(figure_handle, 'MarkerSize', 6);
            set(handles, 'MarkerSize', size);
        end
    end
end





