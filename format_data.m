%% Reformatting Data into Struct

%clear proband_table;
%proband_id = 2; % insert proband number here for manual usage

clear cleaned_proband_data;
clear xrays
data = all_recordings(proband_id);

cleaned_proband_data = struct('events',[]);
cleaned_proband_data.events = struct('framepos', [], 'labelcode', [], 'labelname', [],'time_abs_serial', [],'time_abs_dt', [],'time_rel', []);

% Sorting labelevents and timestamps and recorded frame index/position
event.framepos = find(data.label~=0)';
event.labelcode = data.label(event.framepos)';
event.labelname = number_labels(event.labelcode)';
event.time_abs_serial = data.ts(event.framepos)';
event.time_abs_dt = datetime(event.time_abs_serial,'ConvertFrom','datenum');
cleaned_proband_data.events = event;
%event.time_abs_time = datestr(event.time_abs_serial, 'HH:MM:SS.FFF');
%event.time_abs_dt = datetime(event.time_abs_time,'InputFormat','HH:mm:ss.SSS');
clear event;


%% Cleaning recorded data from unwanted events or add missing events like "retry" for 2x bone entered (?)
% Check: (old Proband records associations?!)
% P1: ok
% P2: 1st try: 2x Start & 2x End; Valid: 2nd Start, 1st End
% P3: ok
% P4: 1st try: Tech probs (WHEN exactly? 1st label correct?); 2nd try: 2nd start valid, xray starts in Lat-View (!)
% P5: 3 trys -> 1st try = L3 left (wrong! hint given); 2x bone entered = retry?; 2nd try = L4 right, 3rd try = L4 left (not a valid attempt? too much learned?)
% P6: ok
% P7: 1st try: Tech probs = explanation regarding cpad_switch (15 s) at 00:00:45
% P8: 1st try: 2x bone entered wrong; Valid: 1st label. 
% P9: 1st try: tech probs (starting ezvid; 15 s) at 00:00:36 with no 2nd tech probs label (!)
% P10: Very long 1st try due to complete retry after bone entered! (2x "End"). Valid only 1st "end". 1st Tech probs unclear. Tech probs = Phone (48 s) _after_ first "End" do not matter
% ---- Study day 22.09.2017
% P11: 2nd try: Stilett in wrong pos - tech probs (6 sec)
% P12: 1st try: Tech probs = moving jamshidi handle ( sec)
% P13: 2nd try: Skin cut too late (6 sec.), delete first "Bone entered" (too early)
% P14: 1st try: Delete first "Start", Tech probs (15 sec.) (WHEN?!)
% P15: 1st try: Delete first "retry"; Wrong pos labels = Duration of hint for 'sacrum' -> wrong level hint
% delete 1st "Bone entered" (1st try)
% ---- Study day 26.09.2017
% P16: ok
% P17: 2nd try: Delete second "End"
% P18: 1st try: Delete first "Start"
% P19: 1st try: Start -5s, 2nd try: G-Arm set -5s, Skin cut -3s
% P20: 1st try: Add 1x "Retry"; 2nd try: Delete first "Start"
% P21: 1st try: Tech probs = 15s
% P22: 2nd try: Tech probs = 1:13 min = 73s (WHEN?!) AND images -5 !
% (IMPORTANT! 3 labels with 'tech_probs' -> delete 2nd label of 2nd try)

switch proband_id
    case 1
        % No label for "Bone entered!" -> set "skin cut" to "Bone entered"
        cleaned_proband_data.events.labelcode(4) = 4;
    case 2
        % delete events associated with 1st "Start" of 1st try
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(1)) = [];   
        end
        % delete events for 2nd "End" of 1st try
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'End'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
    case 4
        % 1st try: set time correction due to tech probs (- SS.sss sec)
        tech_probs = cleaned_proband_data.events.time_abs_dt(strcmp(cleaned_proband_data.events.labelname, 'Tech probs'));
        cleaned_proband_data.attempts(1).time_correction = duration(tech_probs(1) - tech_probs(2),'Format','s');
        % 2nd try: delete first start event
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
    case 5
        % delete 3rd "start" and 3rd "end" events 
        % to delete 3rd attempt
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(3)) = [];   
        end
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'End'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(3)) = [];   
        end
    case 7
        % setting time correction for 1st attempt
        cleaned_proband_data.attempts(1).time_correction = seconds(-15);
    case 8
        % delete 2nd "bone entered" label
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Bone entered'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
    case 9
        % 1st try: setting time correction 
        cleaned_proband_data.attempts(1).time_correction = seconds(-15);
    case 10
        % 1st try: delete 2nd "end"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'End'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
        % setting time correction for 1st attempt's tech probs
        %cleaned_proband_data.attempts(1).time_correction = seconds(-22);
    case 11
        % 2nd try: delete first start
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
        % 2nd try: Tech probs time delay
        cleaned_proband_data.attempts(2).time_correction = seconds(-22);
    case 12
    case 13
        % 2nd try: delete first "Bone entered" (number = x)
        %lbl2del = find(cleaned_proband_data.events.labelname == "Bone entered");
        %for fn = fieldnames(cleaned_proband_data.events)'
            %cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        %end
    case 14
        % 1st try: Delete first "Start"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(1)) = [];   
        end
        % 1st try: Time correction tech probs (15 sec)
        cleaned_proband_data.attempts(1).time_correction = seconds(-15);
    case 15
        % 1st try: Delete first "retry"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Retry'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(1)) = [];   
        end
        % 1st try: Delete first "Bone entered"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Bone entered'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(1)) = [];   
        end
        % 1st try: Time correction (wrong pos labels) (  sec)
        cleaned_proband_data.attempts(1).time_correction = seconds(-1);
    case 17
        % 2nd try: Delete second "End" (third overall end)
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'End'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(3)) = [];
        end
    case 18
        % 1st try: Delete first "Start"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(1)) = [];   
        end
    case 19
        % correct Timestamps for V1 start, V2 g-arm set, skin cut
    case 20
        % add retry label for 1st try
        % 2nd try: Delete first "Start"
        lbl2del = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
        for fn = fieldnames(cleaned_proband_data.events)'
            cleaned_proband_data.events.(fn{1})(lbl2del(2)) = [];   
        end
    case 21
        % 1st try: Time correction tech probs (15 sec)
        cleaned_proband_data.attempts(1).time_correction = seconds(-15);
    case 22
        % 2nd try: Time correction tech probs (73 sec)
        cleaned_proband_data.attempts(2).time_correction = seconds(-73);
        % 2nd try: Correct X-ray images -5 !
end

% IMPORTANT AFTER CLEANING!
% ------------------------------------
% Calculating relative timestamps (durations) for events from datetime
% values in precision (HH:MM:SS)
cleaned_proband_data.events.time_rel = cleaned_proband_data.events.time_abs_dt...
    - cleaned_proband_data.events.time_abs_dt(1);


%% Sorting Data for timestamps of start & end and retry count into new substruct 'attempts' according to single attempts
% also calculating durations for each attempt and save it into struct

% Get index of Start and End events and use it to assign data to corresponding attempt
% (!) Assume there is only _one_ start and _one_ end event for each of overall _two_ attempts
% after cleaning the data (!)

% Getting indices for events
start_event_idx = find(strcmp(cleaned_proband_data.events.labelname, 'Start'));
end_event_idx = find(strcmp(cleaned_proband_data.events.labelname, 'End'));
retry_event_logic = strcmp(cleaned_proband_data.events.labelname,'Retry');

% Getting start and end timestamps in absolute serial time (datenum)
trys.start = cleaned_proband_data.events.time_abs_serial(start_event_idx);
trys.end = cleaned_proband_data.events.time_abs_serial(end_event_idx);
trys.retrys = [];

% Assign retrys to corresponding attempts (counting 'end' events so get number)
for attempt_idx=1:numel(end_event_idx)
    % search and count 'retry' events in logic array 
    % search only in section corresponding to each attempt (start -> end)
    trys.retrys(attempt_idx) = length(find(retry_event_logic(start_event_idx(attempt_idx):end_event_idx(attempt_idx))));
end

% Assign timestamps of START & END and counted RETRYS to corresponding attempts
% and calculating DURATION for each attempt and save it into struct
for idx=1:numel(start_event_idx)
    cleaned_proband_data.attempts(idx).start = trys.start(idx);
    cleaned_proband_data.attempts(idx).end = trys.end(idx);
    cleaned_proband_data.attempts(idx).retry_count = trys.retrys(idx);
    cleaned_proband_data.attempts(idx).duration = cleaned_proband_data.events.time_abs_dt(end_event_idx(idx)) - cleaned_proband_data.events.time_abs_dt(start_event_idx(idx));
end

clear trys;
clearvars = {'start_event_idx','end_event_idx','rety_event_logic'};
clear(clearvars{:});


%% Translating numbers of cpad_switch_code into "ap" (= 17), "lat" (= 33), "CT" (= 34) and counting them
% working with timestamps to search sections associated with attempts

xraycode = struct('ap', 17, 'lat', 33, 'ct', 34);

shots = data.xray_event;
cpad_switch_codes = data.cpad_switch_code;
garm_angle = abs(data.orbital_angle);

% evaluate x-ray images by attempt
for a_idx = 1:numel(cleaned_proband_data.attempts)
    pos1 = find(data.ts == cleaned_proband_data.attempts(a_idx).start);
    pos2 = find(data.ts == cleaned_proband_data.attempts(a_idx).end);
    % multiplication of logic matrices (transposed cpad_switch_codes)
    % to get instant result for shots in the right plane!
    xrays.ap = shots(pos1:pos2)*(cpad_switch_codes(pos1:pos2)==xraycode.ap)';
    % counting G-arm orbital rotation for lateral imaging, too!
    lat = shots(pos1:pos2)*(cpad_switch_codes(pos1:pos2)==xraycode.lat)';
    lat_garm = shots(pos1:pos2)*(garm_angle(pos1:pos2)>70)';
    xrays.lat = lat + lat_garm;
    cleaned_proband_data.attempts(a_idx).xrays = xrays;
end
clear pos1;
clear pos2;

%% Computing angles of G-Arm (G-Arm settings) when label X-ray / skin cut / bone entered is set
% to compare values with 'ideal' G-Arm settings for corresponding vertebrae
% use 'Bone entered' or 'Skin cut' label for better accuracy

%angle_orbital = data.orbital_angle;
%angle_tilt = data.tilt_angle;

%frame_set = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelname=="X-Ray set"); 
%frame_cut = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelname=="Cut");
%frame_bone = cleaned_proband_data.events.framepos(cleaned_proband_data.events.labelname=="Bone entered");

% Problem: selection of events that count (1st try, 2nd try)
%
% define struct angles = struct('orbital',[],'tilt,[]);
%
%angles.orbital.set = angle_orbital(frame_set)';
%angles.tilt.set = angle_tilt(frame_set)';
%angles.orbital.cut = angle_orbital(frame_cut)';
%angles.tilt.cut = angle_tilt(frame_cut)';
%angles.orbital.bone = angle_orbital(frame_bone)';
%angles.tilt.bone = angle_tilt(frame_bone)';


%% Creating struct 'cleaned_proband_data' and convert into table 'proband_table'
t = struct();
t.proband = proband_id;

% This can be made more efficient with 'cat' or some reshaping method to
% access all cell / struct elements and sum it up!
a1 = cleaned_proband_data.attempts(1);
a2 = cleaned_proband_data.attempts(2);
a1_shots = (a1.xrays.ap + a1.xrays.lat);
a2_shots = (a2.xrays.ap + a2.xrays.lat);

t.time_total = seconds(a1.duration + a2.duration);
t.shots_total = (a1_shots + a2_shots);
t.retrys_total = a1.retry_count + a2.retry_count;

% attempt 1
t.time1 = seconds(a1.duration);
t.shots1 = a1_shots;
t.xrays_lat1 = a1.xrays.lat;
t.xrays_ap1 = a1.xrays.ap;
t.retrys1 =  a1.retry_count;

% attempt 2
t.time2 = seconds(a2.duration);
t.shots2 = a2_shots;
t.xrays_lat2 = a2.xrays.lat;
t.xrays_ap2 = a2.xrays.ap;
t.retrys2 =  a2.retry_count;

% creating table 'proband_table' from struct (if exist, append rows)
if(exist('proband_table','var') == 0)
    % create new, if not already exists
    proband_table = struct2table(t);
    proband_table.Properties.VariableUnits{'time_total'} = 's';
    proband_table.Properties.VariableUnits{'time1'} = 's';
    proband_table.Properties.VariableUnits{'time2'} = 's';
else
    % append, if exists
    t_newRow = struct2table(t);
    proband_table = [proband_table;t_newRow];
end

% free memory
clear('a1', 'a2','a1_shots','a2_shots','t');

% Possbile functions (examples)
%--------------------------------------------------
% Find the Average Across Each Row
% Extract the data from the second, third, and fourth variables using curly braces, {}, find the average of each row, and store it in a new variable, TestAvg.
% T.TestAvg = mean(T{:,2:end},2)
%
% Compute Statistics Using Grouping Variable
%
% Compute the mean and maximum of TestAvg by gender of the students.
%
% varfun(@mean,T,'InputVariables','TestAvg',...
%       'GroupingVariables','Gender')
%
% ans = 2�3 table
%     Gender     GroupCount    mean_TestAvg
%    ________    __________    ____________
%
%    'female'    5             87.067      
%    'male'      5               83.4      
%
% Set a categorical property of a variable (e.g. 'Expert' vs 'novice')
% T.Location = categorical(T.Location);