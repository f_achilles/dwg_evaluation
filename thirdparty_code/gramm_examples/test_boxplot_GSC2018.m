%% Color codes

% green: #5dd780
% orange: #ffdb00
% red: #e81e42 or #ff7474 (lighter)

% darkblue (exp): #1155cc or #0073bd (better)
% lightblue (nov): #6d9eeb or #00bdd6 (better)


%% Graphical stats for wrong level
t = 'Richtig';
f = 'Falsch';
exp = 'experienced';
nov = 'novice';
clear g;

ident_plot.x = {'experienced','experienced','novice','novice'}';

% setting identification of correct vertebra
ident_plot.y = [8/8,0/8,5/12,7/12]'*100;
% ------------------------------------------
ident_plot.class = {t,f,t,f};

g=gramm('x',ident_plot.x,'y',ident_plot.y,'lightness',ident_plot.class);
g.geom_bar('stacked',true,'width',0.4);
g.set_names('x','Gruppe','y','Pedikulierte Wirbelk�rper (%)','lightness','Identifikation');
g.set_title({'Identifikation des korrekten','Wirbelk�rpers'},'FontSize',13);
g.axe_property('YLim',[0 100]);
g.set_order_options('lightness',0); % Order x-axis as raw order
g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
% format text styles
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.draw();


%% Graphical stats for breaches with Gertzbein & Robins Classification
% Version 1 (stacked barplot Grade A, Grade B, > B)
a = 'A (0 mm)';
b = 'B (0-2 mm)';
c = 'C - E (3 mm or more)';
exp = 'experienced';
nov = 'novice';
clear g;
clear lang;
breaches_plot.x = {exp,exp,exp,nov,nov,nov}';

% setting Gertzbein grade of each attempt (gertzbein_eval_p1-p22_xls.txt)
breaches_plot.y = [20/22,1/22,1/22,11/22,4/22,7/22]'*100;
% -----------------------------------------------------------------------
breaches_plot.class = {a,b,c,a,b,c}';

lang(1,:) = {'Group','Number of screws (%)', 'Classification', 'Classification of screw placement', '(Gertzbein & Robbins)'};
lang(2,:) = {'Gruppe','Anzahl an Schrauben (%)', 'Klassifikation', 'Klassifikation der Schraubenendlage', '(Gertzbein & Robbins)'};
lang_idx = 1;   % set language (1 = EN, 2 = DE)

g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'lightness',breaches_plot.class);
g.geom_bar('stacked',true,'width',0.4);
g.set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'lightness',lang(lang_idx,3));
g.set_title([lang(lang_idx,4), lang(lang_idx,5)],'FontSize',13);
g.axe_property('YLim',[0 100]);
g.set_order_options('lightness',0); % Order x-axis as raw order
g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
% format text styles
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.draw();

%% Graphical stats for breaches with Gertzbein & Robins Classification
% Version 2

a = 'A + B (0-2 mm)';
b = '> B (> 2 mm)';
exp = 'experienced';
nov = 'novice';
clear g;
breaches_plot.x = {a,b,a,b}';
breaches_plot.y = [8/8,0/8,8/12,4/12]'*100;
breaches_plot.groups = {exp,exp,nov,nov};

g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'color',breaches_plot.groups);

g.stat_summary('geom',{'bar'});
g.set_names('x','Klassifikation','y','Anzahl an Schrauben (%)','color','Gruppe');
g.set_title({'Auswertung der Schraubenendlage','(nach Gertzbein & Robins)'},'FontSize',13);
g.axe_property('YLim',[0 100]);
% format text styles
g.set_text_options('font','Arial',...
    'base_size',14,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.set_order_options('x',0); % Order x-axis as raw order
g.draw();

%% Graphical stats for Time and X-ray images

clear g;
clear lang;

lang(1,:) = {'Group','Time (min)', 'Total time for both attempts', '(mean per group, 95 % CI)', 'X-ray images','Total number of X-ray images','(mean per group, 95 % CI)'};
lang(2,:) = {'Gruppe','Zeit (min)', 'Zeitdauer f�r zwei Pedikulierungen', '(Mittelwert pro Gruppe, 95 % KI)', 'R�ntgenbilder','Anzahl ben�tigter R�ntgenbilder','(Mittelwert pro Gruppe, 95 % KI)'};
lang_idx = 1;   % set language (1 = EN, 2 = DE)

% x = Groups
% y = time (total) OR xrays (sum ap & lat)
% color = ?
% subset = mean both / 1st try / 2nd try)
T = proband_table;
plotting.x = T.group;
plotting.y = T.time_total/60; % converting sec -> min
plotting.y2 = T.shots_total;

% Time evaluation
g(1,1)=gramm('x',plotting.x,'y',plotting.y,'color',plotting.x);
g(2,1)=copy(g(1,1));
g(2,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
g(2,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color',' ');
g(2,1).set_title([lang(lang_idx,3),lang(lang_idx,4)],'FontSize',13);
g(2,1).axe_property('YLim',[0 30]);
g(2,1).axe_property('Ytick',0:5:50);
% Text f�r MW
%text('experienced',10,{'Important' 'event'},'Parent',g.facet_axes_handles(1),'FontName','Courier');

g(1,1).stat_boxplot();
g(1,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color',' ');
g(1,1).set_title(lang(lang_idx,3),'FontSize',13);
g(1,1).axe_property('YLim',[0 30]);

% X-ray evaluation
g(1,2)=gramm('x',plotting.x,'y',plotting.y2,'color',plotting.x);
g(2,2)=copy(g(1,2));
g(2,2).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
g(2,2).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,5),'color',' ');
g(2,2).set_title([lang(lang_idx,6),lang(lang_idx,7)],'FontSize',13);
g(2,2).axe_property('YLim',[0 150]);
% Text f�r MW

g(1,2).stat_boxplot();
g(1,2).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,5),'color',' ');
g(1,2).set_title(lang(lang_idx,6),'FontSize',13);
g(1,2).axe_property('YLim',[0 150]);

g(1,1).no_legend();
g(1,2).no_legend();
g(2,1).no_legend();
g(2,2).no_legend();

g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);

g.draw();

%% Graphical stats for Path length difference between groups

clear g;
clear plotting;
clear plotting2;
clear lang;

lang(1,:) = {'Group','Path length (m)', 'Path length of instrument tip', '(Avg per group, 95 % CI)'};
lang(2,:) = {'Gruppe','Zeit (min)', 'Zeitdauer f�r zwei Pedikulierungen', '(Mittelwert pro Gruppe, 95 % KI)'};
lang_idx = 1;   % set language (1 = EN, 2 = DE)

T = proband_table;

% setting variables for plot2
plotting.x = T.group;
plotting.y = T.path_total;

% Path length evaluation
g(1,1)=gramm('x',plotting.x,'y',plotting.y,'color',plotting.x);
g(1,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
g(1,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color',' ');
g(1,1).set_title([lang(lang_idx,3),lang(lang_idx,4)],'FontSize',13);
g(1,1).axe_property('YLim',[0 20]);
g(1,1).axe_property('Ytick',0:5:20);
g(1,1).no_legend();

% Histogram
g(1,2)=gramm('x',plotting.y,'color',plotting.x);
g(1,2).stat_bin('geom','overlaid_bar');

% setting variables for plot2
plotting2.x = [T.group;T.group];
plotting2.y = [T.path1;T.path2];
plotting2.trys = [repmat(1,length(T.path1),1);repmat(2,length(T.path2),1)];

g(2,1)=gramm('x',plotting2.x,'y',plotting2.y,'color',plotting2.trys);
g(2,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.3,'dodge',0.3);
g(2,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color','attempts');
g(2,1).set_title([lang(lang_idx,3),lang(lang_idx,4)],'FontSize',13);
g(2,1).set_layout_options('legend_position',[0.85 -0.15 0.2 1]);
g(2,1).set_continuous_color('colormap','viridis','active',false);
g(2,1).axe_property('YLim',[0 20]);
g(2,1).axe_property('Ytick',0:5:20);

% setting options for _all_ plots
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);

% draw everything in one figure
g.draw();

%% Graphical stats for Gertzbein precision [mm] difference between groups

clear g;
clear plotting;
clear plotting2;
clear lang;

lang(1,:) = {'Group','Distance from pedicle wall (mm)', 'Precision of screw placement within pedicle', '(mean per group, 95 % CI)'};
lang(2,:) = {'Gruppe','Zeit (min)', 'Zeitdauer f�r zwei Pedikulierungen', '(Mittelwert pro Gruppe, 95 % KI)'};
lang_idx = 1;   % set language (1 = EN, 2 = DE)

% x = Groups
% y = Gertzbein (both attempts)
% color = ?
% subset = mean both / 1st try / 2nd try)
T = readtable('../../gertzbein_evaluation_table.txt');
plotting.x = [T.group;T.group];
plotting.y = [T.gertzbein_try1;T.gertzbein_try2]; % converting sec -> min

% Precision evaluation
g(1,1)=gramm('x',plotting.x,'y',plotting.y,'color',plotting.x);
g(2,1)=copy(g(1,1));
g(2,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.4);
g(2,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color',' ');
g(2,1).set_title([lang(lang_idx,3),lang(lang_idx,4)],'FontSize',13);
g(2,1).axe_property('YLim',[-2 4]);
g(2,1).axe_property('Ytick',-2:1:4);
% Text f�r MW
%text('experienced',10,{'Important' 'event'},'Parent',g.facet_axes_handles(1),'FontName','Courier');

g(1,1).stat_boxplot();
g(1,1).set_names('x',lang(lang_idx,1),'y',lang(lang_idx,2),'color',' ');
g(1,1).set_title(lang(lang_idx,3),'FontSize',13);
g(1,1).axe_property('YLim',[-4 10]);
g(1,1).axe_property('Ytick',-4:2:10);

g(1,1).no_legend();
g(2,1).no_legend();

g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);

g.draw();


%% TEST

clear t
t.xx =     {'C','C','C','B','B','B','A','A','A'}';
t.yy =     [ 2 , 1 , 3 , 3 , 2 , 10 , 5 , 30 , 2 ]';
t.type =   [ 1 , 1 , 1 , 1 , 2 , 2 , 2 , 2 , 2 ]';

t=struct2table(t)
t=sortrows(t,'yy')

subset = t.type;

g(1,1) = gramm('x',t.xx,'y',t.yy,'color',t.type);
g(1,1).stat_summary('geom',{'bar' 'black_errorbar'},'width',0.3);
figure
g.draw()