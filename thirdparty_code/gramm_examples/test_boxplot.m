%% Color codes

% green: #5dd780
% red: #ff7474 or #e81e42 (darker)

% darkblue (exp): #1155cc or #0073bd (better)
% lightblue (nov): #6d9eeb or #00bdd6 (better)


%% Graphical stats for wrong level
t = 'Richtig';
f = 'Falsch';
exp = 'experienced';
nov = 'novice';
clear g;

ident_plot.x = {'experienced','experienced','novice','novice'}';

% setting identification of correct vertebra
ident_plot.y = [8/8,0/8,5/12,7/12]'*100;
% ------------------------------------------
ident_plot.class = {t,f,t,f};

g=gramm('x',ident_plot.x,'y',ident_plot.y,'lightness',ident_plot.class);
g.geom_bar('stacked',true,'width',0.4);
g.set_names('x','Gruppe','y','Pedikulierte Wirbelk�rper (%)','lightness','Identifikation');
g.set_title({'Identifikation des korrekten','Wirbelk�rpers'},'FontSize',13);
g.axe_property('YLim',[0 100]);
g.set_order_options('lightness',0); % Order x-axis as raw order
g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
% format text styles
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.draw();


%% Graphical stats for breaches with Gertzbein & Robins Classification
% Version 1 (stacked barplot Grade A, Grade B, > B)
a = 'A (0 mm)';
b = 'B (0-2 mm)';
c = '> B (> 2 mm)';
exp = 'experienced';
nov = 'novice';
clear g;
breaches_plot.x = {exp,exp,exp,nov,nov,nov}';

% setting Gertzbein grade of each attempt (gertzbein_eval_p1-p22_xls.txt)
breaches_plot.y = [7/8,1/8,0/8,7/12,1/12,4/12]'*100;
% -----------------------------------------------------------------------
breaches_plot.class = {a,b,c,a,b,c}';

g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'lightness',breaches_plot.class);
g.geom_bar('stacked',true,'width',0.4);
g.set_names('x','Gruppe','y','Anzahl an Schrauben (%)','lightness','Klassifikation');
g.set_title({'Klassifikation der Schraubenendlage','(nach Gertzbein & Robbins)'},'FontSize',13);
g.axe_property('YLim',[0 100]);
g.set_order_options('lightness',0); % Order x-axis as raw order
g.set_color_options('lightness_range',[100 50],'chroma_range',[50 80],'legend','separate');
% format text styles
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.draw();

%% Graphical stats for breaches with Gertzbein & Robins Classification
% Version 2

a = 'A + B (0-2 mm)';
b = '> B (> 2 mm)';
exp = 'experienced';
nov = 'novice';
clear g;
breaches_plot.x = {a,b,a,b}';
breaches_plot.y = [8/8,0/8,8/12,4/12]'*100;
breaches_plot.groups = {exp,exp,nov,nov};

g=gramm('x',breaches_plot.x,'y',breaches_plot.y,'color',breaches_plot.groups);

g.stat_summary('geom',{'bar'});
g.set_names('x','Klassifikation','y','Anzahl an Schrauben (%)','color','Gruppe');
g.set_title({'Auswertung der Schraubenendlage','(nach Gertzbein & Robins)'},'FontSize',13);
g.axe_property('YLim',[0 100]);
% format text styles
g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);
g.set_order_options('x',0); % Order x-axis as raw order
g.draw();

%% Graphical stats for Time and X-ray images

clear g;
% x = Groups
% y = time (total) OR xrays (sum ap & lat)
% color = ?
% subset = mean both / 1st try / 2nd try)
T = proband_table;
plotting2.x = T.group;
plotting2.y = T.time_total/60; % converting sec -> min
plotting2.y2 = T.shots_total;

% Time evaluation
g(1,1)=gramm('x',plotting2.x,'y',plotting2.y,'color',plotting2.x);
g(2,1)=copy(g(1,1));
g(2,1).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
g(2,1).set_names('x','Gruppe','y','Zeit (min)','color',' ');
g(2,1).set_title({'Zeitdauer f�r zwei Pedikulierungen','(MW + 95 % CI pro Gruppe)'},'FontSize',13);
g(2,1).axe_property('YLim',[0 45]);
% Text f�r MW
%text('experienced',10,{'Important' 'event'},'Parent',g.facet_axes_handles(1),'FontName','Courier');

g(1,1).stat_boxplot();
g(1,1).set_names('x','Gruppe','y','Zeit (min)','color',' ');
g(1,1).set_title('Zeitdauer f�r zwei Pedikulierungen','FontSize',13);
g(1,1).axe_property('YLim',[0 45]);

% X-ray evaluation
g(1,2)=gramm('x',plotting2.x,'y',plotting2.y2,'color',plotting2.x);
g(2,2)=copy(g(1,2));
g(2,2).stat_summary('geom',{'bar','black_errorbar'},'width',0.6);
g(2,2).set_names('x','Gruppe','y','R�ntgenbilder','color',' ');
g(2,2).set_title({'Anzahl ben�tigter R�ntgenbilder','(MW + 95 % CI pro Gruppe)'},'FontSize',13);
g(2,2).axe_property('YLim',[0 250]);
% Text f�r MW

g(1,2).stat_boxplot();
g(1,2).set_names('x','Gruppe','y','R�ntgenbilder','color',' ');
g(1,2).set_title('Anzahl ben�tigter R�ntgenbilder','FontSize',13);
g(1,2).axe_property('YLim',[0 250]);

g(1,1).no_legend();
g(1,2).no_legend();
g(2,1).no_legend();
g(2,2).no_legend();

g.set_text_options('font','Arial',...
    'base_size',13,...
    'label_scaling',0.9,...
    'legend_scaling',0.9,...
    'legend_title_scaling',0.9,...
    'facet_scaling',1,...
    'title_scaling',1.0);

g.draw();